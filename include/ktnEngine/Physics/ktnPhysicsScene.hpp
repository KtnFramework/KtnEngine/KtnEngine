#ifndef KTNENGINE_PHYSICS_KTNPHYSICSSCENE_HPP
#define KTNENGINE_PHYSICS_KTNPHYSICSSCENE_HPP
#include "ktnEngine/ktnCore"
#include "ktnPhysicalObject.hpp"

#include <optional>

namespace ktn::Physics {
class ktnPhysicsScene : public Core::ktnObject {
public:
    ktnPhysicsScene();
    ~ktnPhysicsScene();
    void AddObject(std::shared_ptr<ktnPhysicalObject> eObject);
    void Start();
    void Stop();
    void Update();

private:
    class Impl;
    Impl *m_Impl;

    std::vector<std::shared_ptr<ktnPhysicalObject>> m_Objects;
    std::optional<long long> m_LastTime;
};

typedef std::shared_ptr<ktnPhysicsScene> ktnPhysicsScenePtr;
} // namespace ktn::Physics

#endif // KTNENGINE_PHYSICS_KTNPHYSICSSCENE_HPP
