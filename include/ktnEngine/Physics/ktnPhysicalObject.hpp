#ifndef KTNENGINE_PHYSICS_KTNPHYSICALOBJECT_HPP
#define KTNENGINE_PHYSICS_KTNPHYSICALOBJECT_HPP
#include <ktnSignalSlot/ktnSignalSlot.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include <memory>

namespace chrono {
class ChBody;
}

namespace ktn::Physics {
class ktnRigidBody : public ktnSignalReceiver {
public:
    ktnRigidBody() : ktnSignalReceiver() {}
    std::shared_ptr<::chrono::ChBody> Body; // WIP needs some wrapping?

    ktnSignal<glm::vec3> PositionUpdated;
    ktnSignal<glm::quat> RotationUpdated;
};

class ktnPhysicalObject : public ktnSignalReceiver {
public:
    ktnPhysicalObject();

    std::shared_ptr<ktnRigidBody> RigidBody;
    float Mass = 0;
};
} // namespace ktn::Physics

#endif // KTNENGINE_PHYSICS_KTNPHYSICALOBJECT_HPP
