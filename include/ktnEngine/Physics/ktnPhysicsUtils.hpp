#ifndef KTNPHYSICSUTILS_HPP
#define KTNPHYSICSUTILS_HPP
#include <chrono/core/ChQuaternion.h>
#include <chrono/core/ChVector.h>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace ktn::Physics::Utils {
inline glm::vec3 Vec3Convert_ch2glm(const ::chrono::ChVector<> &vec) {
    return glm::vec3(vec.x(), vec.y(), vec.z());
}

inline ::chrono::ChVector<> Vec3Convert_glm2ch(glm::vec3 vec) {
    return ::chrono::ChVector<>(vec.x, vec.y, vec.z);
}

inline glm::quat QuatConvert_ch2glm(const ::chrono::ChQuaternion<> &quat) {
    return glm::quat(
        quat.e0(),
        glm::vec3{
            quat.e1(),
            quat.e2(),
            quat.e3(),
        } //
    );
}

inline ::chrono::ChQuaternion<> QuatConvert_glm2ch(const glm::quat &quat) {
    return ::chrono::ChQuaternion<>(quat.w, ::chrono::ChVector<>(quat.x, quat.y, quat.z));
}
} // namespace ktn::Physics::Utils
#endif // KTNPHYSICSUTILS_HPP
