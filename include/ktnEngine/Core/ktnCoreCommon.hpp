#ifndef KTNENGINE_CORE_KTNCORECOMMON_HPP
#define KTNENGINE_CORE_KTNCORECOMMON_HPP
#include <glm/ext/quaternion_trigonometric.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

// defines to cause compiler warnings for incomplete features and questionable segments of code
#define WIP \
    { bool Unfinished = true; }
#define CONSIDER \
    { bool ThinkAboutThis = true; }
#define BACKEND_DEPENDENT \
    { bool shouldSplitIntoInheritedClass = true; }

namespace ktn {
const inline static glm::quat DefaultRotation(
    1.F,
    glm::vec3{
        0.F,
        0.F,
        0.F,
    });
const inline static glm::vec3 DefaultScale(1, 1, 1);
const inline static glm::vec3 DefaultTranslation(0, 0, 0);

enum class AngleUnit { degree, radian };

/**
 * @brief ComparisonThreshold_Float is the comparison threshold for floats.
 * Any number whose absolute value lies below this threshold will be treated as zero.
 */
static const float ComparisonThreshold_Float(1e-18F);

enum class ExecutionStatus { OK, FAILURE };

enum class ReferenceSpace { Local, World };
const inline static ReferenceSpace LocalSpace(ReferenceSpace::Local);
const inline static ReferenceSpace WorldSpace(ReferenceSpace::World);

// since glm v0.9.9.0 deprecated the initialization of the identity matrix using glm::mat4(), a constant for identity matrix has to be defined
const glm::mat4 IDENTITY_MATRIX_4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
} // namespace ktn
#endif // KTNENGINE_CORE_KTNCORECOMMON_HPP
