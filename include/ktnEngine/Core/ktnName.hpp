﻿#ifndef KTNENGINE_CORE_KTNNAME_HPP
#define KTNENGINE_CORE_KTNNAME_HPP
#include "ktnCoreCommon.hpp"

#include <string>

namespace ktn::Core {
class ktnName {
public:
    explicit ktnName(const char *eString = "unnamed");
    explicit ktnName(const std::string &eString);
    ktnName(const ktnName &eName);
    ktnName(const ktnName &&);
    virtual ~ktnName();

    auto operator==(const std::string &eName) const -> bool;
    auto operator==(const ktnName &eName) const -> bool;
    auto operator!=(const std::string &eName) const -> bool;
    auto operator!=(const ktnName &eName) const -> bool;
    auto operator=(const std::string &eName) -> ktnName &;
    auto operator=(const ktnName &eName) -> ktnName &;
    auto operator=(ktnName &&eName) -> ktnName &;

    [[nodiscard]] auto ToStdString() const -> std::string;

    /**
     * @brief AppendSuffix adds "_0000" to the end of the given name if it's not ended with an underscore and 4 digits
     */
    virtual void AppendSuffix();

    /**
     * @brief BumpSuffix bumps the number at the end of the given name
     */
    virtual void BumpSuffix();

protected:
    /**
     * @brief Format formats a string to meet these requirements:
     * - Alphabetic character at the beginning
     * - No characters except those from a to z, A to Z, 0 to 9 and _
     * - No trailing underscores
     * In case the result after the formatting is an empty string, replace the empty string with "unnamed"
     * @param eString
     */
    static auto Format(const std::string &eString) -> std::string;

    /**
     * @brief SetName sets the name to the given string when it's valid, "unnamed" otherwise
     * @param eString
     */
    inline void SetName(const std::string &eString) {
        m_String = Format(eString);
    }

private:
    std::string m_String = "unnamed";
};
} // namespace ktn::Core
#endif // KTNENGINE_CORE_KTNNAME_HPP
