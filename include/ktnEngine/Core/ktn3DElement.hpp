#ifndef KTNENGINE_CORE_KTN3DELEMENT_HPP
#define KTNENGINE_CORE_KTN3DELEMENT_HPP
#include "ktnObject.hpp"

#include "ktnTransform3D.hpp"

#include <map>
#include <memory>
#include <vector>

namespace ktn::Core {
// TODO: separate dimensionless elements from other 3d elements
// TODOTODO: use Entity-Component-System architecture to achieve that instead of writing new classes for every distinct combination
class ktn3DElement : public ktnObject {
public:
    explicit ktn3DElement(const std::string &eName = "ktn3DElement", ktn3DElement *eParent = nullptr);
    ktn3DElement(const ktn3DElement &other); //!< The copy constructor.

    auto operator=(const ktn3DElement &other) -> ktn3DElement &; //!< The copy assignment operator.

    ~ktn3DElement() override; //!< The destructor.

    auto operator==(const ktn3DElement &other) const -> bool;
    auto operator!=(const ktn3DElement &other) const -> bool;

    [[nodiscard]] auto Parent() const -> ktn3DElement *; //!< Gets the pointer to the parent object.
    [[nodiscard]] auto Children() -> const std::vector<ktn3DElement *> *;

    [[nodiscard]] virtual auto AnimatedModelMatrix(ReferenceSpace eSpace) const -> glm::mat4;
    [[nodiscard]] virtual auto ModelMatrix(ReferenceSpace eSpace) const -> glm::mat4;
    [[nodiscard]] virtual auto Position(ReferenceSpace eSpace) const -> glm::vec3;
    [[nodiscard]] virtual auto Rotation(ReferenceSpace eSpace) const -> glm::quat;
    [[nodiscard]] virtual auto Scale(ReferenceSpace eSpace) const -> glm::vec3;

    virtual void SetParent(ktn3DElement *eParent);
    virtual void SetPosition(const glm::vec3 &ePosition, ReferenceSpace eSpace);
    virtual void SetRotation(const glm::quat &eRotationQuaternion, ReferenceSpace eSpace);
    virtual void SetScale(float eScale, ReferenceSpace eSpace);
    virtual void SetScale(const glm::vec3 &eScale, ReferenceSpace eSpace);
    virtual void SetWorldSpacePosition(glm::vec3 ePosition);
    virtual void SetWorldSpaceRotation(glm::quat eRotation);

    virtual void OffsetPosition(const glm::vec3 &offset, ReferenceSpace eSpace);

    virtual void ResetAnimatedTransforms();

    virtual void UpdateAnimatedLocalSpaceModelMatrix();
    virtual void UpdateAnimatedWorldSpaceModelMatrix();

    std::map<std::string, ktnPosRotScaleInfluence> AnimatedInfluences;
#ifndef NDEBUG
    void PrintDebugInfo(std::ostream &os = std::cout) override; //!< Prints the object's debug information.
#endif
protected:
    void UpdateLocalSpaceModelMatrix();
    void UpdateWorldSpaceModelMatrix();

    ktnTransform3D m_Transform_LS; //!< The original local space transform.
    ktnTransform3D m_Transform_WS; //!< The original world space transform.

    glm::mat4 m_AnimatedMatrix_LS = IDENTITY_MATRIX_4; //!< The local space model matrix with animations applied.
    glm::mat4 m_AnimatedMatrix_WS = IDENTITY_MATRIX_4; //!< The world space model matrix with animations applied.

    std::vector<ktn3DElement *> m_Children;
    ktn3DElement *m_Parent = nullptr;

private:
    void AddChild(ktn3DElement *eChild);

    /**
     * @brief RemoveChild
     * @param eChild is the child object to be removed from the children list.
     */
    void RemoveChild(ktn3DElement *eChild);
    static glm::quat ValidateRotationQuaternion(const glm::quat &eQuaternion);
};
} // namespace ktn::Core
#endif // KTNENGINE_CORE_KTN3DELEMENT_HPP
