﻿#ifndef KTNENGINE_CORE_KTNCOREUTILS_HPP
#define KTNENGINE_CORE_KTNCOREUTILS_HPP
#include "ktnCoreCommon.hpp"

#include <algorithm>
#include <vector>

namespace ktn::Core::Utils {
enum class CompareFloatResult { Equal, Greater, Smaller };
auto AreEqual(const glm::quat &q1, const glm::quat &q2, const float &threshold = ComparisonThreshold_Float) -> bool;
/**
 * @brief CompareFloat compares 2 floating point numbers.
 * This is used in situations where a difference under a certain threshold is considered neligible,
 * for example, comparing objects' positions in 3D Cartesian space to find duplicates.
 * The user can decide how much precision they need based on each use case, and set the threshold accordingly.
 * @param f1 is the first number.
 * @param f2 is the second number.
 * @param threshold is the threshold, under which a number is considered zero.
 * @return Equal if the absolute difference is less than the threshold.
 * Otherwise Greater if f1 > f2, and Smaller if f1 < f2.
 */
auto CompareFloat(const float &f1, const float &f2, const float &threshold = ComparisonThreshold_Float) -> CompareFloatResult;

int64_t CurrentMicrosecondsSinceEpoch();
int64_t CurrentMillisecondsSinceEpoch();

template <class ItemType, class Predicate_t>
void EraseRemove(std::vector<ItemType> &eContainer, Predicate_t ePredicate) {
    eContainer.erase(std::remove_if(eContainer.begin(), eContainer.end(), ePredicate), eContainer.end());
}

/**
 * @brief IsAlphabetic checks if the character belongs in the English alphabet.
 * This function is written despite the knowledge that isalpha and isalnum exist,
 * since those functions depend on the locale of the machine on which the code is executed.
 * If the developer use a non-English locale and a character in any string happens to be non-alphabetic in English,
 * the application might throw strange errors that the developers might easily overlook and cannot replicate on their machine.
 * @param c is the character whose alphabeticity is inspected.
 * @return true if c is alphabetic, false if not.
 */
auto IsAlphabetic(const char &c) -> bool;
} // namespace ktn::Core::Utils
#endif // KTNENGINE_CORE_KTNCOREUTILS_HPP
