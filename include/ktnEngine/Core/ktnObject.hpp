#ifndef KTNENGINE_CORE_KTNOBJECT_HPP
#define KTNENGINE_CORE_KTNOBJECT_HPP
#include <ktnSignalSlot/ktnSignalSlot.hpp>

#include "ktnCoreCommon.hpp"
#include "ktnName.hpp"

#ifndef NDEBUG
#include <iostream>
#endif

namespace ktn::Core {
class ktnObject : public ktnSignalReceiver {
public:
    explicit ktnObject(const std::string &eName = "ktnObject");
    ktnObject(const ktnObject &other); //!< The copy constructor.
    virtual ~ktnObject(); //!< The destructor.

    auto operator==(const ktnObject &eObj) const -> bool;
    auto operator!=(const ktnObject &eObj) const -> bool;
    auto operator=(const ktnObject &eObj) -> ktnObject &; //!< The copy assignment operator.

#ifndef NDEBUG
    virtual void PrintDebugInfo(std::ostream &os = std::cout); //!< Prints the object's debug information.
#endif

    ktnName Name;
};
} // namespace ktn::Core
#endif // KTNENGINE_CORE_KTNOBJECT_HPP
