#ifndef KTNENGINE_CORE_KTNTRANSFORM3D_HPP
#define KTNENGINE_CORE_KTNTRANSFORM3D_HPP
#include "ktnCoreCommon.hpp"

#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/compatibility.hpp> // for vec3's lerp function
#include <glm/gtx/quaternion.hpp>

namespace ktn::Core {
class ktnPosRotScaleInfluence {
public:
    glm::vec3 Position = glm::vec3(0.0F, 0.0F, 0.0F);
    glm::quat Rotation = DefaultRotation;
    glm::vec3 Scale = glm::vec3(1.0F, 1.0F, 1.0F);
    float Influence = 0.0F;
};

class ktnTransform3D {
public:
    bool operator!=(const ktnTransform3D &other) const;
    glm::mat4 ModelMatrix = IDENTITY_MATRIX_4;
    glm::vec3 Position = glm::vec3(0.0F, 0.0F, 0.0F);
    glm::quat Rotation = DefaultRotation;
    glm::vec3 Scale = glm::vec3(1.0F, 1.0F, 1.0F);
};
} // namespace ktn::Core
#endif // KTNENGINE_CORE_KTNTRANSFORM3D_HPP
