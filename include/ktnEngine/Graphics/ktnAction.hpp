#ifndef KTNENGINE_GRAPHICS_KTNACTION_HPP
#define KTNENGINE_GRAPHICS_KTNACTION_HPP
#include <ktnEngine/ktnCore>

#include "ktnActionSampler.hpp"

#include <ktnSignalSlot/ktnSignalSlot.hpp>

#include <map>
#include <string>

namespace ktn::Graphics {
class ktnAction : ktnSignalReceiver {
public:
    struct Channel {
        enum class ChannelPath { Rotation, Scale, Translation, Weights };

        Core::ktn3DElement *Target = nullptr;
        ChannelPath Path;
        std::shared_ptr<ktnActionSampler> Sampler;
    };

    [[nodiscard]] float Duration() const; //!< The duration of the action. Unit: [s].

    void CalculateDuration();
    bool IsPlaying() const;

    /**
     * @brief Interpolate determines the transformation at the current time, and applies to the animation target.
     */
    void Interpolate();
    void LoadFromGLTF(const gleb::glTF &eGltf, size_t eIndex);
    void Start();
    void Stop();

    std::string Name;

    float Influence = 0;
    float TimeScale;
    bool Loop = false;
    std::vector<Channel> Channels;

    // WIP why can't it just be a vector?
    std::map<size_t, std::shared_ptr<ktnActionSampler>> Samplers;

    bool Completed;

private:
    void CalculateInfluence(float currentSecondsSinceStart);

    /**
     * @brief Stop stops the action.
     * @param currentTime is the current time [ms since epoch].
     */
    void Stop(long currentTime);
    float m_TransitionDuration = 100; //!< Duration of the transition [ms]. Must be > 0 due to division.
    std::unique_ptr<float> m_Duration; //!< The duration of the action. Unit: [s].
    long m_StartTime = 0; //!< The time at which the action is started [ms since epoch].

    long m_StopTime = 0; //!< The time at which the action is stopped [ms since epoch].
};
} // namespace ktn::Graphics
#endif // KTNENGINE_GRAPHICS_KTNACTION_HPP
