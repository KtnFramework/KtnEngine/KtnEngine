#ifndef KTNENGINE_GRAPHICS_KTNMESH_GLTF_HPP
#define KTNENGINE_GRAPHICS_KTNMESH_GLTF_HPP
#include "ktnGraphicsCommon.hpp"

#include "ktnMaterial.hpp"
#include "ktnShaderProgram.hpp"
#include "ktnSkin.hpp"

namespace ktn::Graphics {
class ktnMesh : public Core::ktn3DElement {
public:
    explicit ktnMesh(const std::string &eName, ktn3DElement *eParent);
    ~ktnMesh() override;

    std::vector<float> Buffer; // contains all positions, normals, tangents, texcoords, in that order

    std::vector<glm::vec3> Positions;
    std::vector<glm::vec3> Normals;
    std::vector<glm::vec4> Tangents;
    std::vector<glm::vec2> TexCoords;
    std::optional<std::vector<glm::uvec4>> Joints;
    std::optional<std::vector<glm::vec4>> Weights;
    std::vector<uint32_t> Indices;

    ktnMaterial Material;
    ktnSkin *Skin = nullptr;

    uint32_t VertexArrayObject{};
    uint32_t VertexBufferObject{};
    uint32_t ElementBufferObject{};
};
} // namespace ktn::Graphics
#endif // KTNENGINE_GRAPHICS_KTNMESH_GLTF_HPP
