#ifndef KTNENGINE_GRAPHICS_KTNRENDERER_OPENGL_HPP
#define KTNENGINE_GRAPHICS_KTNRENDERER_OPENGL_HPP
#include "IktnRenderer.hpp"
#include "ktnFrameBuffer.hpp"

#include <memory>

namespace ktn::Graphics {
class ktnRenderer_OpenGL : public IktnRenderer {
public:
    ktnRenderer_OpenGL();
    ~ktnRenderer_OpenGL() override;

    [[nodiscard]] ktnGraphicsBackend Backend() final;

    void SetContext(void *eContext) override;
    void SetShader(const std::string &name, ktnShaderProgram *shader) override;

    void Initialize(const vk::Extent2D &eExtent2D, unsigned int shadowMapSize) override;
    void RenderFrame(ktnGraphicsScenePtr scene) override;
    void CleanUp() override;
    void WaitIdle() override;

    void Bind(ktnFrameBuffer *eBuffer);
    void CreateOnGPU(ktnFrameBuffer *eBuffer, const FrameBufferType &type, const vk::Extent2D &eExtent2D, const unsigned int &nTextures = 1);

    void LoadToGPU(ktnGraphicsScenePtr eScene) override;
    void LoadToGPU(ktnMesh *eMesh) override;
    void LoadToGPU(ktnModel *eModel) override;
    void LoadToGPU(ktnTexture *eTexture);

    void RenderShadow(ktnGraphicsScenePtr scene);
    void RenderColor(ktnGraphicsScenePtr scene);
    void RenderBlurs();
    void RenderBloom();
    void RenderCombined();

    void UnbindFrameBuffers();
    void UnloadFromGPU(ktnFrameBuffer *eBuffer);
    void UnloadFromGPU(ktnGraphicsScenePtr eScene) override;
    void UnloadFromGPU(ktnTexture *eTexture);

    void RenderDeferred(ktnGraphicsScenePtr scene);
    void RenderGBuffer(ktnGraphicsScenePtr scene);
    virtual void RenderGUI(ktnGraphicsScenePtr scene);

    void HandleEvent_FramebufferSizeChanged(int width, int height) override;

protected:
    void CleanUpFrameBuffers();
    void DrawMesh(ktnMesh *eMesh, ktnShaderProgram *eShader);
    void DrawModel(std::shared_ptr<ktnModel> eModel, ktnShaderProgram *eShader);
    void DrawScene(ktnGraphicsScenePtr eGraphicsScene, ktnShaderProgram *eShader);
    void SetUpFramebuffers(const vk::Extent2D &eExtent2D);

    std::vector<ktnFrameBuffer *> ShadowBuffer_Directional;

private:
    std::vector<std::pair<size_t, std::string>> m_DebugText;
    std::unique_ptr<ktnFrameBuffer> m_GBuffer;
    std::unique_ptr<ktnFrameBuffer> m_FrameBuffer_Deferred;

    std::unique_ptr<ktnFrameBuffer> m_FrameBuffer;
    std::unique_ptr<ktnFrameBuffer> m_FrameBufferMedium;
    std::unique_ptr<ktnFrameBuffer> m_FrameBufferSmall;
    std::array<std::unique_ptr<ktnFrameBuffer>, 2> m_BlurBuffersMedium;
    std::array<std::unique_ptr<ktnFrameBuffer>, 2> m_BlurBuffersSmall;
    std::unique_ptr<ktnFrameBuffer> m_BloomBuffer;

    unsigned int m_FullscreenRectangleVertexArray = 0;
    unsigned int m_FullscreenRectangleVertexBuffer = 0;

    std::map<std::string, ktnShaderProgram *> Shaders;

    unsigned int m_ShadowMapSize = 0;

    const unsigned int m_DownscaleFactor_Medium = 2;
    const unsigned int m_DownscaleFactor_Small = 16;
    const unsigned int m_DownscaleFactor_Tiny = 64;

    bool m_Initialized = false;
};
} // namespace ktn::Graphics
#endif // KTNENGINE_GRAPHICS_KTNRENDERER_OPENGL_HPP
