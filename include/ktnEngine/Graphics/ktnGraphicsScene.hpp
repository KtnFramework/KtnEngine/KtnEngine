#ifndef KTNENGINE_GRAPHICS_KTNGRAPHICSSCENE_HPP
#define KTNENGINE_GRAPHICS_KTNGRAPHICSSCENE_HPP
#include "ktnCamera.hpp"
#include "ktnLight.hpp"
#include "ktnModel.hpp"

#include <mutex>

namespace ktn::Graphics {
class ktnGraphicsScene : public Core::ktnObject {
public:
    ktnGraphicsScene();
    virtual ~ktnGraphicsScene();

    [[nodiscard]] auto CurrentCamera() const -> std::shared_ptr<ktnCamera>;

    void SetCurrentCamera(std::shared_ptr<ktnCamera> eCamera);

    void AddCamera(std::shared_ptr<ktnCamera> eCamera);
    void AddLight(std::shared_ptr<ktnLight> eLight);
    void AddModel(std::shared_ptr<ktnModel> eModel);
    void RemoveCamera(std::shared_ptr<ktnCamera> eCamera);
    void RemoveLight(std::shared_ptr<ktnLight> eLight);
    void RemoveModel(std::shared_ptr<ktnModel> eModel);

    std::vector<std::shared_ptr<ktnCamera>> Cameras;
    std::vector<std::shared_ptr<ktnLight>> Lights;
    std::vector<std::shared_ptr<ktnModel>> Models;

protected:
    mutable std::mutex m_ModelMutex;

    std::shared_ptr<ktnCamera> m_CurrentCamera;
};

typedef std::shared_ptr<ktnGraphicsScene> ktnGraphicsScenePtr;
} // namespace ktn::Graphics
#endif // KTNENGINE_GRAPHICS_KTNGRAPHICSSCENE_HPP
