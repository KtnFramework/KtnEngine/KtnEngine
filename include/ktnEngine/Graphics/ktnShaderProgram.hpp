﻿#ifndef KTNENGINE_GRAPHICS_KTNSHADERPROGRAM_HPP
#define KTNENGINE_GRAPHICS_KTNSHADERPROGRAM_HPP
#include "ktnLight.hpp"

#include <filesystem>
#include <string>

namespace ktn::Graphics {
class ktnShaderProgram {
public:
    explicit ktnShaderProgram(
        const std::filesystem::path &VertexShaderPath, // TODO add description comments for the parameters
        const std::filesystem::path &FragmentShaderPath,
        const std::filesystem::path &GeometryShaderPath = std::string());
    explicit ktnShaderProgram( //
        const std::string &eVertexShaderSource,
        const std::string &eFragmentShaderSource);

    void SetShader(const std::string &eSource, const ShaderType &type);
    void Update(); // not sure if possible to update shader on the fly
    void Use() const;

    void SetVertexShader(const std::string &VertexShaderPath);
    void SetFragmentShader(const std::string &FragmentShaderPath);
    void SetGeometryShader(const std::string &GeometryShaderPath);

    /**
     * Notice the references with 2 '&'s.
     * Without the extra '&', the compiler will give an error
     * when we pass a function or a class method to this parameter
     * For example: yourshader.SetUniformMat4("ViewMatrix", Camera::ViewMatrix())
     * wouldn't work without the &&
     * This problem could also be solved by using template, but templates are defined in the header,
     * and generally not smooth to use, so I resorted to this.
     */
    void SetUniformBool(const std::string &name, const bool &value) const;
    void SetUniformBool(const std::string &name, const bool &&value) const;

    void SetUniformFloat(const std::string &name, const float &value) const;
    void SetUniformFloat(const std::string &name, const float &&value) const;

    void SetUniformInt(const std::string &name, const int &value) const;
    void SetUniformInt(const std::string &name, const int &&value) const;

    void SetUniformMat2(const std::string &name, const glm::mat2 &mat) const;
    void SetUniformMat2(const std::string &name, const glm::mat2 &&mat) const;
    void SetUniformMat3(const std::string &name, const glm::mat3 &mat) const;
    void SetUniformMat3(const std::string &name, const glm::mat3 &&mat) const;
    void SetUniformMat4(const std::string &name, const glm::mat4 &mat) const;
    void SetUniformMat4(const std::string &name, const glm::mat4 &&mat) const;

    void SetUniformVec2(const std::string &name, const glm::vec2 &vec) const;
    void SetUniformVec2(const std::string &name, const glm::vec2 &&vec) const;
    void SetUniformVec2(const std::string &name, const float &x, const float &y) const;
    void SetUniformVec2(const std::string &name, const float &&x, const float &&y) const;

    void SetUniformVec3(const std::string &name, const glm::vec3 &vec) const;
    void SetUniformVec3(const std::string &name, const glm::vec3 &&vec) const;
    void SetUniformVec3(const std::string &name, const float &x, const float &y, const float &z) const;
    void SetUniformVec3(const std::string &name, const float &&x, const float &&y, const float &&z) const;

    void SetUniformVec4(const std::string &name, const glm::vec4 &vec) const;
    void SetUniformVec4(const std::string &name, const glm::vec4 &&vec) const;
    void SetUniformVec4(const std::string &name, const float &x, const float &y, const float &z, const float &w) const;
    void SetUniformVec4(const std::string &name, const float &&x, const float &&y, const float &&z, const float &&w) const;

    ShaderPass Pass;

private:
    static std::string GetShaderCode(const std::string &Path);
    static auto CompileShader(const std::string &source, const ShaderType &type) -> unsigned int;
    static void CheckCompileErrors(unsigned int ShaderID, ShaderType type);
    static void CheckLinkErrors(unsigned int ProgramID);
    unsigned int ProgramID;
    unsigned int FragmentShaderID{};
    unsigned int GeometryShaderID{};
    unsigned int VertexShaderID{};
};
} // namespace ktn::Graphics
#endif // KTNENGINE_GRAPHICS_KTNSHADERPROGRAM_HPP
