#ifndef KTNENGINE_GRAPHICS_KTNMODELLOADER_GLTF_HPP
#define KTNENGINE_GRAPHICS_KTNMODELLOADER_GLTF_HPP
#include "IktnModelLoader.hpp"

#include <gleb/gltf.hpp>

namespace ktn::Graphics {
class ktnModelLoader_glTF : public IktnModelLoader {
public:
    ~ktnModelLoader_glTF();
    void Initialize(std::shared_ptr<gleb::glTF> eGltf);
    [[nodiscard]] std::map<size_t, std::shared_ptr<ktnAction>> LoadActions() final;
    [[nodiscard]] std::map<size_t, ktnMesh *> LoadMeshes(Core::ktn3DElement *eParent) final;
    [[nodiscard]] std::map<size_t, ktnSkin *> LoadSkins() final;

private:
    void LoadMeshFromNode(std::shared_ptr<gleb::glTF> eGltf, size_t eNodeIndex, std::map<size_t, ktnMesh *> &eMeshMap, Core::ktn3DElement *eParent);
    [[nodiscard]] static ktnSkin *LoadSkin(std::shared_ptr<gleb::glTF> eGltf, size_t eIndex);
    [[nodiscard]] static ktnJoint *LoadJoint(std::shared_ptr<gleb::glTF> eGltf, size_t eIndex, Core::ktn3DElement *eSkin);
    std::weak_ptr<gleb::glTF> m_Gltf;
};
} // namespace ktn::Graphics
#endif // KTNENGINE_GRAPHICS_KTNMODELLOADER_GLTF_HPP
