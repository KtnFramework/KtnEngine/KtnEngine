#ifndef KTNENGINE_GRAPHICS_KTNCAMERA_HPP
#define KTNENGINE_GRAPHICS_KTNCAMERA_HPP
#include <ktnEngine/Core/ktn3DElement.hpp>

#include "ktnEngine/Math/ktnEulerAngles.hpp"
#include "ktnEngine/ktnCore"
#include "ktnGraphicsCommon.hpp"

#include <glm/glm.hpp>

namespace ktn::Graphics {
class ktnCamera : public Core::ktn3DElement {
public:
    explicit ktnCamera(const glm::vec3 &initialPosition = glm::vec3(0.0F), const Math::ktnEulerAngles &initialEulerAngles = Math::ktnEulerAngles());

    ~ktnCamera() override;

    auto operator==(const ktnCamera &eCamera) const -> bool;
    auto operator!=(const ktnCamera &eCamera) const -> bool;
    auto operator=(const ktnCamera &eCamera) -> ktnCamera &;

    [[nodiscard]] auto Direction() const -> glm::vec3;
    [[nodiscard]] auto Front() const -> glm::vec3;
    [[nodiscard]] auto Mode() const -> CameraMode;
    [[nodiscard]] auto Right() const -> glm::vec3;
    [[nodiscard]] auto Sensitivity() const -> float;
    [[nodiscard]] auto Target() const -> glm::vec3;
    [[nodiscard]] auto Top() const -> glm::vec3;
    [[nodiscard]] auto Type() const -> CameraType;
    [[nodiscard]] auto UniversalUp() const -> glm::vec3;
    [[nodiscard]] auto ViewMatrix() const -> glm::mat4;

    void SetDirection(const glm::vec3 &dir);
    void SetDirection(const float &x, const float &y, const float &z);
    void SetFront(const glm::vec3 &front);
    void SetFront(const float &x, const float &y, const float &z);
    void SetMode(const CameraMode &Mode);
    void SetTarget(const glm::vec3 &targetpos);
    void SetTarget(const float &x, const float &y, const float &z);
    void SetType(const CameraType &Type);
    void SetUniversalUp(const glm::vec3 &up);

    void MoveForward(const float &thismuch);
    void MoveBackward(const float &thismuch);
    void MoveLeft(const float &thismuch);
    void MoveRight(const float &thismuch);
    void RegulatePitch();
    void TurnDown(const float &thismuch);
    void TurnLeft(const float &thismuch);
    void TurnRight(const float &thismuch);
    void TurnUp(const float &thismuch);
    /**
     * @brief UpdateRightAndTop updates the camera's Right and Top vectors
     * depending on the current camera type and mode
     */
    void UpdateRightAndTop();
    void UpdateFront();
    void UpdateViewMatrix();

    Math::ktnEulerAngles EulerAngles;

private:
    CameraType m_Type = CameraType::FIRSTPERSON;
    CameraMode m_Mode = CameraMode::FREELOOK;
    glm::vec3 m_UniversalUp = glm::vec3(0.0F, 1.0F, 0.0F);
    glm::vec3 m_PositionOffset{};
    glm::vec3 m_Target{}; // not needed for fps but does need for other types
    glm::vec3 m_Direction{};
    glm::vec3 m_Right{};
    glm::vec3 m_Top = glm::vec3(0.0F, 0.0F, 1.0F);
    glm::vec3 m_Front = glm::vec3(0.0F, -1.0F, 0.0F);
    glm::mat4 m_ViewMatrix = IDENTITY_MATRIX_4;

    float m_Sensitivity = Defaults::Camera_Sensitivity;
};
} // namespace ktn::Graphics
#endif // KTNENGINE_GRAPHICS_KTNCAMERA_HPP
