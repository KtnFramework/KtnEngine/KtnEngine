#ifndef KTNENGINE_GRAPHICS_KTNACTIONSAMPLER_HPP
#define KTNENGINE_GRAPHICS_KTNACTIONSAMPLER_HPP
#include <ktnEngine/ktnCore>

#include <gleb/gltf.hpp>

namespace ktn::Graphics {
struct ktnActionSampler {
    enum class InterpolationAlgorithm { LINEAR, STEP, CUBICSPLINE };
    enum class SamplerType { Undefined, Rotation, Scale, Translation };

    template <typename T>
    struct SamplePack {
        void Verify() {
            if (Current.size() == 0) { throw std::runtime_error("Sample pack contains no sample."); }

            if ((PreviousTangent.has_value() && !NextTangent.has_value()) || (!PreviousTangent.has_value() && NextTangent.has_value())) {
                throw std::runtime_error("Inconsistent sample pack.");
            }
            if (PreviousTangent.has_value()) {
                if (PreviousTangent.value().size() != Current.size() || Current.size() != NextTangent.value().size()) {
                    throw std::runtime_error("Inconsistent sample pack.");
                }
            }
        }
        std::optional<std::vector<std::pair<float, T>>> PreviousTangent; //!< Time and sample at each time
        std::vector<std::pair<float, T>> Current; //!< Time and sample at each time
        std::optional<std::vector<std::pair<float, T>>> NextTangent; //!< Time and sample at each time
    };
    void LoadRotationSamples(size_t eSampleCount, const gleb::bufferView &eBufferView, const gleb::buffer &eBuffer);
    void LoadSampleTimes(size_t eSampleCount, gleb::bufferView eBufferView, gleb::buffer eBuffer);
    void LoadScaleSamples(size_t eSampleCount, std::string eType, const gleb::bufferView &eBufferView, const gleb::buffer &eBuffer);
    void LoadTranslationSamples(size_t eSampleCount, const gleb::bufferView &eBufferView, const gleb::buffer &eBuffer);

    void Verify();

    SamplerType Type = SamplerType::Undefined;

    InterpolationAlgorithm Interpolation = InterpolationAlgorithm::LINEAR;

    std::vector<float> SampleTimes; // TODO consider a way to remove duplicates

    std::shared_ptr<SamplePack<glm::quat>> RotationSamples;

    std::shared_ptr<SamplePack<glm::vec3>> ScaleSamples;

    std::shared_ptr<SamplePack<glm::vec3>> TranslationSamples;
};
} // namespace ktn::Graphics
#endif // KTNENGINE_GRAPHICS_KTNACTIONSAMPLER_HPP
