#ifndef KTNENGINE_GRAPHICS_KTNGRAPHICSSETTINGS_HPP
#define KTNENGINE_GRAPHICS_KTNGRAPHICSSETTINGS_HPP
#include "ktnGraphicsCommon.hpp"

namespace ktn::Graphics {
class GraphicsSettings {
public:
    GraphicsSettings();
    std::string AspectRatio;
    std::string Resolution;
    unsigned int BloomLevel{};
    unsigned int FogLevel{};
    unsigned int MotionBlurLevel{};
    bool DepthOfField{};
    bool HighDynamicRange{};
    bool MultiSampleAntiAliasing{};
    bool ScreenSpaceGlobalIllumination{};
    bool ScreenSpaceReflection{};
    bool ScreenSpaceAmbientOcclusion{};
    bool Tesselation{};
    bool VerticalSynchronization{};
    GraphicsLevels TextureDetails = GraphicsLevels::LOW;
    GraphicsLevels ShadowDetails = GraphicsLevels::LOW;
};
} // namespace ktn::Graphics
#endif // KTNENGINE_GRAPHICS_KTNGRAPHICSSETTINGS_HPP
