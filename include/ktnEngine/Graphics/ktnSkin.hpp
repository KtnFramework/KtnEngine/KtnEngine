#ifndef KTNENGINE_GRAPHICS_KTNSKIN_HPP
#define KTNENGINE_GRAPHICS_KTNSKIN_HPP
#include "ktnGraphicsCommon.hpp"
#include "ktnJoint.hpp"

namespace ktn::Graphics {
class ktnSkin : public Core::ktn3DElement {
public:
    ktnSkin(const std::string &eName, ktn3DElement *eParent);
    ~ktnSkin();
    std::vector<std::pair<size_t, ktnJoint *>> Joints;
};
} // namespace ktn::Graphics
#endif
