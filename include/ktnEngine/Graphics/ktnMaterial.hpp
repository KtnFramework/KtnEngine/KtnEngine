#ifndef KTNENGINE_GRAPHICS_KTNMATERIAL_HPP
#define KTNENGINE_GRAPHICS_KTNMATERIAL_HPP
#include "ktnGraphicsCommon.hpp"
#include "ktnTexture.hpp"
#include <ktnEngine/ktnCore>

#include <algorithm> // for std::clamp
#include <optional>

namespace ktn::Graphics {
class ktnMaterial : public Core::ktnObject {
public:
    struct pbrMetallicRoughnessInfo {
        struct baseColorTexture_t {
            baseColorTexture_t(const std::string &eUri, size_t eTexCoords);
            ~baseColorTexture_t();
            std::unique_ptr<ktnTexture> Texture;
            size_t TextureCoordinates = 0;
        };

        pbrMetallicRoughnessInfo(); //!< The default constructor.
        pbrMetallicRoughnessInfo(pbrMetallicRoughnessInfo &&other); //!< The move constructor.

        pbrMetallicRoughnessInfo &operator=(pbrMetallicRoughnessInfo &other); //!< The copy assignment operator.
        glm::vec4 baseColorFactor = Defaults::BaseColorFactor;
        std::shared_ptr<baseColorTexture_t> baseColorTexture = nullptr;
        float metallicFactor = Defaults::MetallicFactor;
        float roughnessFactor = Defaults::RoughnessFactor;
    };

    ~ktnMaterial() override; //!< The default destructor.
    bool doubleSided = false;
    std::shared_ptr<pbrMetallicRoughnessInfo> pbrMetallicRoughness;
};
} // namespace ktn::Graphics

#endif // KTNENGINE_GRAPHICS_KTNMATERIAL_HPP
