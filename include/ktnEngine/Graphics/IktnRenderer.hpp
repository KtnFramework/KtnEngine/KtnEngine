#ifndef KTNENGINE_GRAPHICS_IKTNRENDERER_HPP
#define KTNENGINE_GRAPHICS_IKTNRENDERER_HPP
#include "ktnGraphicsScene.hpp"
#include "ktnGraphicsSettings.hpp"

namespace ktn::Graphics {
class IktnRenderer : public Core::ktnObject {
public:
    IktnRenderer(std::string eName) : ktnObject(eName) {}
    virtual ~IktnRenderer() = 0;

    [[nodiscard]] virtual ktnGraphicsBackend Backend() = 0;

    // TODO: This was apparently used to hide dependency to glfw.
    // instead of passing a GLFWWindow, it passes a "context".
    // The window and its related properties (like querying width, height, creating surface...)
    // Should be separated into a light wrapper.
    virtual void SetContext(void *eContext) = 0;
    virtual void SetShader(const std::string &name, ktnShaderProgram *shader) = 0;

    virtual void CleanUp() = 0;
    virtual void Initialize(const vk::Extent2D &eExtent2D, unsigned int shadowMapSize) = 0;
    virtual void LoadToGPU(ktnGraphicsScenePtr eScene) = 0;
    virtual void LoadToGPU(ktnModel *eModel) = 0;
    virtual void LoadToGPU(ktnMesh *eMesh) = 0;
    virtual void RenderFrame(ktnGraphicsScenePtr scene) = 0;
    virtual void UnloadFromGPU(ktnGraphicsScenePtr eScene) = 0;
    virtual void WaitIdle() = 0;

    virtual void HandleEvent_FramebufferSizeChanged(int width, int height) = 0;
    ktnSignal<> FrameRendered;

protected:
    vk::Extent2D m_Extent2D;
    GraphicsSettings m_GraphicsSettings;
}; // class IktnRenderer
} // namespace ktn::Graphics
#endif // KTNENGINE_GRAPHICS_IKTNRENDERER_HPP
