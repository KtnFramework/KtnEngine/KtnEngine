#ifndef KTNENGINE_GRAPHICS_KTNRENDERER_VULKAN_HPP
#define KTNENGINE_GRAPHICS_KTNRENDERER_VULKAN_HPP
#include "IktnRenderer.hpp"

#include <optional>

struct GLFWwindow;

namespace ktn::Graphics {
////////////////////
// class QueueFamily
class QueueFamily {
public:
    void SetIndex(uint32_t index) {
        m_Index = index;
    }
    void SetCount(uint32_t count) {
        m_Count = count;
    }
    auto Index() -> std::optional<uint32_t> {
        return m_Index;
    }
    auto Count() -> std::optional<uint32_t> {
        return m_Count;
    }

private:
    std::optional<uint32_t> m_Index;
    std::optional<uint32_t> m_Count;
};

//////////////////////
// class QueueFamilies
class QueueFamilies {
public:
    auto AreComplete() -> bool {
        return (Graphics.Count().has_value() && Graphics.Index().has_value() && Present.Count().has_value() && Present.Index().has_value());
    }

    QueueFamily Graphics;
    QueueFamily Present;
};

///////////////////////////
// class ktnRenderer_Vulkan
class ktnRenderer_Vulkan : public IktnRenderer {
public:
    ktnRenderer_Vulkan();
    ~ktnRenderer_Vulkan() override;

    [[nodiscard]] ktnGraphicsBackend Backend() final;

    virtual void SetContext(void *eContext) override;
    void SetShader(const std::string &name, ktnShaderProgram *shader) override;

    void CleanUp() override;
    void Initialize(const vk::Extent2D &eExtent2D, unsigned int /*shadowMapSize*/) override;
    void LoadToGPU(ktnGraphicsScenePtr /*eScene*/) override {}
    void LoadToGPU(ktnModel * /*eModel*/) override {}
    void LoadToGPU(ktnMesh * /*eMesh*/) override {}
    void RenderFrame(ktnGraphicsScenePtr scene) override;
    void UnloadFromGPU(ktnGraphicsScenePtr /*eScene*/) override {}
    void WaitIdle() override;

protected:
    // Vulkan specific methods:
    void CreateInstance();
    void SetUpDebugCallback();
    void CreateSurface();
    void PickPhysicalDevice();
    auto CreateLogicalDevice() -> ExecutionStatus;
    auto CreateSwapchain() -> ExecutionStatus;
    auto CreateImageViews() -> ExecutionStatus;
    auto CreateRenderPass() -> ExecutionStatus;
    auto CreatePipeline() -> ExecutionStatus;
    auto CreateFramebuffers() -> ExecutionStatus;
    auto CreateCommandPool() -> ExecutionStatus;
    auto CreateCommandBuffers() -> ExecutionStatus;
    auto CreateSyncObjects() -> ExecutionStatus;

    void RecreateSwapchain();
    void CleanUpSwapchain();

    void Draw();

    // helper methods
    static auto CheckValidationLayer() -> ExecutionStatus;
    auto CheckPhysicalDevice(vk::PhysicalDevice device) -> ExecutionStatus;
    static auto CheckPhysicalDeviceExtensionsSupport(vk::PhysicalDevice device) -> ExecutionStatus;
    auto CheckPhysicalDeviceSwapchainSupport(vk::PhysicalDevice device) -> ExecutionStatus;
    auto CheckQueueFamilies(vk::PhysicalDevice device) -> ExecutionStatus;

    static auto ChooseSwapSurfaceFormat(const std::vector<vk::SurfaceFormatKHR> &supportedFormats) -> vk::SurfaceFormatKHR;
    static auto ChooseSwapchainPresentMode(const std::vector<vk::PresentModeKHR> &supportedModes) -> vk::PresentModeKHR;
    auto CreateShaderModule(const std::vector<char> &buffer) -> vk::ShaderModule; //!< for spv binaries loaded from disk
    auto CreateShaderModule(const std::vector<uint32_t> &buffer) -> vk::ShaderModule; //!< for spv binaries compiled online

    static auto GetRequiredExtentions() -> std::vector<const char *>;
    static auto GetShaderFromFile(const std::string &filepath) -> std::vector<char>;

    void HandleEvent_FramebufferSizeChanged(int width, int height) override;

    bool m_FramebufferSizeChanged = false;

    vk::Instance m_Instance;
    vk::PhysicalDevice m_PhysicalDevice;
    vk::Device m_LogicalDevice;
    vk::Queue m_GraphicsQueue;
    vk::Queue m_PresentQueue;
    QueueFamilies m_QueueFamilies;
    VkSurfaceKHR m_Surface{};
    vk::SwapchainKHR m_Swapchain;
    std::vector<vk::Image> m_SwapchainImages;
    std::vector<vk::ImageView> m_SwapchainImageViews;
    std::vector<vk::PresentModeKHR> m_SwapchainPresentModes;
    std::vector<vk::Framebuffer> m_SwapchainFramebuffers;
    vk::SurfaceCapabilitiesKHR m_SurfaceCapabilities;
    std::vector<vk::SurfaceFormatKHR> m_SurfaceFormats;

    vk::Format m_SwapchainImageFormat;
    vk::Extent2D m_SwapchainExtent = vk::Extent2D(0, 0);

    vk::RenderPass m_RenderPass;

    vk::PipelineCache m_PipelineCache;

    vk::PipelineLayout m_PipelineLayout;
    vk::Pipeline m_Pipeline;

    vk::CommandPool m_CommandPool;
    std::vector<vk::CommandBuffer> m_CommandBuffers;

    std::vector<vk::Semaphore> m_Semaphores_ImgAvailable;
    std::vector<vk::Semaphore> m_Semaphores_RenderDone;
    unsigned int m_CurrentFrame = 0;
    std::vector<vk::Fence> m_Fences;
    GLFWwindow *m_Window;
#ifdef KTN_DEBUG
    VkDebugUtilsMessengerEXT m_DebugUtilsMessenger;
#endif
};
} // namespace ktn::Graphics

#endif // KTNENGINE_GRAPHICS_KTNRENDERER_VULKAN_HPP
