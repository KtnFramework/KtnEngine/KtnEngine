#ifndef KTNENGINE_GRAPHICS_KTNJOINT_HPP
#define KTNENGINE_GRAPHICS_KTNJOINT_HPP
#include <ktnEngine/Core/ktn3DElement.hpp>

namespace ktn::Graphics {
class ktnJoint : public Core::ktn3DElement {
public:
    ktnJoint(const std::string &eName = "ktnJoint", ktn3DElement *eParent = nullptr); //!< The default constructor.

    /**
     * @brief UpdateAnimatedWorldSpaceModelMatrix overrides the base function by adding the multiplication with the inverse bind matrix at the end.
     * @details After updating the matrices for itself and all children recursively,
     * the animated world space model matrix is multiplied with the joint's inverse bind matrix.
     * Without this, the primitives bound to a joint would act as if it was first transformed with the original pose twice,
     * then transformed by the per-frame animation with a wrong pivot point.
     * The further the joint is from the root joint, the more drastically inaccurate this behavior is.
     */
    void UpdateAnimatedWorldSpaceModelMatrix() override;

    void SetInverseBindMatrix(const glm::mat4 &eInverseBindMatrix); //!< Sets the inverse bind matrix.

private:
    glm::mat4 m_InverseBind = IDENTITY_MATRIX_4; //!< This matrix transforms global space to joint space.
};
} // namespace ktn::Graphics
#endif // KTNENGINE_GRAPHICS_KTNJOINT_HPP
