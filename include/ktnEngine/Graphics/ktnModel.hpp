#ifndef KTNENGINE_GRAPHICS_KTNMODEL_GLTF_HPP
#define KTNENGINE_GRAPHICS_KTNMODEL_GLTF_HPP
#include "ktnAction.hpp"
#include "ktnEngine/Physics/ktnPhysicalObject.hpp"
#include "ktnMesh.hpp"

#include <gleb/gltf.hpp>
#include <ktnSignalSlot/ktnSignalSlot.hpp>

#include <filesystem>
#include <string>

namespace ktn::Graphics {
typedef struct ktnBoundingBoxMeasurements {
    glm::vec3 Max = glm::vec3(std::numeric_limits<float>::min());
    glm::vec3 Median;
    glm::vec3 Min = glm::vec3(std::numeric_limits<float>::max());
} ktnBoundingBoxMeasurements;

ktnBoundingBoxMeasurements operator*(ktnBoundingBoxMeasurements eMeasurement, glm::vec3 eScale);

class ktnModel : public Core::ktn3DElement {
public:
    explicit ktnModel(const std::string &ePath, const std::string &eName = "ktnModel");
    ~ktnModel() override;

    auto operator==(const ktnModel &eModel) -> bool;
    auto operator!=(const ktnModel &eModel) -> bool;
    auto operator=(const ktnModel &eModel) -> ktnModel & = default;

    [[nodiscard]] auto BoundingBox() -> ktnBoundingBoxMeasurements;

    void Clear();
    void InitializePhysicalObject(float density, bool shouldMove);
    void LoadFromDisk();
    void StartAction(std::string eActionName);
    void StopAction(std::string eActionName);

    /**
     * @brief Meshes are meshes loaded from the .gltf file, including their indices.
     * The indices are necessary, as they are used to map animations/materials.
     */
    std::map<size_t, ktnMesh *> Meshes;
    std::map<size_t, ktnSkin *> Skins;

    std::shared_ptr<Physics::ktnPhysicalObject> PhysicalObject;

    std::map<size_t, std::shared_ptr<ktnAction>> Actions;
    std::vector<std::shared_ptr<ktnAction>> CurrentActions;

    std::filesystem::path Path;

private:
    ktnBoundingBoxMeasurements m_BoundingBox;
    bool m_IsLoadedToGPU = false;
};
} // namespace ktn::Graphics

#endif // KTNENGINE_GRAPHICS_KTNMODEL_GLTF_HPP
