#ifndef KTNENGINE_GRAPHICS_KTNGRAPHICSCOMMON_HPP
#define KTNENGINE_GRAPHICS_KTNGRAPHICSCOMMON_HPP
#include <ktnEngine/ktnCore>

#include <glm/glm.hpp>

#include <vulkan/vulkan.hpp>

#include <map>
#include <string>

namespace ktn {
namespace Graphics {
namespace StandardShaderString {
const inline static std::string DeferredLight("DeferredLight");
const inline static std::string GBuffer("GBuffer");
} // namespace StandardShaderString

enum class CameraMode { FREELOOK, TRACKING };

enum class CameraType { FIRSTPERSON, THIRDPERSONFREE, THIRDPERSONOVERSHOULDER, FLIGHT, SUPERVISION };

enum class FrameBufferType { DEPTH, COLOR_RGB, G_BUFFER };

enum class GraphicsLevels { NONE, LOW, MEDIUM, HIGH };

enum class LightType { POINT, DIRECTIONAL, SPOT };

enum class ShaderPass { SHADOW, COLOR, G_BUFFER, POSTPROCESS };

enum class ShaderType { VERTEX, FRAGMENT, GEOMETRY };

static std::map<ShaderType, std::string> ShaderTypeString = {
    {ShaderType::VERTEX, "vertex"}, //
    {ShaderType::FRAGMENT, "fragment"},
    {ShaderType::GEOMETRY, "geometry"} // clang-format
};

enum class TextureType { //
    G_BUFFER_POSITION,
    G_BUFFER_NORMAL,
    G_BUFFER_DIFFUSE,
    G_BUFFER_TEXCOORDS,
    DIFFUSE,
    SPECULAR,
    HEIGHT,
    NORMAL,
    ROUGHNESS,
    BASECOLOR,
    METALLIC,
    EMISSIONCOLOR,
    DEPTH
};

typedef struct {
    glm::vec3 Position;
    glm::vec3 Normal;
    glm::vec2 TextureCoordinates;
} ktnVertex;

enum class ktnGraphicsBackend { OPENGL, VULKAN };

namespace Defaults {
const inline static glm::vec4 BaseColorFactor(0.5);
const inline static float MetallicFactor(0.5);
const inline static float RoughnessFactor(0.5);

const inline static float Camera_Sensitivity(0.1F);

/**
 * @brief Default_LightCutoff_InnerAngle is the default inner angle for the light cutoff class.
 * The unit is [degree].
 * The default value of 12.5 degree corresponds to a 25 degree opening.
 */
const inline static float LightCutoff_InnerAngle(12.5F);

/**
 * @brief Default_LightCutoff_OuterAngle is the default outer angle for the light cutoff class.
 * The unit is [degree].
 * The default value of 17.5 degree corresponds to a 35 degree opening.
 */
const inline static float LightCutoff_OuterAngle(17.5F);
} // namespace Defaults
} // namespace Graphics
} // namespace ktn
#endif // KTNENGINE_GRAPHICS_KTNGRAPHICSCOMMON_HPP
