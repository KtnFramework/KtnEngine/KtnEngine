#ifndef KTNENGINE_GRAPHICS_KTNGRAPHICSUTILS_HPP
#define KTNENGINE_GRAPHICS_KTNGRAPHICSUTILS_HPP

namespace ktn::Graphics::Utils {
long FrameDuration(); //!< The time at which the last frame finish rendering [us since epoch].
long FrameTime(); //!< the time it took the last frame to render [us].
void SetFrameDuration(long);
void SetFrameTime(long);
} // namespace ktn::Graphics::Utils

#endif // KTNENGINE_GRAPHICS_KTNGRAPHICSUTILS_HPP
