#ifndef KTNENGINE_GRAPHICS_KTNFRAMEBUFFER_HPP
#define KTNENGINE_GRAPHICS_KTNFRAMEBUFFER_HPP
#include "ktnGraphicsCommon.hpp"
#include "ktnTexture.hpp"

#include <vector>

namespace ktn::Graphics {
class ktnFrameBuffer {
public:
    ktnFrameBuffer();
    ~ktnFrameBuffer();

    std::vector<std::shared_ptr<ktnTexture>> Textures;

    unsigned int ID{};
    vk::Extent2D Extent2D;
};
} // namespace ktn::Graphics
#endif // KTNENGINE_GRAPHICS_KTNFRAMEBUFFER_HPP
