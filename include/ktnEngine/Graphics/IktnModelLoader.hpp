#ifndef KTNENGINE_GRAPHICS_IKTNMODELLOADER_HPP
#define KTNENGINE_GRAPHICS_IKTNMODELLOADER_HPP
#include "ktnAction.hpp"
#include "ktnMesh.hpp"
#include "ktnSkin.hpp"

namespace ktn::Graphics {
class IktnModelLoader {
public:
    virtual ~IktnModelLoader() = 0;
    virtual std::map<size_t, std::shared_ptr<ktnAction>> LoadActions() = 0;
    virtual std::map<size_t, ktnMesh *> LoadMeshes(Core::ktn3DElement *eParent) = 0;
    virtual std::map<size_t, ktnSkin *> LoadSkins() = 0;
};
} // namespace ktn::Graphics

#endif // KTNENGINE_GRAPHICS_IKTNMODELLOADER_HPP
