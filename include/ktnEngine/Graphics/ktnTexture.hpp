#ifndef KTNENGINE_GRAPHICS_KTNTEXTURE_HPP
#define KTNENGINE_GRAPHICS_KTNTEXTURE_HPP
#include "ktnGraphicsCommon.hpp"

#include <filesystem>

namespace ktn::Graphics {
class ktnTexture {
public:
    /**
     * @brief ktnTexture generates a texture for a frame buffer.
     * @param eExtent2D is the 2D extent of the texture.
     */
    ktnTexture(const vk::Extent2D &eExtent2D);
    ktnTexture(const std::filesystem::path &ePath);
    ktnTexture(ktnTexture &&other); //!< The move constructor.
    ~ktnTexture();

    auto operator==(const ktnTexture &eTexture) -> bool;
    auto operator!=(const ktnTexture &eTexture) -> bool;

    static void SetTextureParameter(unsigned int target, unsigned int paramName, int param);

    void CreateOnGPU(const TextureType &eType);

    // WIP separate into subclasses, the image can be generated in-memory or loaded from disk.
    std::filesystem::path m_Path;

    TextureType m_Type;
    unsigned int ID = 0;
    vk::Extent2D m_Extent2D;
    uint8_t m_nChannels = 0;
};
} // namespace ktn::Graphics
#endif // KTNENGINE_GRAPHICS_KTNTEXTURE_HPP
