#ifndef KTNENGINE_GRAPHICS_KTNLIGHT_HPP
#define KTNENGINE_GRAPHICS_KTNLIGHT_HPP
#include "ktnGraphicsCommon.hpp"

#include <glm/glm.hpp>

#include <cmath>

namespace ktn::Graphics {
/**
 * @brief The ktnLight class describes the 3D light object.
 */
class ktnLight : public Core::ktn3DElement {
public:
    /**
     * @brief The hCutoff class describes the light cutoff parameters.
     */
    class hCutoff {
    public:
        /**
         * @brief hCutoff is the default constructor.
         */
        explicit hCutoff(const float &eInnerAngle = Defaults::LightCutoff_InnerAngle, const float &eOuterAngle = Defaults::LightCutoff_OuterAngle);

        auto operator==(const hCutoff &eCutoff) const -> bool;
        auto operator!=(hCutoff eCutoff) const -> bool;

        /**
         * @brief InnerCos
         * @return the cosinus of the inner angle.
         */
        [[nodiscard]] inline auto InnerCos() const -> float {
            return m_Inner;
        }

        /**
         * @brief OuterCos
         * @return the cosinus of the outer angle.
         */
        [[nodiscard]] inline auto OuterCos() const -> float {
            return m_Outer;
        }

        /**
         * @brief SetInnerAngle sets the inner angle to eInnerAngle degree
         * @param eAngle is the angle to be set, in degree.
         */
        inline void SetInnerAngle(const float &eAngle) {
            m_Inner = glm::cos((glm::radians(eAngle)));
        }

        /**
         * @brief SetOuterAngle sets the inner angle to eAngle degree
         * @param eAngle is the angle to be set, in degree.
         */
        inline void SetOuterAngle(const float &eAngle) {
            m_Outer = glm::cos(glm::radians(eAngle));
        }

    private:
        float m_Inner;
        float m_Outer;
    }; // class hCutoff

    /**
     * @brief The hFalloff class describes the light falloff parameters.
     * @details The members of this class are based on this equation: \n
     * <center>\f$I_d = \frac{I_0} {1 + Ld + Qd^2}\f$, whereas:</center>
     * \f$d\f$ is the world-coordinate distance from the light source,\n
     * \f$I_d\f$ is the resulting intensity of the light at distance d,\n
     * \f$I_0\f$ is the intensity of the light at distance 0\n
     * \f$L\f$ and \f$Q\f$ are adjustment parameters to control the falloff curve.
     * \f$L\f$ is called the linear term, and \f$Q\f$ is called the quadratic term.\n
     * \n
     * This equation is loosely based on the physical inverse square law:\n
     * <center>\f$I_d = \frac{I_0}{d ^ 2}\f$</center>
     * which has 2 practical problems\n
     * - There is a singularity at \f$d = 0\f$.
     * When the light approaches an object, it gets infinitely bright.
     * Therefore the constant term 1 is added, so that \f$\lim \limits_{d \to 0} I_d = I_0\f$.
     * - The lighting of the scene is used to induce its mood regardless of physical accuracy.
     * The linear and quadratic terms are added to give the level designer more control over this.
     */
    class hFalloff {
    public:
        /**
         * @brief hFalloff is the default constructor.
         * @param eLinearTerm is the linear term.
         * @param eQuadraticTerm is the quadratic term.
         */
        explicit hFalloff(const float &eLinearTerm = 0.0F, const float &eQuadraticTerm = 1.0F);

        auto operator==(hFalloff eFalloff) const -> bool;
        auto operator!=(hFalloff eFalloff) const -> bool;

        float LinearTerm; //!< The linear term
        float QuadraticTerm; //!< The quadratic term
    }; // class hFalloff

    explicit ktnLight(const std::string &eName = "ktnLight"); //!< The default constructor.
    ktnLight(ktnLight const &other);
    ktnLight(ktnLight &&other);

    ~ktnLight() override; //!< The default destructor.

    auto operator=(ktnLight const &other) -> ktnLight &;
    auto operator=(ktnLight &&other) -> ktnLight &;
    auto operator==(const ktnLight &eLight) const -> bool;
    auto operator!=(const ktnLight &eLight) const -> bool;

    [[nodiscard]] auto Color() const -> glm::vec3; //!< Get the light color
    [[nodiscard]] auto Direction() const -> glm::vec3; //!< Get the light direction
    [[nodiscard]] auto InfluenceRadius() const -> float; //!< Get the influence radius

    void SetColor(const glm::vec3 &col); //!< Set the light color
    void SetColor(const float &r, const float &g, const float &b); //!< Set the light color
    void SetDirection(const glm::vec3 &dir); //!< Set the light direction

    /**
     * @brief Calculate the maximum distance at which a point light has influence.
     * @details Assuming \f$ Q \neq 0 \f$:
     * <center>\f$ d = \frac{-L + \sqrt{L^2 - 4Q(1-256*I_0I_c)}}{2Q} \f$</center>
     * This formula is derived as follows:
     * - The intensity at distance \f$d\f$ is calculated using the equation
     * \f$I_d = \frac{I_0} {1 + Ld + Qd^2}\f$ (see details [here](\ref hFalloff))
     * - For a bit depth of 8, the minimum influence a light can have over a surface is \f$I_{min} = \frac{1}{2^8} = \frac{1}{256}\f$
     * - \f$I_d = I_{min} \Longleftrightarrow 256*I_0 = 1 + Ld + Qd^2\f$
     * - Solving this equation for \f$d\f$ yields the influence radius when the light color is white (1.0, 1.0, 1.0)
     * - Lights with color values less than 1.0 must have less influence, and the equations can be modified by adding a term
     * \f$I_c = max(color.r, color.g, color.b)\f$:
     * \f$256*I_0I_c = 1 + Ld + Qd^2\f$
     * - Solving this equation for \f$d \geq 0\f$ yields: \f$d = \frac{-L + \sqrt{\Delta}}{2Q}\f$, whereas \f$\Delta = L^2 - 4Q(1-256*I_0I_c)\f$.
     */
    void CalculateInfluenceRadius();

    hCutoff Cutoff; //!< The cutoff parameters
    hFalloff Falloff; //!< The falloff parameters
    float Intensity = 1.0F; //!< The light intensity
    bool IsCastingShadows = false;
    glm::mat4 ProjectionMatrix = IDENTITY_MATRIX_4; //!< The light projection matrix used for shadow casting
    LightType Type = LightType::POINT; //!< The light type
    glm::mat4 ViewMatrix = IDENTITY_MATRIX_4; //!< The view matrix from the light's pov

private:
    glm::vec3 m_Color = glm::vec3(1.0, 1.0, 1.0); //!< The light color
    glm::vec3 m_Direction = glm::vec3(0.0F, 0.0F, -1.0F); //!< The light direction used for shadow casting

    /**
     * @brief m_InfluenceRadius is the radius of the spherical volume around a point light that can be influenced by it.
     * This can be calculated based on its color, intensity, falloff parameters and precision.
     * @see CalculateInfluenceRadius
     * It is then used as the scaling factor for the light sphere object during deferred shading.
     */
    float m_InfluenceRadius{};

}; // class ktnLight
} // namespace ktn::Graphics
#endif // KTNENGINE_GRAPHICS_KTNLIGHT_HPP
