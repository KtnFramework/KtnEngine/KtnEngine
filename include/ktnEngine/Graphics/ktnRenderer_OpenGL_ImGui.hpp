#ifndef KTNENGINE_GRAPHICS_KTNRENDERER_OPENGL_IMGUI_HPP
#define KTNENGINE_GRAPHICS_KTNRENDERER_OPENGL_IMGUI_HPP
#include "ktnRenderer_OpenGL.hpp"

#include <epoxy/gl.h>

#include <GLFW/glfw3.h>

namespace ktn::Graphics {
class ktnRenderer_OpenGL_ImGui : public ktnRenderer_OpenGL {
public:
    ktnRenderer_OpenGL_ImGui();

    void SetContext(void *eContext) override;
    void Initialize(const vk::Extent2D &eExtent2D, unsigned int shadowMapSize) override;
    void RenderGUI(ktnGraphicsScenePtr scene) override;

private:
    GLFWwindow *m_Window;
};
} // namespace ktn::Graphics
#endif // KTNENGINE_GRAPHICS_KTNRENDERER_OPENGL_IMGUI_HPP
