#ifndef KTNENGINE_MATH_KTNQUATERNION_HPP
#define KTNENGINE_MATH_KTNQUATERNION_HPP
#include <ktnEngine/Core/ktnCoreCommon.hpp>

namespace ktn::Math {
class ktnQuaternion {
public:
    enum class ComponentBasis { Real, I, J, K };

    /**
     * @brief The Component class describes the individual components of a quaternion,
     * namely its basis (real, i, j, k) and scale.
     */
    struct Component {
    public:
        Component(const ComponentBasis &basis, const float &scale);

        const ComponentBasis Basis;
        float Scale = 0;
    };

    /**
     * @brief The ComponentReal class describes the real component of a quaternion.
     */
    class ComponentReal : public Component {
    public:
        explicit ComponentReal(const float &scale) noexcept;
        ComponentReal(const ComponentReal &c);
        virtual ~ComponentReal();

        auto operator=(const ComponentReal &c) -> ComponentReal &;
        auto operator+(const ComponentReal &c) const -> ComponentReal;
        auto operator+(const float &addend) const -> ComponentReal;
        auto operator*(const float &scale) const -> ComponentReal;
    };

    /**
     * @brief The ComponentI class describes the component of a quaternion along the i basis.
     */
    class ComponentI : public Component {
    public:
        explicit ComponentI(const float &scale) noexcept;
        ComponentI(const ComponentI &c);
        virtual ~ComponentI();

        auto operator=(const ComponentI &c) -> ComponentI &;
        auto operator+(const ComponentI &c) const -> ComponentI;
        auto operator*(const float &scale) const -> ComponentI;
    };

    /**
     * @brief The ComponentJ class describes the component of a quaternion along the j basis.
     */
    class ComponentJ : public Component {
    public:
        explicit ComponentJ(const float &scale) noexcept;
        ComponentJ(const ComponentJ &c);
        virtual ~ComponentJ();

        auto operator=(const ComponentJ &c) -> ComponentJ &;
        auto operator+(const ComponentJ &c) const -> ComponentJ;
        auto operator*(const float &scale) const -> ComponentJ;
    };

    /**
     * @brief The ComponentK class describes  the component of a quaternion along the k basis.
     */
    class ComponentK : public Component {
    public:
        explicit ComponentK(const float &scale) noexcept;
        ComponentK(const ComponentK &c);
        virtual ~ComponentK();

        auto operator=(const ComponentK &c) -> ComponentK &;
        auto operator+(const ComponentK &c) const -> ComponentK;
        auto operator*(const float &scale) const -> ComponentK;
    };

    ktnQuaternion();
    ktnQuaternion(const ktnQuaternion &q);
    ktnQuaternion(const ComponentReal &real, const ComponentI &i, const ComponentJ &j, const ComponentK &k);
    ktnQuaternion(float real, float i, float j, float k);
    ~ktnQuaternion();

    auto operator=(const ktnQuaternion &eQuaternion) -> ktnQuaternion &;
    auto operator+(const ktnQuaternion &eQuaternion) const -> ktnQuaternion;

    // All of these multiplications are right multiplications,
    // except with the real component, where a left multiplication has the same result as a right multiplication.
    auto operator*(const ComponentI &c) const -> ktnQuaternion;
    auto operator*(const ComponentJ &c) const -> ktnQuaternion;
    auto operator*(const ComponentK &c) const -> ktnQuaternion;
    auto operator*(const ComponentReal &c) const -> ktnQuaternion;
    auto operator*(const float &scale) const -> ktnQuaternion;

    ComponentReal W = ComponentReal(0);
    ComponentI X = ComponentI(0);
    ComponentJ Y = ComponentJ(0);
    ComponentK Z = ComponentK(0);

    void Set(const ComponentReal &real, const ComponentI &i, const ComponentJ &j, const ComponentK &k);
    void Set(const float &real, const float &i, const float &j, const float &k);

    auto Equals(const ktnQuaternion &q, const float &tolerance = ComparisonThreshold_Float) -> bool;
    [[nodiscard]] auto Conjugated() const -> ktnQuaternion;
    [[nodiscard]] auto Norm() const -> float;

    inline static const ComponentReal BasisReal = ComponentReal(1.0F);
    inline static const ComponentI BasisI = ComponentI(1.0F);
    inline static const ComponentJ BasisJ = ComponentJ(1.0F);
    inline static const ComponentK BasisK = ComponentK(1.0F);
};

////////////
// utilities
// left multiplications for quaternions
auto operator*(float scale, const ktnQuaternion &q) -> ktnQuaternion;
auto operator*(const ktnQuaternion::ComponentReal &basis, const ktnQuaternion &q) -> ktnQuaternion;
auto operator*(const ktnQuaternion::ComponentI &basis, const ktnQuaternion &q) -> ktnQuaternion;
auto operator*(const ktnQuaternion::ComponentJ &basis, const ktnQuaternion &q) -> ktnQuaternion;
auto operator*(const ktnQuaternion::ComponentK &basis, const ktnQuaternion &q) -> ktnQuaternion;

struct QuaternionNormalizationResult {
    ktn::ExecutionStatus Status;
    ktnQuaternion Quaternion;
};

auto Normalize(const ktnQuaternion &q) -> QuaternionNormalizationResult;
} // namespace ktn::Math
#endif // KTNENGINE_MATH_KTNQUATERNION_HPP
