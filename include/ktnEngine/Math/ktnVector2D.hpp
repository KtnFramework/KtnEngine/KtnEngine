#ifndef KTNENGINE_MATH_KTNVECTOR2D_HPP
#define KTNENGINE_MATH_KTNVECTOR2D_HPP
#include "ktnVector.hpp"
namespace ktn::Math {
class ktnVector2D : public ktnVector<2> {
public:
    explicit ktnVector2D(const float &x, const float &y) : ktnVector<2>({x, y}) {}
    ~ktnVector2D() override = default;

    [[nodiscard]] inline auto X() const -> float {
        return (*this)[0];
    }

    [[nodiscard]] inline auto Y() const -> float {
        return (*this)[1];
    }

    virtual void SetX(const float &x);
    virtual void SetY(const float &y);
};

inline auto Angle(const ktn::AngleUnit &eUnit, const ktnVector2D &vec1, const ktnVector2D &vec2) -> float {
    return Angle<2>(eUnit, vec1, vec2);
}

inline auto DotProduct(const ktnVector2D &vec1, const ktnVector2D &vec2) -> float {
    return DotProduct<2>(vec1, vec2);
}
} // namespace ktn::Math
#endif // KTNENGINE_MATH_KTNVECTOR2D_HPP
