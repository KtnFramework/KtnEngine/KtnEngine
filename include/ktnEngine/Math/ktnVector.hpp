#ifndef KTNENGINE_MATH_KTNVECTOR_HPP
#define KTNENGINE_MATH_KTNVECTOR_HPP
#include <ktnEngine/ktnCore>

#include "ktnMathCommon.hpp"

#include <algorithm>
#include <array>
#include <iostream>
#include <vector>

namespace ktn::Math {
template <const size_t size>
class ktnVector {
public:
    ktnVector() {
        static_assert(size > 0, "A ktnVector must have at least one component.");
        for (size_t i = 0; i < size; ++i) { m_Data[i] = 0.0F; }
    }

    ktnVector(const ktnVector &eOther) {
        for (size_t i = 0; i < size; ++i) { m_Data[i] = eOther[i]; }
    }

    explicit ktnVector(const std::array<float, size> &eInitList) {
        for (size_t i = 0; i < size; ++i) { m_Data[i] = eInitList[i]; }
    }

    virtual ~ktnVector() = default;

    virtual auto operator[](const size_t &index) const -> float {
        assert(index < size);
        return m_Data[index];
    }

    virtual auto operator+(const ktnVector &eOther) -> ktnVector & {
        for (size_t i = 0; i < size; ++i) { m_Data[i] += eOther[i]; }
        return *this;
    }

    virtual auto operator*(const float &eScalar) -> ktnVector & {
        for (auto &component : m_Data) { component *= eScalar; }
        return *this;
    }

    [[nodiscard]] inline auto Angle(const ktn::AngleUnit &eUnit, const ktnVector &eOther) const -> float {
        const float mag1 = Magnitude();
        const float mag2 = eOther.Magnitude();

        // The angle between a vector and a null vector is defined as 0.
        // To reduce numerical instability, very short vectors are also treated as null vectors.
        if (mag1 < ktn::ComparisonThreshold_Float || mag2 < ktn::ComparisonThreshold_Float) { return 0.0F; }

        switch (eUnit) {
        case ktn::AngleUnit::degree:
            return RadianToDegree(acosf(DotProduct(eOther) / mag1 / mag2));
        case ktn::AngleUnit::radian:
            return acosf(DotProduct(eOther) / mag1 / mag2);
        }
        throw std::runtime_error(std::string(__FUNCTION__) + ": Unknown angle unit.");
    }

    [[nodiscard]] inline auto DotProduct(const ktnVector &eOther) const -> float {
        float sum = 0;
        for (size_t i = 0; i < size; ++i) { sum += m_Data[i] * eOther[i]; }
        return sum;
    }

    [[nodiscard]] inline auto Size() const -> size_t {
        return size;
    }

    [[nodiscard]] inline auto Magnitude() const -> float {
        float sum = 0;
        constexpr float exponent(2.0F);
        for (auto &component : m_Data) { sum += powf(component, exponent); }
        return sqrtf(sum);
    }

    // TODO find better name for this
    auto Set(const size_t &index, const float &value) -> ktn::ExecutionStatus {
        if (index < size) {
            m_Data[index] = value;
            return ktn::ExecutionStatus::OK;
        }
        std::cerr //
            << "The given index (" << index //
            << ") does not belong in a ktnVector of size " << size //
            << "." << std::endl;
        return ktn::ExecutionStatus::FAILURE;
    }

    /**
     * @brief Normalize
     * @return the unit vector of the direction to which the vector is pointing.
     * Note that a magnitude of 0 will result in undefined behavior, as the null vector cannot be normalized.
     * If such is the case, throw an exception to crash the program as early as possible, while printing an error message.
     */
    inline void Normalize() {
        auto currentMagnitude = Magnitude();
        if (currentMagnitude < ktn::ComparisonThreshold_Float) {
            std::cerr << "ktnVector::Normalize: the vector has a magnitude of " //
                      << currentMagnitude //
                      << ", which is smaller than the currently set comparison threshold for floating point numbers." //
                      << std::endl;
            throw std::domain_error("ktnVector::Normalize: degenerative case");
        }
        std::transform(m_Data.begin(), m_Data.end(), m_Data.begin(), [&currentMagnitude](float element) -> float { return element / currentMagnitude; });
    }

protected:
    std::array<float, size> m_Data = std::array<float, size>{0.0F};
}; // class ktnVector

/**
 * @brief Angle
 * @param unit
 * @param vec1
 * @param vec2
 * @return the angle between vec1 and vec2, in the unit
 */
template <size_t size>
auto Angle(const ktn::AngleUnit &unit, const ktnVector<size> &vec1, const ktnVector<size> &vec2) -> float {
    return vec1.Angle(unit, vec2);
}

template <size_t size>
inline auto DotProduct(const ktnVector<size> &vec1, const ktnVector<size> &vec2) -> float {
    return vec1.DotProduct(vec2);
}

} // namespace ktn::Math
#endif // KTNENGINE_MATH_KTNVECTOR_HPP
