#ifndef KTNENGINE_MATH_KTNEULERANGLES_HPP
#define KTNENGINE_MATH_KTNEULERANGLES_HPP
#include "ktnMathCommon.hpp"

namespace ktn::Math {
/**
 * @brief The ktnEulerAngles class describes the orientation of a rigid body using the
 * Tait–Bryan variant of the Euler angles, namely pitch, roll and yaw.
 */
class ktnEulerAngles {
public:
    /**
     * @brief The default constructor.
     * @param eUnit is the unit of each angle.
     * @param ePitch is the initiated pitch angle.
     * @param eRoll is the initiated roll angle.
     * @param eYaw is the initiated yaw angle.
     */
    explicit ktnEulerAngles( //
        const AngleUnit &eUnit = AngleUnit::degree,
        const float &ePitch = 0.0F,
        const float &eRoll = 0.0F,
        const float &eYaw = 0.0F);
    ktnEulerAngles(const ktnEulerAngles &angle);
    ktnEulerAngles(ktnEulerAngles &&angle) = default;
    virtual ~ktnEulerAngles() = default;

    auto operator==(const ktnEulerAngles &eAngles) const -> bool;
    auto operator!=(const ktnEulerAngles &eAngles) const -> bool;
    auto operator=(const ktnEulerAngles &eAngles) -> ktnEulerAngles &;
    auto operator=(ktnEulerAngles &&eAngles) -> ktnEulerAngles & = default;

    float Pitch = 0.0F;
    float Roll = 0.0F;
    float Yaw = 0.0F;
};
} // namespace ktn::Math
#endif // KTNENGINE_MATH_KTNEULERANGLES_HPP
