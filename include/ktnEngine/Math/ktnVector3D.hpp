#ifndef KTNENGINE_MATH_KTNVECTOR3D_HPP
#define KTNENGINE_MATH_KTNVECTOR3D_HPP
#include "ktnVector.hpp"

namespace ktn::Math {
class ktnVector3D : public ktnVector<3> {
public:
    ktnVector3D() = default;
    ktnVector3D(const float &x, const float &y, const float &z) : ktnVector({x, y, z}) {}

    [[nodiscard]] inline auto X() const -> float {
        return (*this)[0];
    }

    [[nodiscard]] inline auto Y() const -> float {
        return (*this)[1];
    }

    [[nodiscard]] inline auto Z() const -> float {
        return (*this)[2];
    }

    virtual void SetX(const float &x);
    virtual void SetY(const float &y);
    virtual void SetZ(const float &z);

    [[nodiscard]] auto CrossProduct(const ktnVector3D &eVector) const -> ktnVector3D;
};

inline auto Angle(const ktn::AngleUnit &eUnit, const ktnVector3D &vec1, const ktnVector3D &vec2) -> float {
    return Angle<3>(eUnit, vec1, vec2);
}

[[nodiscard]] inline auto CrossProduct(const ktnVector3D &vec1, const ktnVector3D &vec2) -> ktnVector3D {
    return vec1.CrossProduct(vec2);
}

[[nodiscard]] inline auto DotProduct(const ktnVector3D &vec1, const ktnVector3D &vec2) -> float {
    return DotProduct<3>(vec1, vec2);
}
} // namespace ktn::Math
#endif // KTNENGINE_MATH_KTNVECTOR3D_HPP
