#ifndef KTNENGINE_APPLICATION_KTNAPPLICATIONWINDOW_HPP
#define KTNENGINE_APPLICATION_KTNAPPLICATIONWINDOW_HPP
#include "ktnApplication.hpp"

struct GLFWwindow;

namespace ktn::Application {
class ktnApplicationWindow : public Core::ktnObject {
public:
    ktnApplicationWindow(const vk::Extent2D &eWindowSize);
    ~ktnApplicationWindow();

    void CleanUp();
    void HideCursor() const;
    void Initialize(std::shared_ptr<ktnApplication> eApplication);
    void ShowCursor() const;
    void Run();

    GLFWwindow *Window = nullptr;
    std::string WindowName = "ktnApplication";
    vk::Extent2D WindowSize;

private:
    void callback_Drop(GLFWwindow *window, int count, const char **paths);
    void callback_FrameBufferSizeChange(GLFWwindow *window, int width, int height);
    void callback_MouseClick(GLFWwindow *window, int button, int action, int mods);
    void callback_MouseMove(GLFWwindow *window, double xpos, double ypos);
    void SwapBuffers() const;

    ktnSignal<int, const char **> WindowSignal_ItemsDroppedIntoWindow;
    ktnSignal<int, int> WindowSignal_FrameBufferSizeChanged;
    ktnSignal<int, int, int> WindowSignal_MouseClicked;
    ktnSignal<double, double> WindowSignal_MouseMoved;

    std::shared_ptr<ktnApplication> m_Application;
};
} // namespace ktn::Application
#endif // KTNENGINE_APPLICATION_KTNAPPLICATIONWINDOW_HPP
