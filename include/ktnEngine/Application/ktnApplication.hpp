#ifndef KTNENGINE_APPLICATION_KTNAPPLICATION_HPP
#define KTNENGINE_APPLICATION_KTNAPPLICATION_HPP
#include "ktnEngine/Graphics/IktnRenderer.hpp"
#include "ktnScene.hpp"

namespace ktn::Application {
class ktnApplication : public Core::ktnObject {
public:
    ktnApplication(std::shared_ptr<Graphics::IktnRenderer> eRenderer);
    virtual ~ktnApplication();

    template <class RendererClass>
    static std::shared_ptr<ktnApplication> CreateApplicationWithDefaultRenderer() {
        static_assert(std::is_base_of<Graphics::IktnRenderer, RendererClass>::value, "Renderer must inherit IktnRenderer.");
        auto renderer = std::make_shared<RendererClass>();
        auto app = std::make_shared<ktnApplication>(renderer);
        return app;
    }

    [[nodiscard]] auto ActiveScene() -> std::shared_ptr<ktnScene>;

    void SetActiveScene(const std::shared_ptr<ktnScene> &scene);

    virtual void Initialize(const vk::Extent2D &eWindowSize);

    std::shared_ptr<Graphics::IktnRenderer> Renderer;
    unsigned int ShadowMapSize;

    ktnSignal<int, int> Signal_FrameBufferSizeChanged;
    ktnSignal<int, const char **> Signal_ItemsDroppedIntoWindow;
    ktnSignal<int, int, int> Signal_MouseClicked;
    ktnSignal<double, double> Signal_MouseMoved;

protected:
    std::shared_ptr<ktnScene> m_ActiveScene = nullptr;

}; // class ktnApplication
} // namespace ktn::Application
#endif // KTNENGINE_APPLICATION_KTNAPPLICATION_HPP
