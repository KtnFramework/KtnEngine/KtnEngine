#ifndef KTNENGINE_APPLICATION_KTNSCENE_HPP
#define KTNENGINE_APPLICATION_KTNSCENE_HPP
#include "IktnInputProcessor.hpp"

#include <ktnEngine/Graphics/ktnGraphicsScene.hpp>
#include <ktnEngine/Physics/ktnPhysicsScene.hpp>

namespace ktn::Application {
class ktnScene : public Core::ktnObject {
public:
    ktnScene() = delete;
    ktnScene(Graphics::ktnGraphicsScenePtr, Physics::ktnPhysicsScenePtr, std::string eName = "ktnScene");
    virtual ~ktnScene();

    [[nodiscard]] Graphics::ktnGraphicsScenePtr GraphicsScene();
    [[nodiscard]] Physics::ktnPhysicsScenePtr PhysicsScene();

    virtual void ProcessInput() {}
    std::shared_ptr<IktnInputProcessor> InputProcessor;

    virtual void HandleEvent_FrameBufferSizeChanged(int width, int height);
    virtual void HandleEvent_ItemsDroppedIntoWindow(int count, const char **paths);
    virtual void HandleEvent_MouseClicked(int button, int action, int mods) {}
    virtual void HandleEvent_MouseMoved(double xpos, double ypos) {}

protected:
    Graphics::ktnGraphicsScenePtr m_GraphicsScene;
    Physics::ktnPhysicsScenePtr m_PhysicsScene;
    float m_DeltaTime = 0.0;

    bool m_FirstMouse = true;
    float m_LastFrameTime = 0.0;
    double m_LastX = 0.0;
    double m_LastY = 0.0;
    bool m_LeftMouseIsClicking = false;
}; // ktnScene

typedef std::shared_ptr<ktnScene> ktnScenePtr;
} // namespace ktn::Application
#endif // KTNENGINE_APPLICATION_KTNSCENE_HPP
