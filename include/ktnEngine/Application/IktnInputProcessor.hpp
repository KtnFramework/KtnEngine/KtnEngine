#ifndef KTNENGINE_APPLICATION_IKTNINPUTPROCESSOR_HPP
#define KTNENGINE_APPLICATION_IKTNINPUTPROCESSOR_HPP
#include <ktnSignalSlot/ktnSignalSlot.hpp>

namespace ktn::Application {
class IktnInputProcessor : public ktnSignalReceiver {
public:
    virtual void ProcessInput() = 0;
};
} // namespace ktn::Application
#endif // KTNENGINE_APPLICATION_IKTNINPUTPROCESSOR_HPP
