#ifndef KTNENGINE_AUDIO_KTNAUDIO_HPP
#define KTNENGINE_AUDIO_KTNAUDIO_HPP
#include "ktnEngine/ktnCore"

#include <portaudio.h>
#include <vorbis/vorbisfile.h>

#include <atomic>
#include <string>

namespace ktn::audio {
class ktnAudio : public Core::ktnObject {
public:
    ktnAudio();
    ktnAudio(std::string path);
    ~ktnAudio() override;
    void Play();
    void Play(std::string path);
    void Loop();

private:
    void Setup();
    std::string m_Path;
    std::atomic<float> m_CurrentVolume{};
    float m_TargetVolume{};

    // for loading the file into memory
    OggVorbis_File m_VorbisFile{};

    // for streaming the audio
    PaStreamParameters m_OutputStreamParameters{};
    PaStream *m_Stream = nullptr;

    // is a static constant due to the output array initialization
    static const int m_FramesPerBuffer = 256;
}; // class ktnAudio
} // namespace ktn::audio
#endif // KTNENGINE_AUDIO_KTNAUDIO_HPP
