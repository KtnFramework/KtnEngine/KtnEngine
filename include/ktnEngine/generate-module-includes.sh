#!/bin/bash

declare -a APPLICATION_HEADERS
declare -a AUDIO_HEADERS
declare -a CORE_HEADERS
declare -a GRAPHICS_HEADERS
declare -a MATH_HEADERS
declare -a PHYSICS_HEADERS

for filename in $(find -name '*.hpp' | sort -n); do
    case $filename in
    ./Application*)
        APPLICATION_HEADERS+=("${filename#*./}")
        ;;
    ./Audio*)
        AUDIO_HEADERS+=("${filename#*./}")
        ;;
    ./Core*)
        CORE_HEADERS+=("${filename#*./}")
        ;;
    ./Graphics*)
        GRAPHICS_HEADERS+=("${filename#*./}")
        ;;
    ./Math*)
        MATH_HEADERS+=("${filename#*./}")
        ;;
    ./Physics*)
        PHYSICS_HEADERS+=("${filename#*./}")
        ;;
    esac
done

rm ktnApplication
for header in ${APPLICATION_HEADERS[*]}; do
    echo "#include \"$header\"" >> ktnApplication
done

rm ktnAudio
for header in ${AUDIO_HEADERS[*]}; do
    echo "#include \"$header\"" >> ktnAudio
done

rm ktnCore
for header in ${CORE_HEADERS[*]}; do
    echo "#include \"$header\"" >> ktnCore
done

rm ktnGraphics
for header in ${GRAPHICS_HEADERS[*]}; do
    echo "#include \"$header\"" >> ktnGraphics
done

rm ktnMath
for header in ${MATH_HEADERS[*]}; do
    echo "#include \"$header\"" >> ktnMath
done

rm ktnPhysics
for header in ${PHYSICS_HEADERS[*]}; do
    echo "#include \"$header\"" >> ktnPhysics
done

