#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ktnEngine/Math/ktnVector.hpp>

using namespace ktn::Math;
using ktn::AngleUnit;

TEST(ktnVector, constructors) {
    ktnVector<1> vector1;
    EXPECT_EQ(vector1[0], 0.0F);

    ktnVector<100> vector100;
    for (size_t i = 0; i < 100; ++i) { EXPECT_EQ(vector100[i], 0.0F); }

    ktnVector<10> vector10({0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
    for (size_t i = 0; i < 10; ++i) { EXPECT_EQ(vector10[i], i); }

    ktnVector<10> vector10_copy(vector10);
    for (size_t i = 0; i < 10; ++i) { EXPECT_EQ(vector10[i], vector10_copy[i]); }
}

TEST(ktnVector, operators) {
    // operator+
    ktnVector<5> vector5({0, 1, 2, 3, 4});
    ktnVector<5> vector5_copy(vector5);
    vector5 = vector5 + vector5_copy;

    for (size_t i = 0; i < 5; ++i) { EXPECT_EQ(vector5[i], 2 * i); }

    // operator*
    ktnVector<4> vector4({5, 6, 7, 8});
    constexpr float scalar = 6;
    vector4 = vector4 * scalar;
    for (size_t i = 0; i < 4; ++i) { EXPECT_EQ(vector4[i], scalar * (i + 5)); }
}

TEST(ktnVector, Angle) {
    ktnVector<3> vector3_1({1, 0, 0});
    ktnVector<3> vector3_2({0, 0, 1});
    EXPECT_EQ(vector3_1.Angle(AngleUnit::degree, vector3_2), 90);
    EXPECT_FLOAT_EQ(vector3_1.Angle(AngleUnit::radian, vector3_2), static_cast<float>(M_PI / 2.0));

    ktnVector<2> vector2_1({0, 0});
    ktnVector<2> vector2_2({60, 3});
    EXPECT_EQ(vector2_1.Angle(AngleUnit::degree, vector2_2), 0);
    EXPECT_EQ(vector2_1.Angle(AngleUnit::radian, vector2_2), 0);
    // TODO add more tests for angles
}

TEST(ktnVector, DotProduct) {
    ktnVector<3> vector3_1({1, 2, 3});
    ktnVector<3> vector3_2({4, 5, 6});
    EXPECT_EQ(vector3_1.DotProduct(vector3_2), 1 * 4 + 2 * 5 + 3 * 6);
}

TEST(ktnVector, Magnitude) {
    ktnVector<3> vector3_1({0, 3, 4});
    EXPECT_EQ(vector3_1.Magnitude(), 5);
}

TEST(ktnVector, Normalize) {
    ktnVector<3> v({3, 0, 0});
    v.Normalize();
    EXPECT_EQ(v[0], 1);

    v = ktnVector<3>({0, 0, 0});
    EXPECT_ANY_THROW(v.Normalize());

    v = ktnVector<3>({6, 8, 3});
    auto m = v.Magnitude();
    v.Normalize();
    EXPECT_FLOAT_EQ(v[0], 6.0F / m);
}

TEST(ktnVector, Size) {
    ktnVector<1> vector1;
    EXPECT_EQ(vector1.Size(), 1);
    ktnVector<2> vector2;
    EXPECT_EQ(vector2.Size(), 2);
    ktnVector<3> vector3;
    EXPECT_EQ(vector3.Size(), 3);
    ktnVector<4> vector4;
    EXPECT_EQ(vector4.Size(), 4);
    ktnVector<5> vector5;
    EXPECT_EQ(vector5.Size(), 5);
}

TEST(ktnVector, Set) {
    ktnVector<6> vector6;
    vector6.Set(1, 1.1F);
    EXPECT_FLOAT_EQ(vector6[1], 1.1F);
    auto result = vector6.Set(6, 6.6F);
    EXPECT_EQ(result, ktn::ExecutionStatus::FAILURE);
}
