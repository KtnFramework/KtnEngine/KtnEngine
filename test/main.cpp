﻿#include <gtest/gtest.h>

class MinimalistPrinter : public ::testing::EmptyTestEventListener {
public:
    MinimalistPrinter(TestEventListener *eDefaultListener) {
        m_DefaultListener = eDefaultListener;
    }
    virtual void OnTestPartResult(const ::testing::TestPartResult &test_part_result) {
        if (test_part_result.failed()) { m_DefaultListener->OnTestPartResult(test_part_result); }
    }

    virtual void OnTestIterationStart(const ::testing::UnitTest &unit_test, int iteration) {
        m_DefaultListener->OnTestIterationStart(unit_test, iteration);
    }

    virtual void OnTestIterationEnd(const ::testing::UnitTest &unit_test, int iteration) {
        m_DefaultListener->OnTestIterationEnd(unit_test, iteration);
    }

private:
    TestEventListener *m_DefaultListener;
};

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    // Gets hold of the event listener list.
    ::testing::TestEventListeners &listeners = ::testing::UnitTest::GetInstance()->listeners();
    // Adds a listener to the end.  googletest takes the ownership.
    auto default_listener = listeners.Release(listeners.default_result_printer());

    MinimalistPrinter printer(default_listener);

    listeners.Append(new MinimalistPrinter(default_listener));
    return RUN_ALL_TESTS();
}
