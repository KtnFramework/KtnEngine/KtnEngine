﻿#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ktnEngine/Core/ktnObject.hpp>

using namespace ktn::Core;

TEST(ktnObject, constructors_destructor) {
    ktnObject obj_default;
    EXPECT_EQ(obj_default.Name.ToStdString() == "ktnObject", true);
    ktnObject obj_noDefault("non-generic name");
    EXPECT_EQ(obj_noDefault.Name.ToStdString() == "nongenericname", true);
}

TEST(ktnObject, copy_assignment_operator) {
    ktnObject obj1("non generic name");

    ktnObject obj2;
    EXPECT_NE(obj2, obj1);

    obj2 = obj1;
    EXPECT_EQ(obj2, obj1);
}

TEST(ktnObject, operators) {
    ktnObject obj_default;
    EXPECT_EQ(obj_default == ktnObject(), true);
    ktnObject obj_noDefault("non_generic name");
    EXPECT_EQ(obj_noDefault != ktnObject(), true);
    obj_noDefault = obj_default;
    EXPECT_EQ(obj_noDefault == ktnObject(), true);
}
