﻿#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ktnEngine/Core/ktnName.hpp>

using namespace ktn::Core;

TEST(ktnName, constructors_destructor) {
    // toStdString is also tested here.
    // The test will fail if either that method or any of the constructors don't act nice.
    ktnName name_default;
    EXPECT_EQ(name_default, "unnamed");

    ktnName name_noDefault("generic name");
    EXPECT_EQ(name_noDefault, "genericname");

    ktnName name_copied(name_noDefault);
    EXPECT_EQ(name_copied, "genericname");

    ktnName name_weird(" a");
    EXPECT_EQ(name_weird, "a");
    name_weird = "\\";
    EXPECT_EQ(name_weird, "unnamed");
    name_weird = "2B";
    EXPECT_EQ(name_weird, "B");

    std::string nameString("mot hai ba bon");
    ktnName name_from_string(nameString);
    EXPECT_EQ(name_from_string, "mothaibabon");
}

TEST(ktnName, copy_assignment_operator) {
    ktnName name1("generic name");
    ktnName name2;
    EXPECT_NE(name2, name1);
    name2 = name1;
    EXPECT_EQ(name2, name1);
}

TEST(ktnName, operators) {
    // == and !=
    ktnName name1("generic name");
    ktnName name2("generic name");
    EXPECT_EQ(name1 == "genericname", true);
    EXPECT_EQ(name1 != "genericname", false);
    EXPECT_EQ(name1 == "genericname1", false);
    EXPECT_EQ(name1 != "genericname1", true);

    EXPECT_EQ(name1 == name2, true);
    EXPECT_EQ(name1 != name2, false);
    ktnName name3;
    ktnName name4;
    EXPECT_EQ(name3 == name4, true);
    EXPECT_EQ(name3 != name4, false);
    EXPECT_EQ(name3 == name1, false);
    EXPECT_EQ(name3 != name1, true);

    // =
    name3 = name1;
    EXPECT_EQ(name3 == name1, true);
    EXPECT_EQ(name3 != name1, false);
    name4 = name2;
    EXPECT_EQ(name4 == name2, true);
    EXPECT_EQ(name4 != name2, false);
}

TEST(ktnName, others) {
    ktnName name;
    name.AppendSuffix();
    EXPECT_EQ(name == "unnamed_0000", true);
    name = "0000";
    name.AppendSuffix();
    EXPECT_EQ(name == "unnamed_0000", true);
    name = "_0000";
    name.AppendSuffix();
    EXPECT_EQ(name == "unnamed_0000", true);
    name = "a0000";
    name.AppendSuffix();
    EXPECT_EQ(name, "a0000_0000");
    name = "c__";
    name.AppendSuffix();
    EXPECT_EQ(name, "c_0000");
    name.BumpSuffix();
    EXPECT_EQ(name, "c_0001");

    name = "a0000"; // bump suffix without appending first
    name.BumpSuffix();
    EXPECT_EQ(name, "a0000_0001");

    name = "name_9999"; // test if BumpSuffix works with more than 4 digit at the end
    name.BumpSuffix();
    EXPECT_EQ(name, "name_10000");
    name.BumpSuffix();
    EXPECT_EQ(name, "name_10001");
    name = "name_19999";
    name.BumpSuffix();
    EXPECT_EQ(name, "name_20000");
}
