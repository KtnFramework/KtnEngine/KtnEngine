#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ktnEngine/Core/ktn3DElement.hpp>

using namespace glm;
using namespace ktn;
using namespace Core;

TEST(ktn3DElement, constructors) {
    const ktn3DElement element_default;
    EXPECT_EQ(element_default.Name.ToStdString(), "ktn3DElement");
    EXPECT_EQ(element_default.ModelMatrix(WorldSpace), IDENTITY_MATRIX_4);
    EXPECT_EQ(element_default.Position(WorldSpace), vec3(0, 0, 0));
    EXPECT_EQ(element_default.Rotation(WorldSpace), DefaultRotation);
    EXPECT_EQ(element_default.Scale(WorldSpace), vec3(1, 1, 1));

    ktn3DElement element_noDefault("non-generic name");
    EXPECT_EQ(element_noDefault.Name.ToStdString(), "nongenericname");

    // copy constructor
    ktn3DElement element_copyconstructed = element_noDefault;
    EXPECT_EQ(element_copyconstructed == element_noDefault, true);
    EXPECT_EQ(element_copyconstructed != element_noDefault, false);
}

TEST(ktn3DElement, destructor) {
    auto parent = new ktn3DElement;
    auto child = new ktn3DElement("child", parent);
    EXPECT_NE(std::find(parent->Children()->begin(), parent->Children()->end(), child), parent->Children()->end());
    delete child;
    EXPECT_EQ(std::find(parent->Children()->begin(), parent->Children()->end(), child), parent->Children()->end());
}

TEST(ktn3DElement, copy_assignment_operator) {
    const ktn3DElement element_default("non-generic-name");
    EXPECT_EQ(element_default.Name, "nongenericname");
    ktn3DElement element_copied;

    element_copied = element_default;
    EXPECT_EQ(element_copied == element_default, true);
    EXPECT_EQ(element_copied != element_default, false);
}

TEST(ktn3DElement, operators) {
    const ktn3DElement element_default("default");

    // different object
    ktn3DElement element2("name2");
    EXPECT_EQ(element_default == element2, false);
    EXPECT_EQ(element_default != element2, true);
    element2 = element_default;
    EXPECT_EQ(element_default == element2, true);
    EXPECT_EQ(element_default != element2, false);

    // different positions
    element2.SetPosition(vec3(0, 1, 2), WorldSpace);
    EXPECT_EQ(element_default == element2, false);
    EXPECT_EQ(element_default != element2, true);
    element2 = element_default;

    // different rotations
    quat rotation(angleAxis(3.F, normalize(vec3(4, 5, 6))));
    element2.SetRotation(rotation, WorldSpace);
    EXPECT_EQ(element_default == element2, false);
    EXPECT_EQ(element_default != element2, true);
    element2 = element_default;

    // different scales
    element2.SetScale({7, 8, 9}, WorldSpace);
    EXPECT_EQ(element_default == element2, false);
    EXPECT_EQ(element_default != element2, true);
}

TEST(ktn3DElement, SetPositionInLocalSpaceWithoutParent) {
    ktn3DElement element;
    const vec3 position(0, 1, 2);

    element.SetPosition(position, LocalSpace);
    EXPECT_EQ(element.Position(LocalSpace), position);
    EXPECT_EQ(element.Position(WorldSpace), position);
}

TEST(ktn3DElement, SetPositionInLocalSpaceWithParent) {
    ktn3DElement parent;
    ktn3DElement element("element", &parent);
    const vec3 position(0, 1, 2);
    parent.SetPosition(position, WorldSpace);

    element.SetPosition(position, LocalSpace);
    EXPECT_EQ(element.Position(LocalSpace), position);
    EXPECT_EQ(element.Position(WorldSpace), position + position);
}

TEST(ktn3DElement, SetPositionInWorldSpaceWithParent) {
    ktn3DElement parent;
    ktn3DElement element("element", &parent);
    const vec3 position(0, 1, 2);
    parent.SetPosition(position, WorldSpace);
    const mat4 modelmatrix(glm::translate(IDENTITY_MATRIX_4, position));

    element.SetPosition(vec3(0, 1, 2), WorldSpace);
    EXPECT_EQ(element.Position(WorldSpace), position);
    EXPECT_EQ(element.ModelMatrix(WorldSpace), modelmatrix);
    EXPECT_EQ(element.Position(LocalSpace), vec3());
    EXPECT_EQ(element.ModelMatrix(LocalSpace), IDENTITY_MATRIX_4);
}

TEST(ktn3DElement, SetRotationInLocalSpaceWithParent) {
    ktn3DElement parent;
    ktn3DElement element("element", &parent);

    const quat rotation1(angleAxis(90.0F, vec3(1.0, 0.0, 0.0))); // rotate 90 degrees around the positive x axis
    const quat rotation2(angleAxis(45.0F, vec3(1.0, 0.0, 0.0))); // rotate another 45 degrees around the same axis
    parent.SetRotation(rotation1, LocalSpace);
    const mat4 modelmatrix = toMat4(rotation2);

    element.SetRotation(rotation2, LocalSpace);
    EXPECT_EQ(element.Rotation(LocalSpace), rotation2);
    EXPECT_EQ(element.ModelMatrix(LocalSpace), modelmatrix);

    const quat rotationSum(angleAxis(135.0F, vec3(1.0, 0.0, 0.0)));
    EXPECT_FLOAT_EQ(element.Rotation(WorldSpace).x, rotationSum.x);
    EXPECT_FLOAT_EQ(element.Rotation(WorldSpace).y, rotationSum.y);
    EXPECT_FLOAT_EQ(element.Rotation(WorldSpace).z, rotationSum.z);
    EXPECT_FLOAT_EQ(element.Rotation(WorldSpace).w, rotationSum.w);
}

TEST(ktn3DElement, SetRotationInWorldSpaceWithParent) {
    ktn3DElement parent;
    const quat rotation_parent(angleAxis(135.0F, vec3(0, 1, 0))); // rotate 90 degrees around the positive x axis
    parent.SetRotation(rotation_parent, WorldSpace);

    ktn3DElement element("element", &parent);
    const quat rotation_child(angleAxis(45.0F, vec3(0, 1, 0))); // rotate 45 degrees around the same axis
    const mat4 modelmatrix = toMat4(rotation_child);

    element.SetRotation(rotation_child, WorldSpace);
    EXPECT_EQ(element.Rotation(WorldSpace), rotation_child);
    EXPECT_EQ(element.ModelMatrix(WorldSpace), modelmatrix);

    const quat rotation_difference(angleAxis(-90.0F, vec3(0, 1, 0)));
    EXPECT_FLOAT_EQ(element.Rotation(LocalSpace).x, rotation_difference.x);
    EXPECT_FLOAT_EQ(element.Rotation(LocalSpace).y, rotation_difference.y);
    EXPECT_FLOAT_EQ(element.Rotation(LocalSpace).z, rotation_difference.z);
    EXPECT_FLOAT_EQ(element.Rotation(LocalSpace).w, rotation_difference.w);
}

TEST(ktn3DElement, OffsetPosistionInWorldSpaceWithParent) {
    ktn3DElement parent;
    ktn3DElement elm("elm", &parent);
    parent.SetPosition(vec3(1, 2, 3), WorldSpace);
    elm.OffsetPosition(vec3(4, 5, 6), WorldSpace);
    EXPECT_EQ(elm.Position(WorldSpace), vec3(5, 7, 9));
}

TEST(ktn3DElement, OffsetPosistionInWorldSpaceWithChildren) {
    ktn3DElement elm;
    ktn3DElement child("child", &elm);
    child.SetPosition(vec3(1, 2, 3), WorldSpace);
    EXPECT_EQ(child.Position(WorldSpace), vec3(1, 2, 3));
    elm.OffsetPosition(vec3(4, 5, 6), WorldSpace);
    EXPECT_EQ(child.Position(WorldSpace), vec3(5, 7, 9));
}

TEST(ktn3DElement, OffsetPosistionInLocalSpaceWithParent) {
    ktn3DElement parent;
    ktn3DElement elm("elm", &parent);
    parent.SetPosition(vec3(1, 2, 3), WorldSpace);
    parent.SetRotation(angleAxis(radians(90.0F), vec3(0, 1, 0)), WorldSpace);

    // this works as if the vector is turned 90 degree along the y axis, then added to parent's position
    elm.OffsetPosition(vec3(4, 6, 8), LocalSpace);

    vec3 result( //
        elm.Position(LocalSpace).z + parent.Position(WorldSpace).x, // elm.z(local) becomes elm.x(world) after rotation
        elm.Position(LocalSpace).y + parent.Position(WorldSpace).y, // y unchanged
        -elm.Position(LocalSpace).x + parent.Position(WorldSpace).z // elm.x(local) becomes elm.-z(world)
    );
    EXPECT_NEAR(length(elm.Position(WorldSpace) - result), 0, 1e-6);
}

TEST(ktn3DElement, OffsetPosistionInLocalSpaceWithChildren) {
    ktn3DElement elm;
    ktn3DElement child("child", &elm);
    elm.SetRotation(angleAxis(radians(90.0F), vec3(0, 1, 0)), WorldSpace);
    elm.OffsetPosition(vec3(1, 2, 3), LocalSpace);

    // this works as if the vector is turned 90 degree along the y axis, then added to parent's position
    child.SetPosition(vec3(4, 6, 8), LocalSpace);
    vec3 result( //
        child.Position(LocalSpace).z + elm.Position(WorldSpace).x,
        child.Position(LocalSpace).y + elm.Position(WorldSpace).y,
        -child.Position(LocalSpace).x + elm.Position(WorldSpace).z);
    EXPECT_NEAR(length(child.Position(WorldSpace) - result), 0, 1e-6);
}

TEST(ktn3DElement, parent_relationship) {
    ktn3DElement e1("e1");
    EXPECT_EQ(e1.Parent(), nullptr);
    EXPECT_EQ(e1.Children()->size(), 0);

    ktn3DElement e2("e2", &e1);
    EXPECT_EQ(e2.Parent(), &e1);
    EXPECT_EQ(e1.Children()->size(), 1);
    EXPECT_NE(std::find(e1.Children()->begin(), e1.Children()->end(), &e2), e1.Children()->end());

    ktn3DElement e3("hmm");
    EXPECT_EQ(e3.Parent(), nullptr);
    EXPECT_EQ(std::find(e1.Children()->begin(), e1.Children()->end(), &e3), e1.Children()->end());

    e3.SetParent(&e1);
    EXPECT_EQ(e3.Parent(), &e1);
    EXPECT_NE(std::find(e1.Children()->begin(), e1.Children()->end(), &e3), e1.Children()->end());
    EXPECT_EQ(std::find(e2.Children()->begin(), e2.Children()->end(), &e3), e2.Children()->end());

    // assign new parent
    e3.SetParent(&e2);
    EXPECT_EQ(e3.Parent(), &e2);
    EXPECT_EQ(std::find(e1.Children()->begin(), e1.Children()->end(), &e3), e1.Children()->end());
    EXPECT_NE(std::find(e2.Children()->begin(), e2.Children()->end(), &e3), e2.Children()->end());
}
