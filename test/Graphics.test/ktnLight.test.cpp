#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ktnEngine/Graphics/ktnLight.hpp>

using namespace glm;
using namespace ktn::Graphics;

TEST(ktnLightHelpers, all) {
    const ktnLight::hCutoff cutoff_default;
    EXPECT_THAT(cutoff_default.InnerCos(), ::testing::FloatEq(cos(radians(12.5f))));
    EXPECT_THAT(cutoff_default.OuterCos(), ::testing::FloatEq(cos(radians(17.5f))));

    const ktnLight::hFalloff falloff_default;
    EXPECT_THAT(falloff_default.LinearTerm, ::testing::FloatEq(0.0f));
    EXPECT_THAT(falloff_default.QuadraticTerm, ::testing::FloatEq(1.0f));

    ktnLight::hCutoff cutoff;
    EXPECT_EQ(cutoff_default == cutoff, true);
    EXPECT_EQ(cutoff_default != cutoff, false);
    cutoff = ktnLight::hCutoff(10, 30);
    EXPECT_EQ(cutoff_default == cutoff, false);
    EXPECT_EQ(cutoff_default != cutoff, true);

    ktnLight::hFalloff falloff;
    EXPECT_EQ(falloff_default == falloff, true);
    EXPECT_EQ(falloff_default != falloff, false);
    falloff = ktnLight::hFalloff(2, 4);
    EXPECT_EQ(falloff_default == falloff, false);
    EXPECT_EQ(falloff_default != falloff, true);
}

TEST(ktnLight, constructors_destructor) {
    const ktnLight light_default;
    const ktnLight::hCutoff lightcutoff_default;
    const ktnLight::hFalloff lightfalloff_default;

    EXPECT_EQ(light_default.Cutoff == lightcutoff_default, true);
    EXPECT_EQ(light_default.Falloff == lightfalloff_default, true);
    EXPECT_EQ(light_default.Color(), vec3(1, 1, 1));

    EXPECT_EQ(light_default.Color(), vec3(1, 1, 1));
    EXPECT_EQ(light_default.Direction(), vec3(0, 0, -1));
    EXPECT_THAT(light_default.Intensity, ::testing::FloatEq(1.0));
    EXPECT_EQ(light_default.IsCastingShadows, false);
    EXPECT_EQ(light_default.Type, LightType::POINT);
}

TEST(ktnLight, operators) {
    const ktnLight light_default;
    ktnLight light;
    EXPECT_EQ(light == light_default, true);
    EXPECT_EQ(light != light_default, false);

    // different ktn3DElements
    light.Name = "non-generic name";
    EXPECT_EQ(light == light_default, false);
    EXPECT_EQ(light != light_default, true);
    light.Name = ktn::Core::ktnName("ktnLight");
    EXPECT_EQ(light == light_default, true);
    EXPECT_EQ(light != light_default, false);

    // reset
    light = light_default;
    EXPECT_EQ(light == light_default, true);
    EXPECT_EQ(light != light_default, false);
}

TEST(ktnLight, getters_setters) {
    ktnLight light;
    light.SetColor(vec3(0.4, 0.5, 0.6));
    EXPECT_EQ(light.Color(), vec3(0.4, 0.5, 0.6));
    light.SetColor(0.7f, 0.8f, 0.9f);
    EXPECT_EQ(light.Color(), vec3(0.7, 0.8, 0.9));
    light.SetDirection(vec3(1, 0, 0));
    EXPECT_EQ(light.Direction(), vec3(1, 0, 0));
}
