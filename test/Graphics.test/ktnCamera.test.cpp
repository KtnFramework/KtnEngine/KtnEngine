#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ktnEngine/Graphics/ktnCamera.hpp>

using namespace glm;
using namespace ktn;
using namespace Graphics;

TEST(ktnCamera, constructors_destructor) {
    const ktnCamera camera_default;
    EXPECT_EQ(camera_default.Position(WorldSpace), vec3(0, 0, 0));
    EXPECT_EQ(camera_default.Sensitivity(), 0.1f);
    EXPECT_EQ(camera_default.Top(), vec3(0.0f, 0.0f, 1.0f));
    EXPECT_EQ(camera_default.UniversalUp(), vec3(0.0f, 1.0f, 0.0f));
}

TEST(ktnCamera, operators) {
    const ktnCamera camera_default;
    ktnCamera camera2;
    camera2.Name = "non-generic camera name";
    EXPECT_EQ(camera_default == camera2, false);
    EXPECT_EQ(camera_default != camera2, true);
    camera2 = camera_default;
    EXPECT_EQ(camera_default == camera2, true);
    EXPECT_EQ(camera_default != camera2, false);
    WIP;
}

TEST(ktnCamera, getters_setters) {
    ktnCamera camera;
    camera.SetDirection(vec3(0, 1, 2));
    EXPECT_EQ(camera.Direction(), vec3(0, 1, 2));
    camera.SetDirection(1, 1, 1);
    EXPECT_EQ(camera.Direction(), vec3(1, 1, 1));
    camera.SetMode(CameraMode::TRACKING);
    EXPECT_EQ(camera.Mode(), CameraMode::TRACKING);
    camera.SetType(CameraType::SUPERVISION);
    EXPECT_EQ(camera.Type(), CameraType::SUPERVISION);
}
