#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ktnEngine/Graphics/ktnAction.hpp>
#include <ktnEngine/Graphics/ktnGraphicsUtils.hpp>

#include <thread>

using namespace glm;
using namespace ktn::Graphics;
using namespace std;

TEST(ktnAction, Duration) {
    ktnAction action;

    auto samplerOfUnknownType = make_shared<ktnActionSampler>();
    action.Samplers.insert({0, samplerOfUnknownType});
    EXPECT_THROW(action.CalculateDuration(), runtime_error);
    action.Samplers.clear();

    auto translationSamplePack = make_shared<ktnActionSampler::SamplePack<vec3>>();
    translationSamplePack->Current.push_back({0.0F, vec3(0, 0, 0)});
    translationSamplePack->Current.push_back({0.1F, vec3(1, 0, 0)});
    auto translationSampler = make_shared<ktnActionSampler>();
    translationSampler->Type = ktnActionSampler::SamplerType::Translation;
    translationSampler->TranslationSamples = translationSamplePack;
    translationSampler->SampleTimes.push_back(0.0F);
    translationSampler->SampleTimes.push_back(0.1F);

    action.Samplers.insert({0, translationSampler});
    action.CalculateDuration();
    EXPECT_FLOAT_EQ(action.Duration(), 0.1);
    translationSampler->Verify();

    auto scaleSamplePack = make_shared<ktnActionSampler::SamplePack<vec3>>();
    scaleSamplePack->Current.push_back({0.2F, vec3(0, 0, 0)});
    scaleSamplePack->Current.push_back({0.4F, vec3(1, 0, 0)});
    auto scaleSampler = make_shared<ktnActionSampler>();
    scaleSampler->Type = ktnActionSampler::SamplerType::Scale;
    scaleSampler->ScaleSamples = scaleSamplePack;
    scaleSampler->SampleTimes.push_back(0.2F);
    scaleSampler->SampleTimes.push_back(0.4F);
    scaleSampler->Verify();

    action.Samplers.insert({1, scaleSampler});
    action.CalculateDuration();
    EXPECT_FLOAT_EQ(action.Duration(), 0.4);

    auto rotationSamplePack = make_shared<ktnActionSampler::SamplePack<quat>>();
    rotationSamplePack->Current.push_back({0.2F, quat(0, 0, 0, 1)});
    rotationSamplePack->Current.push_back({0.3F, angleAxis(90.0F, vec3(0, 1, 0))});
    auto rotationSampler = make_shared<ktnActionSampler>();
    rotationSampler->Type = ktnActionSampler::SamplerType::Rotation;
    rotationSampler->RotationSamples = rotationSamplePack;
    rotationSampler->SampleTimes.push_back(0.2F);
    rotationSampler->SampleTimes.push_back(0.3F);
    rotationSampler->Verify();

    action.Samplers.insert({2, rotationSampler});
    action.CalculateDuration();
    EXPECT_FLOAT_EQ(action.Duration(), 0.4);
}

TEST(ktnAction, IsPlaying) {
    auto duration = 0.1F;
    auto translationSamplerPack = make_shared<ktnActionSampler::SamplePack<vec3>>();
    translationSamplerPack->Current.push_back({.0F, vec3(0, 0, 0)});
    translationSamplerPack->Current.push_back({duration, vec3(1, 0, 0)});

    auto sampler = make_shared<ktnActionSampler>();
    sampler->Type = ktnActionSampler::SamplerType::Translation;
    sampler->TranslationSamples = translationSamplerPack;
    sampler->SampleTimes.push_back(.0F);
    sampler->SampleTimes.push_back(duration);

    ktnAction action;
    action.Samplers[0] = sampler;

    action.CalculateDuration();
    Utils::SetFrameTime(0);
    action.Start();
    EXPECT_EQ(action.IsPlaying(), true);

    Utils::SetFrameTime((duration * 1E6 * 3.0 / 2.0));
    action.Interpolate();
    EXPECT_EQ(action.IsPlaying(), false);

    action.Loop = true;
    Utils::SetFrameTime(0);
    action.Start();
    Utils::SetFrameTime((duration * 1E6 * 3.0 / 2.0));
    action.Interpolate();
    EXPECT_EQ(action.IsPlaying(), true);
}
