#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ktnEngine/Graphics/ktnModel.hpp>

using namespace glm;
using namespace ktn::Graphics;

TEST(ktnModel, constructor) {
    EXPECT_THROW(ktnModel model(""), std::invalid_argument);
}
