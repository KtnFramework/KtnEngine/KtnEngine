#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ktnEngine/Graphics/ktnAction.hpp>
#include <ktnEngine/Graphics/ktnGraphicsUtils.hpp>
#include <ktnEngine/Graphics/ktnJoint.hpp>

using namespace ktn::Graphics;
using namespace std;

TEST(Combined, JointTransformationInterpolationViaAction_RotationIsCorrect) {
    RecordProperty(
        "Description",
        "This test shows that rotation interpolation via action is correct."
        "An animation with duration of 1s, when interpolated at 0.5s, will have rotated halfway through.");

    auto rotationSamplePack = make_shared<ktnActionSampler::SamplePack<glm::quat>>();
    rotationSamplePack->Current.push_back({0.F, glm::angleAxis(0.F, glm::vec3(0, 1, 0))});
    rotationSamplePack->Current.push_back({1.F, glm::angleAxis(90.F, glm::vec3(0, 1, 0))});

    auto sampler = make_shared<ktnActionSampler>();
    sampler->Type = ktnActionSampler::SamplerType::Rotation;
    sampler->RotationSamples = rotationSamplePack;
    sampler->SampleTimes.push_back(0.F);
    sampler->SampleTimes.push_back(1.F);

    auto channel = ktnAction::Channel();
    channel.Path = ktnAction::Channel::ChannelPath::Rotation;
    channel.Sampler = sampler;

    ktnJoint joint;
    joint.OffsetPosition(glm::vec3(0.F, 10.F, 0.F), ktn::ReferenceSpace::World);

    channel.Target = &joint;
    ktnAction action;
    action.Name = "rotate";
    action.Channels.push_back(channel);
    action.Samplers[0] = sampler;
    action.CalculateDuration();
    joint.AnimatedInfluences.insert({action.Name, ktn::Core::ktnPosRotScaleInfluence()});

    Utils::SetFrameTime(0);
    action.Start();
    Utils::SetFrameTime(500000);
    Utils::SetFrameDuration(500000);
    action.Interpolate();
    EXPECT_NEAR(joint.AnimatedInfluences[action.Name].Influence, 1.F, 1e-6);

    auto referenceMat = glm::mat4{ktn::IDENTITY_MATRIX_4};
    auto transformed = glm::translate(glm::mat4{ktn::IDENTITY_MATRIX_4}, {0.F, 10.F, 0.F});
    referenceMat = glm::rotate(transformed, 45.F, {0, 1, 0});
    auto inverseBind = glm::inverse(transformed);
    joint.SetInverseBindMatrix(inverseBind);
    joint.UpdateAnimatedLocalSpaceModelMatrix();
    joint.UpdateAnimatedWorldSpaceModelMatrix();

    // The world space model matrix for a joint is already multiplied with the inverse bind matrix,
    // so the reference matrix has to multiply with it as well to compare.
    ASSERT_EQ(joint.AnimatedModelMatrix(ktn::ReferenceSpace::World), referenceMat * inverseBind);
}
