#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <gleb/buffer.hpp>

#include <ktnEngine/Graphics/ktnActionSampler.hpp>

using namespace ktn;
using namespace Graphics;

using namespace glm;
using namespace std;

TEST(ktnActionSampler_SamplePack, Verify) {
    // TODO specialize exception types
    ktnActionSampler::SamplePack<vec3> pack;
    EXPECT_ANY_THROW(pack.Verify()); // no sample in current
    pack.Current.emplace_back(0.1, vec3(0, 0, 0));
    EXPECT_NO_THROW(pack.Verify());

    pack.PreviousTangent.emplace();
    EXPECT_ANY_THROW(pack.Verify()); // only previous tangent sample list exists

    pack.NextTangent.emplace();
    EXPECT_ANY_THROW(pack.Verify()); // both tangent sample list exist but not same size as current

    pack.PreviousTangent->emplace_back(0.0, vec3(0, -1, 0));
    pack.NextTangent->emplace_back(0.0, vec3(0, 1, 0));
    EXPECT_NO_THROW(pack.Verify());

    pack.PreviousTangent.reset();
    EXPECT_ANY_THROW(pack.Verify()); // only next tangent sample list exists
}

TEST(ktnActionSampler, Verify) {
    // TODO specialize exception types
    ktnActionSampler sampler;
    EXPECT_ANY_THROW(sampler.Verify()); // no sample times

    sampler.SampleTimes.emplace_back(0.5);
    EXPECT_ANY_THROW(sampler.Verify()); // no sampler type

    sampler.Type = ktnActionSampler::SamplerType::Rotation;
    EXPECT_ANY_THROW(sampler.Verify()); // rotation samples not created

    sampler.RotationSamples = make_shared<ktnActionSampler::SamplePack<quat>>();
    EXPECT_ANY_THROW(sampler.Verify()); // rotation samples vector has 0 element

    sampler.RotationSamples->Current.emplace_back(0.5, angleAxis(radians(90.0F), vec3(1, 0, 0)));
    EXPECT_NO_THROW(sampler.Verify());

    sampler.Type = ktnActionSampler::SamplerType::Scale;
    sampler.ScaleSamples = make_shared<ktnActionSampler::SamplePack<vec3>>();
    sampler.ScaleSamples->Current.emplace_back(0.5, vec3(3, 6, 9));
    EXPECT_ANY_THROW(sampler.Verify()); // rotation samples not deleted

    sampler.RotationSamples.reset();
    EXPECT_NO_THROW(sampler.Verify());

    sampler.Type = ktnActionSampler::SamplerType::Translation;
    sampler.TranslationSamples = make_shared<ktnActionSampler::SamplePack<vec3>>();
    sampler.TranslationSamples->Current.emplace_back(0.5, vec3(3, 6, 9));
    EXPECT_ANY_THROW(sampler.Verify()); // scale samples not deleted

    sampler.ScaleSamples.reset();
    EXPECT_NO_THROW(sampler.Verify());
}
