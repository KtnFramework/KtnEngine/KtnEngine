#!/bin/bash

THIS_SCRIPT_NAME=$(basename -- "$0")

declare -a CATEGORIES # a list of categories to check with clang-tidy

for i in "$@"; do
    case $i in
    --check-all)
        CATEGORIES+=(bugprone)
        CATEGORIES+=(cert)
        CATEGORIES+=(clang-analyzer)
        CATEGORIES+=(cppcoreguidelines)
        CATEGORIES+=(hicpp)
        CATEGORIES+=(llvm)
        CATEGORIES+=(misc)
        CATEGORIES+=(modernize)
        CATEGORIES+=(performance)
        CATEGORIES+=(portability)
        CATEGORIES+=(readability)
        ;;
    --check-bugprone)
        CATEGORIES+=(bugprone)
        ;;
    --check-cert)
        CATEGORIES+=(cert)
        ;;
    --check-clang-analyzer)
        CATEGORIES+=(clang-analyzer)
        ;;
    --check-cppcoreguidelines)
        CATEGORIES+=(cppcoreguidelines)
        ;;
    --check-hicpp)
        CATEGORIES+=(hicpp)
        ;;
    --check-llvm)
        CATEGORIES+=(llvm)
        ;;
    --check-misc)
        CATEGORIES+=(misc)
        ;;
    --check-modernize)
        CATEGORIES+=(modernize)
        ;;
    --check-performance)
        CATEGORIES+=(performance)
        ;;
    --check-portability)
        CATEGORIES+=(portability)
        ;;
    --check-readability)
        CATEGORIES+=(readability)
        ;;
    -h|--help)
        echo "USAGE: ./ci-scripts/$THIS_SCRIPT_NAME [options]"
        echo
        echo "OPTIONS:"
        echo "  -h or --help                 - Print this message"
        echo "  --check-all                  - Enable checking all relevant categories"
        echo "  --check-bugprone             - Enable checking the bugprone category"
        echo "  --check-cert                 - Enable checking the cert category"
        echo "  --check-clang-analyzer       - Enable checking the clang-analyzer category"
        echo "  --check-cppcoreguidelines    - Enable checking the cppcoreguidelines category"
        echo "  --check-hicpp                - Enable checking the hicpp category"
        echo "  --check-llvm                 - Enable checking the llvm category"
        echo "  --check-misc                 - Enable checking the misc category"
        echo "  --check-modernize            - Enable checking the modernize category"
        echo "  --check-performance          - Enable checking the performance category"
        echo "  --check-portability          - Enable checking the portability category"
        echo "  --check-readability          - Enable checking the readability category"
        exit
        ;;
    esac
done



if [ ${#CATEGORIES[@]} -eq 0 ]; then
    echo "Please specify which categories to analyze. Check ./ci-scripts/$THIS_SCRIPT_NAME --help for details."
    exit 1
else
    echo "Analyzing based on these categories: ${CATEGORIES[@]}"
fi



echo "Performing static analysis using clang-tidy..."
clang-tidy --version || { exit 1; }
echo

STATIC_ANALYSIS_OUTPUT_FILE=static-analysis.txt
if [ -f $STATIC_ANALYSIS_OUTPUT_FILE ]; then
    rm $STATIC_ANALYSIS_OUTPUT_FILE
fi
CATEGORIES_STRING="-*"
for category in "${CATEGORIES[@]}"; do
    CATEGORIES_STRING=$OPTION_CATEGORIES,$category-*
done

# TODO: this will become unmanageable as time goes on.
# Ideally one should not have to pass INCLUDE_DIRS and DEFINES, 
# if CMake is configured to generate compilation database.
# Explore this possibility.

INCLUDE_DIRS="
-Iexternal
-Iexternal/imgui
-Igleb/include
-Iinclude
-IKtnSignalSlot/include
"

DEFINES="
-DIMGUI_IMPL_OPENGL_LOADER_CUSTOM=<epoxy/gl.h>
-DKTNSIGNALSLOT_SIMPLE_TYPEDEFS
"

for file_name in $(find -name '*.[ch]pp'); do
    case $file_name in
    ./build*|./external*|./gleb*|./KtnSignalSlot*|./test*)
        continue
        ;;
    *)
        clang-tidy --checks=$CATEGORIES_STRING --extra-arg=-std=c++17 --fix --quiet \
        $file_name -- $INCLUDE_DIRS $DEFINES | tee -a $STATIC_ANALYSIS_OUTPUT_FILE
        ;;
    esac
done

echo "--------------------------------------------------------------------------------"
echo "Number of errors and warnings by category:"
for category in "${CATEGORIES[@]}"; do
    count=$(cat $STATIC_ANALYSIS_OUTPUT_FILE | grep -Poc "\[$category-")
    echo "  $category: $count"
done

echo "--------------------------------------------------------------------------------"
FIXCOUNT=$(cat $STATIC_ANALYSIS_OUTPUT_FILE | grep -Poc "note: FIX-IT applied")
echo "Number of possible automatic fixes: $FIXCOUNT"

echo "--------------------------------------------------------------------------------"
echo "Checking if formatting is needed..."
./ci-scripts/check-formatting.sh || {
    exit 1
}
