#!/bin/bash

for i in "$@"; do
    case $i in
    --trigger-build)
    declare TRIGGER_BUILD=TRUE
    esac
done

if [ ! -z "$TRIGGER_BUILD" ]; then
    ./ci-scripts/build.sh --build-unit-tests || {
        exit 1
    }
elif [ ! -f build/test/KtnEngine-unit-test ]; then
    ./ci-scripts/build.sh --build-unit-tests || {
        exit 1
    }
fi

./build/test/KtnEngine-unit-test || {
    exit 1
}
