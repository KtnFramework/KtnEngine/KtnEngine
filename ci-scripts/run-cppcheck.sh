INCLUDE_DIRS="
-I external
-I gleb/include
-I include
-I KtnSignalSlot/include
"

for file_name in $(find -name '*.[ch]pp'); do
    case $file_name in
    ./build*|./external*|./gleb*|./KtnSignalSlot*|./test*)
        continue
        ;;
    *)
        cppcheck --enable=all $file_name $INCLUDE_DIRS --suppress=*:external/* --suppress=missingIncludeSystem --suppress=unmatchedSuppression:* --suppress=unusedFunction:*
        echo
        ;;
    esac
done
