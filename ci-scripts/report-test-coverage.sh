#!/bin/bash

for i in "$@"; do
    case $i in
    --trigger-build)
        declare TRIGGER_BUILD=TRUE
    esac
done

for file_name in $(find -name '*gcda'); do
    rm -f $file_name
done

if [ ! -z "$TRIGGER_BUILD" ]; then
    ./ci-scripts/run-test.sh --trigger-build || {
        exit 1
    }
else
    ./ci-scripts/run-test.sh || {
        exit 1
    }
fi

cd build/test
python3 -m gcovr -r ../.. \
--exclude ../../external/ \
--exclude ../../gleb/ \
--exclude ../../KtnSignalSlot/ \
--exclude ../../test/ --html-details -o coverage.html || {
    exit 1
}

python3 -m gcovr -r ../.. \
--exclude ../../external/ \
--exclude ../../gleb/ \
--exclude ../../KtnSignalSlot/ \
--exclude ../../test/ || {
    exit 1
}
cd ..
