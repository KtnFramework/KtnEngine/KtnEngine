# parse arguments
for i in "$@"; do
    case $i in
    --build-unit-tests)
        KTN_UNIT_TEST_TOGGLE=ON
        ;;
    esac
done

if [ -z "$KTN_UNIT_TEST_TOGGLE" ]; then
    KTN_UNIT_TEST_TOGGLE=OFF
fi

if [ ! -d build ]; then
    mkdir build
fi

cd build

KTN_CMAKE_OPTIONS="
-DKTNENGINE_BUILD_UNIT_TESTS=$KTN_UNIT_TEST_TOGGLE \
-DKTNENGINE_USE_OPENGL=ON \
-DKTNENGINE_USE_VULKAN=ON \
"

cmake $KTN_CMAKE_OPTIONS .. || {
    exit 1
}

make -j$(nproc) || {
    exit 1
}
