param(
    [switch]$force = $false
)

if ($force -and (Test-Path build)) {
    Write-Output "Removing old build directory..."
    Remove-Item build -Recurse -Force
}

if (!(Test-Path build)) {
    mkdir build
}

Set-Location build
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_RC_COMPILER=llvm-rc -GNinja ..
ninja
Set-Location ..