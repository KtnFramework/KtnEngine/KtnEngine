# check the executables
uname -a || { exit 1; }
cmake --version || { exit 1; }
g++ --version || { exit 1; }



function download_or_update {
    if [ -d $1 ]; then
        echo "Updating $1..."
        cd $1
        git pull --ff-only
        if [ $? -ne 0 ]; then
            echo "Error updating $1! Downloading instead..."
            cd ..
            rm -rf $1
            download_or_update $1 $2
        else
            cd ..
        fi
    else
        git clone $2 || {
            echo "Error downloading $1!"
            exit 1
        }
        echo "Dependency \"$1\" from $2 downloaded."
    fi
}



git submodule init
git submodule update --remote
git submodule foreach 'git checkout master && git pull --ff-only'

# download external libraries
if [ -d "external" ]; then
    echo "Updating external libraries..."
else
    echo "Downloading external libraries..."
    mkdir external
fi
cd external



# googletest
download_or_update googletest https://github.com/google/googletest.git



# imgui
download_or_update imgui https://github.com/ocornut/imgui.git



# stb
download_or_update stb https://github.com/nothings/stb.git



curl -o chrono_7.0.0_1640532762.tar.gz https://gitlab.com/KtnFramework/CI/engine-dependencies-linux/-/raw/tronk/chrono_7.0.0_1640532762.tar.gz?inline=false
tar -xzvf chrono_7.0.0_1640532762.tar.gz



cd ..
echo "All libraries downloaded."
