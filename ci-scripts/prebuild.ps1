param(
    [switch]$force = $false,

    # submodules
    [bool]$gleb = $true,
    [bool]$ktnSignalSlot = $true,

    # header-only libraries
    [bool]$glm = $true,
    [bool]$stb = $true,

    # dynamically linked libraries
    [bool]$glfw = $true,
    [bool]$libepoxy = $true,
    [bool]$vulkan = $true,

    # statically linked libraries
    [bool]$gtest = $true,
    [bool]$imgui = $true,

    # prebuilt dependencies
    [bool]$prebuilt_deps = $true
)

if ($force) {
    Write-Output "Removing existing external libraries folder..."
    Remove-Item external -Recurse -Force
    Remove-Item gleb -Recurse -Force
    Remove-Item ktnSignalSlot -Recurse -Force

    $gleb = $true
    $ktnSignalSlot = $true

    $glm = $true
    $stb = $true

    $glfw = $true
    $libepoxy = $true
    $vulkan = $true
    
    $gtest = $true
    $imgui = $true

    $prebuilt_deps = $true
}

if ($gleb -or $ktnSignalSlot) {
    $submodule_libs = $true
}

if ($glm -or $stb) {
    $header_only_libs = $true
}

if ($glfw -or $libepoxy -or $vulkan) {
    $dynamic_libs = $true
}

if ($gtest -or $imgui) {
    $static_libs = $true
}

$env:GIT_REDIRECT_STDERR = '2>&1'

function download_or_update {
    param(
        [parameter(Mandatory = $true)]    
        [String] $destination,
        [parameter(Mandatory = $true)]
        [String] $source,
        [parameter(Mandatory = $false)]
        [String] $tag
    )
    if (Test-Path $destination) {
        Write-Output "Updating $destination..."
        Set-Location $destination
        if (!$PSBoundParameters.ContainsKey('tag')) {
            git pull --ff-only
            if (!$?) {
                Write-Output "Error updating $destination! Downloading instead..."
                Set-Location ..
                Remove-Item -Recurse -Force $destination
                download_or_update -destination $destination -source $source -tag $tag
            }
            else {
                Set-Location ..
            }
        }
        else {
            git checkout $tag
            Set-Location ..
        }
    }
    else {
        Write-Output "Downloading $destination..."
        git clone $source $destination
        if (!$?) {
            Write-Output "Error downloading $destination!"
            exit 1
        }
        if ($PSBoundParameters.ContainsKey('tag')) {
            Set-Location $destination
            git checkout $tag
            if (!$?) {
                Write-Output "Error checking out tag $tag for $destination!"
                Set-Location ..
                exit 1
            }
            Set-Location ..
        }

        Write-Output "Dependency `"$destination`" from $source downloaded."
    }
}

$nproc = Get-WmiObject -Class Win32_Processor | Select-Object -Expand NumberOfLogicalProcessors | Out-String
$nproc = $nproc -as [int]

# check build tools
Write-Output --------------------------------------------------------------------------------
Write-Output "Checking MSBuild version..."
MsBuild -version; if (!$?) {
    Write-Output "The directory for MSBuild.exe from the Visual Studio Build Tools is not in PATH."; exit 1
}
Write-Output `nDone.

Write-Output --------------------------------------------------------------------------------
Write-Output "Checking 7zip..."
7z *>$null; if (!$?) {
    Write-Output "The directory for 7z.exe is not in PATH."; exit 1
}
Write-Output Done.

if ($submodule_libs) {
    Write-Output --------------------------------------------------------------------------------
    Write-Output "Setting up submodules..."
    git submodule init
    git submodule update
    git submodule foreach 'git checkout master && git pull --ff-only'
    if ($gleb) {
        Set-Location gleb
        ./ci-scripts/setup.ps1
        Set-Location ..
    }
    Write-Output Done.
}

Write-Output --------------------------------------------------------------------------------
Write-Output "Setting up dependencies..."

# create external directory if it's not already there
if ([IO.Directory]::Exists((Join-Path(Get-Location) 'external'))) {
    Write-Output "Working with existing external libraries folder. To force setup with new folder, use the -force switch.";
}
else {
    mkdir external
}

Set-Location external



if ($header_only_libs) {
    Write-Output --------------------------------------------------------------------------------
    Write-Output "Setting up header-only libraries..."
    if ($glm) { download_or_update -destination glm -source https://github.com/g-truc/glm.git }
    if ($stb) { download_or_update -destination stb -source https://github.com/nothings/stb.git }
}



if ($dynamic_libs) {
    Write-Output --------------------------------------------------------------------------------
    Write-Output "Setting up dynamic libraries..."
    if ($glfw) {
        $glfw_version = "3.3.4"
        $glfw_extracted_name = "glfw-$glfw_version.bin.WIN64"
        $glfw_archive = "$glfw_extracted_name.zip"
        $glfw_download_target = "$PWD/$glfw_archive"
        $glfw_url = "https://github.com/glfw/glfw/releases/download/$glfw_version/$glfw_archive"
        if (!([IO.Directory]::Exists((Join-Path(Get-Location) 'glfw')))) {
            if (Test-Path $glfw_extracted_name) {
                Remove-Item $glfw_extracted_name -Recurse -Force
            }
            if (Test-Path $glfw_archive) {
                Remove-Item $glfw_archive
            }
            Write-Output "Downloading glfw..."
            (New-Object System.Net.WebClient).DownloadFile($glfw_url, $glfw_download_target); if (!$?) {
                Write-Output "Could not download glfw."; exit 1
            }
            7z x $glfw_archive; if (!$?) {
                Write-Output "Could not extract $glfw_archive."; exit 1
            }
            Remove-Item $glfw_archive
            Rename-Item $glfw_extracted_name glfw
        }
    }
    if ($libepoxy) {
        $libepoxy_version = "1.5.3"
        $libepoxy_extracted_name = "libepoxy-shared-x64"
        $libepoxy_archive = "$libepoxy_extracted_name.zip"
        $libepoxy_download_target = "$PWD/$libepoxy_archive"
        $libepoxy_url = "https://github.com/anholt/libepoxy/releases/download/$libepoxy_version/$libepoxy_archive"
        if (!([IO.Directory]::Exists((Join-Path(Get-Location) 'libepoxy')))) {
            if (Test-Path $libepoxy_extracted_name) {
                Remove-Item $libepoxy_extracted_name -Recurse -Force
            }
            if (Test-Path $libepoxy_archive) {
                Remove-Item $libepoxy_archive
            }
            Write-Output "Downloading libepoxy..."
            (New-Object System.Net.WebClient).DownloadFile($libepoxy_url, $libepoxy_download_target); if (!$?) {
                Write-Output "Could not download libepoxy."; exit 1
            }
            7z x $libepoxy_archive; if (!$?) {
                Write-Output "Could not extract $libepoxy_archive."; exit 1
            }
            Remove-Item $libepoxy_archive
            Rename-Item $libepoxy_extracted_name libepoxy
        }
    }
    if ($vulkan) {
        $vulkan_version = "1.2.176.1"
        Write-Output "Setting up Vulkan SDK..."
        $vulkan_sdk_exec = "VulkanSDK-$vulkan_version-Installer.exe"
        $vulkan_sdk_url = "https://sdk.lunarg.com/sdk/download/$vulkan_version/windows/$vulkan_sdk_exec"
        $vulkan_sdk_download_target = "$PWD/$vulkan_sdk_exec"
        if (!(Test-Path $vulkan_sdk_exec)) {
            Write-Output "Downloading Vulkan SDK..."
            [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12;
            (New-Object System.Net.WebClient).DownloadFile($vulkan_sdk_url, $vulkan_sdk_download_target); if (!$?) {
                Write-Output "Could not download Vulkan SDK."; exit 1
            }
        }
        else {
            Write-Output "Using predownloaded Vulkan SDK installer."
        }
        Set-Alias vulkan_sdk_install $vulkan_sdk_download_target
        vulkan_sdk_install /S
        Write-Output "Vulkan SDK installed."
        Write-Output "Setting up Vulkan runtime..."
        $vulkan_runtime_exec = "VulkanRT-$vulkan_version-Installer.exe"
        $vulkan_runtime_url = "https://sdk.lunarg.com/sdk/download/$vulkan_version/windows/$vulkan_runtime_exec"
        $vulkan_runtime_download_target = "$PWD/$vulkan_runtime_exec"
        if (!(Test-Path $vulkan_runtime_exec)) {
            Write-Output "Downloading Vulkan runtime..."
            [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12;
            (New-Object System.Net.WebClient).DownloadFile($vulkan_runtime_url, $vulkan_runtime_download_target); if (!$?) {
                Write-Output "Could not download Vulkan runtime."; exit 1
            }
        }
        else {
            Write-Output "Using predownloaded Vulkan runtime installer."
        }
        Set-Alias vulkan_rt_install $vulkan_runtime_download_target
        vulkan_rt_install /S
        Write-Output "Vulkan runtime installed."
    }
}



if ($static_libs) {
    Write-Output --------------------------------------------------------------------------------
    Write-Output "Setting up static libraries..."
    if ($gtest) {
        $googletest_archive = "googletest.zip"
        $googletest_download_target = "$PWD/$googletest_archive"
        $googletest_url = "https://github.com/google/googletest/archive/master.zip"
        if (!([IO.Directory]::Exists((Join-Path(Get-Location) 'googletest')))) {
            if (Test-Path $googletest_archive) {
                Remove-Item $googletest_archive
            }
            Write-Output "Downloading googletest..."
            (New-Object System.Net.WebClient).DownloadFile($googletest_url, $googletest_download_target); if (!$?) {
                Write-Output "Could not download googletest."; exit 1
            }
            7z x $googletest_download_target; if (!$?) {
                Write-Output "Could not extract $googletest_archive."; exit 1
            }
            Remove-Item $googletest_archive
            Rename-Item googletest-master googletest
        }
    }
    if ($imgui) {
        download_or_update -destination imgui -source https://github.com/ocornut/imgui.git
    }
}



if ($prebuilt_deps) {
    $prebuilt_deps_name = "engine-dependencies-windows"
    $prebuilt_deps_extracted_name = "$prebuilt_deps_name-tronk"
    $prebuilt_deps_archive = "$prebuilt_deps_extracted_name.zip"
    $prebuilt_deps_download_target = "$PWD/$prebuilt_deps_archive"
    $prebuilt_deps_url = "https://gitlab.com/KtnFramework/CI/$prebuilt_deps_name/-/archive/tronk/$prebuilt_deps_archive"

    if (!(Test-Path $prebuilt_deps_extracted_name)) {
        Write-Output "Downloading prebuilt_deps..."
        (New-Object System.Net.WebClient).DownloadFile($prebuilt_deps_url, $prebuilt_deps_download_target); if (!$?) {
            Write-Output "Could not download prebuilt_deps."; exit 1
        }
        7z x $prebuilt_deps_archive; if (!$?) {
            Write-Output "Could not extract $prebuilt_deps_archive."; exit 1
        }
        Remove-Item $prebuilt_deps_archive
    }
    if (Test-Path -Path oggvorbis) {
        Remove-Item -Force -Recurse oggvorbis
    }
    if (Test-Path -Path portaudio) {
        Remove-Item -Force -Recurse portaudio
    }
    if (Test-Path -Path shaderc) {
        Remove-Item -Force -Recurse shaderc
    }

    7z x "$prebuilt_deps_extracted_name/ogg-1.3.5-vorbis-1.3.7-2021_06_06.7z" -ooggvorbis
    7z x "$prebuilt_deps_extracted_name/portaudio-v190600_20161030-2021_06_06.7z" -oportaudio
    7z x "$prebuilt_deps_extracted_name/shaderc-2021.0-2021_06_06.7z" -oshaderc
}



Write-Output Done.
Set-Location ..
