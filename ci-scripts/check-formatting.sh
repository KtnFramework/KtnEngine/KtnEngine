#!/bin/bash

for i in "$@"; do
    case $i in
    --fix)
        declare AUTO_FIX=1
        ;;
    esac
done

declare -a UNFORMATTED_FILES
for file_name in $(find -name '*.[ch]pp'); do
    case $file_name in
    ./build*|./external*|./gleb*|./KtnSignalSlot*|./test*)
        continue
        ;;
    *)
        # compare the original file and the formatted file, then check the output to see if there are changes
        LINES_CHANGED=$(diff -u <(cat $file_name) <(clang-format $file_name) | grep -c ^)
        # add name of files that need formatting to the UNFORMATTED_FILES array
        if [ $LINES_CHANGED -ne 0 ]; then
            UNFORMATTED_FILES+=("$file_name")
        fi
        ;;
    esac
done

if [ ${#UNFORMATTED_FILES[@]} -eq 0 ]; then
    echo "No formatting needed."
else
    echo "Files that need formatting: "
    for filename in ${UNFORMATTED_FILES[@]}; do
        echo "$filename"
    done
    if [ -z ${AUTO_FIX+x} ]; then
        exit 1
    else
        echo
        echo "Formatting files:"
        for filename in ${UNFORMATTED_FILES[@]}; do
            clang-format -i $filename && echo "[   OK   ] $filename" || {
                echo "[ FAILED ] $filename"
                declare AUTOFORMATTING_FAILED=1
            }
        done
    fi
fi

if [ ! -z ${AUTOFORMATTING_FAILED+x} ]; then
    exit 1
fi
