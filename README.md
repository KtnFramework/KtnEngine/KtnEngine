# KtnEngine
This repository contains the source code of KtnEngine,
a lightweight game engine written in C++17 with focus on ease of use and cross-platform availability.
The engine is currently available for Windows and Linux.

# Supported Operating systems
## Linux
The scripts have been tested on the following distributions:
- OpenSUSE Leap/Tumbleweed
- Ubuntu 19.10
  - Any older version will not work due to incompatible C++ libraries,
  especially std::filesystem

Pull requests for other distributions are welcome.
## Windows
The setup script is written in Powershell, so the prefered version of Windows would be Windows 10.

# Dependencies
These dependencies will be installed using the setup scripts
- Project Chrono (BSD-3-Clause).
- dear imgui for GUI overlay (MIT).
- gleb for importing glfw assets (Apache License, Version 2.0).
- GLFW for window creation and handling keyboard/mouse input (Zlib).
- glm for GL math (MIT).
- libepoxy for loading OpenGL functions (MIT).
- libogg and libvorbis for loading ogg audio files (BSD-3-Clause).
- PortAudio for audio I/O operations (MIT).
- stb for loading images (Public Domain/MIT).
- shaderc for compiling SPIR-V shaders online (BSD-3-Clause).
- Vulkan SDK (Apache License, Version 2.0).

# License
```
   Copyright 2018-2021 Tien Dat Nguyen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```

# Development
## Development environment setup
### Linux
```
./ci-scripts/prebuild.sh
```
### Windows 10
Before running the script, you need to install these tools
and add the appropriate paths in your PATH environment variable:
- [7-Zip](https://www.7-zip.org/download.html)
- [Build Tools for Visual Studio 2019](https://visualstudio.microsoft.com/downloads/)
- [CMake](https://cmake.org/download/)
- [git](https://git-scm.com/downloads)

Then, in Powershell:
```
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope Process
```
```
.\ci-scripts\prebuild.ps1
```
Note that the script uses 7zip, CMake, git and MSBuild, so make sure their paths
are in your environment variables.

## Compilation
### Linux
```
./ci-scripts/build.sh
```

### Windows 10
In Powershell:
```
.\ci-scripts\build.ps1
```

### With Qt Creator (Cross-platform)
- Open CMakeLists.txt with Qt Creator
- Build -> Build All or `Ctrl + Shift + B`

## Test
### Building tests
Googletest is used for testing. It is downloaded to the `external` directory with the setup script.

Enable building tests by passing -KTNENGINE_BUILD_UNIT_TESTS to CMake (enabled by default).

### Running tests
In Qt Creator: Tools -> Tests -> Run All Tests.
Alternatively, on Linux:
```
./ci-scripts/run-test.sh
```
To see what needs more testing, generate test coverage with the [script](ci-scripts/report-test-coverage.sh) (only on Linux):
```
./ci-scripts/report-test-coverage.sh
```