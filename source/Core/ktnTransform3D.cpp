#include <ktnEngine/Core/ktnTransform3D.hpp>

namespace ktn::Core {
bool ktnTransform3D::operator!=(const ktnTransform3D &other) const {
    return ModelMatrix != other.ModelMatrix //
           || Position != other.Position //
           || Rotation != other.Rotation //
           || Scale != other.Scale;
}
} // namespace ktn::Core
