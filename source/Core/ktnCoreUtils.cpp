#include <chrono>
#include <ktnEngine/Core/ktnCoreUtils.hpp>

namespace ktn::Core::Utils {
auto AreEqual(const glm::quat &q1, const glm::quat &q2, const float &threshold) -> bool {
    if (std::fabs(q1.w - q2.w) > threshold) { return false; }
    if (std::fabs(q1.x - q2.x) > threshold) { return false; }
    if (std::fabs(q1.y - q2.y) > threshold) { return false; }
    if (std::fabs(q1.z - q2.z) > threshold) { return false; }
    return true;
}

auto CompareFloat(const float &f1, const float &f2, const float &threshold) -> CompareFloatResult {
    if (std::fabs(f1 - f2) < threshold) { return CompareFloatResult::Equal; }
    if (f1 > f2) { return CompareFloatResult::Greater; }
    if (f1 < f2) { return CompareFloatResult::Smaller; }
    return CompareFloatResult::Equal;
}

auto CurrentMicrosecondsSinceEpoch() -> int64_t {
    return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

auto CurrentMillisecondsSinceEpoch() -> int64_t {
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

auto IsAlphabetic(const char &c) -> bool {
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}
} // namespace ktn::Core::Utils
