#include <ktnEngine/Core/ktnObject.hpp>

#include <utility>

using namespace std;
namespace ktn::Core {
ktnObject::ktnObject(const string &eName) : ktnSignalReceiver(), Name(eName) {}

ktnObject::ktnObject(const ktnObject &other) : ktnSignalReceiver(other), Name(other.Name) {}

ktnObject::~ktnObject() = default;

auto ktnObject::operator==(const ktnObject &eObj) const -> bool {
    return Name == eObj.Name;
}

auto ktnObject::operator!=(const ktnObject &eObj) const -> bool {
    return Name != eObj.Name;
}

auto ktnObject::operator=(const ktnObject &eObj) -> ktnObject & {
    Name = eObj.Name;
    return *this;
}

#ifndef NDEBUG
void ktnObject::PrintDebugInfo(std::ostream &os) {
    os << "ktnObject at 0x" << hex << reinterpret_cast<size_t>(this) << std::endl;
}
#endif
} // namespace ktn::Core
