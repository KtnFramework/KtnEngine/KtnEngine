#include <ktnEngine/Core/ktn3DElement.hpp>

#include <ktnEngine/Core/ktnCoreUtils.hpp>

#include <glm/gtc/matrix_transform.hpp>

#include <iostream>
#include <stdexcept>

using namespace glm;
using namespace std;

namespace ktn::Core {
ktn3DElement::ktn3DElement(const string &eName, ktn3DElement *eParent) : ktnObject(eName) {
    SetParent(eParent);
}

ktn3DElement::ktn3DElement(const ktn3DElement &other)
    : ktnObject(other), //
      m_Transform_LS(other.m_Transform_LS),
      m_Transform_WS(other.m_Transform_WS) {
    SetParent(other.m_Parent);
    UpdateLocalSpaceModelMatrix();
    UpdateWorldSpaceModelMatrix();
}

auto ktn3DElement::operator=(const ktn3DElement &other) -> ktn3DElement & {
    ktnObject::operator=(other);
    m_Transform_LS = other.m_Transform_LS;
    m_Transform_WS = other.m_Transform_WS;
    SetParent(other.m_Parent);
    return *this;
}

ktn3DElement::~ktn3DElement() {
    if (m_Parent != nullptr) { m_Parent->RemoveChild(this); }
    for (auto *child : m_Children) {
        child->m_Parent = nullptr;
        child->SetPosition(vec3(m_Transform_WS.ModelMatrix * vec4(child->m_Transform_LS.Position, 1)), WorldSpace);
        child->SetRotation(m_Transform_WS.Rotation * m_Transform_WS.Rotation, WorldSpace);
        child->SetPosition(vec3(m_Transform_WS.ModelMatrix * vec4(child->m_Transform_LS.Scale, 1)), WorldSpace);
    }
}

auto ktn3DElement::operator==(const ktn3DElement &other) const -> bool {
    return !operator!=(other);
}

auto ktn3DElement::operator!=(const ktn3DElement &other) const -> bool {
    return ktnObject::operator!=(other) //
           || m_Parent != other.m_Parent //
           || (m_Transform_LS != other.m_Transform_LS) //
           || m_Transform_WS != other.m_Transform_WS //
           || m_AnimatedMatrix_LS != other.m_AnimatedMatrix_LS //
           || m_AnimatedMatrix_WS != other.m_AnimatedMatrix_WS;
}

auto ktn3DElement::AnimatedModelMatrix(ReferenceSpace eSpace) const -> mat4 {
    switch (eSpace) {
    case LocalSpace:
        return m_AnimatedMatrix_LS;
    case WorldSpace:
        return m_AnimatedMatrix_WS;
    }

    throw runtime_error("Unknown reference space.");
}

auto ktn3DElement::ModelMatrix(ReferenceSpace eSpace) const -> mat4 {
    switch (eSpace) {
    case LocalSpace:
        return m_Transform_LS.ModelMatrix;
    case WorldSpace:
        return m_Transform_WS.ModelMatrix;
    }

    throw runtime_error("Unknown reference space.");
}

auto ktn3DElement::Parent() const -> ktn3DElement * {
    return m_Parent;
}

auto ktn3DElement::Children() -> const std::vector<ktn3DElement *> * {
    return &m_Children;
}

auto ktn3DElement::Position(ReferenceSpace eSpace) const -> vec3 {
    switch (eSpace) {
    case LocalSpace:
        return m_Transform_LS.Position;
    case WorldSpace:
        return m_Transform_WS.Position;
    }

    throw runtime_error("Unknown reference space.");
}

auto ktn3DElement::Scale(ReferenceSpace eSpace) const -> vec3 {
    switch (eSpace) {
    case LocalSpace:
        return m_Transform_LS.Scale;
    case WorldSpace:
        return m_Transform_WS.Scale;
    }

    throw runtime_error("Unknown reference space.");
}

auto ktn3DElement::Rotation(ReferenceSpace eSpace) const -> quat {
    switch (eSpace) {
    case LocalSpace:
        return m_Transform_LS.Rotation;
    case WorldSpace:
        return m_Transform_WS.Rotation;
    }

    throw runtime_error("Unknown reference space.");
}

void ktn3DElement::SetParent(ktn3DElement *eParent) {
    if (m_Parent != nullptr) { m_Parent->RemoveChild(this); }
    m_Parent = eParent;
    if (m_Parent != nullptr) { m_Parent->AddChild(this); }
}

void ktn3DElement::SetPosition(const vec3 &ePosition, ReferenceSpace eSpace) {
    switch (eSpace) {
    case LocalSpace:
        m_Transform_LS.Position = ePosition;
        if (m_Parent != nullptr) {
            m_Transform_WS.Position = m_Parent->ModelMatrix(WorldSpace) * vec4(m_Transform_LS.Position, 1);
        } else {
            m_Transform_WS.Position = m_Transform_LS.Position;
        }
        UpdateLocalSpaceModelMatrix();
        UpdateWorldSpaceModelMatrix();
        break;

    case WorldSpace:
        m_Transform_WS.Position = ePosition;
        if (m_Parent != nullptr) {
            UpdateWorldSpaceModelMatrix();
            m_Transform_LS.Position = inverse(m_Parent->m_Transform_WS.ModelMatrix) * vec4(m_Transform_WS.Position, 1);
            UpdateLocalSpaceModelMatrix();
        } else {
            m_Transform_LS.Position = m_Transform_WS.Position;
            UpdateLocalSpaceModelMatrix();
            UpdateWorldSpaceModelMatrix();
        }
        break;
    }

    for (auto &child : m_Children) {
        child->SetPosition(child->m_Transform_LS.Position, LocalSpace);
        child->SetRotation(child->m_Transform_LS.Rotation, LocalSpace);
        child->SetScale(child->m_Transform_LS.Scale, LocalSpace);
    }
}

void ktn3DElement::SetRotation(const quat &eRotationQuaternion, ReferenceSpace eSpace) {
    ValidateRotationQuaternion(eRotationQuaternion);
    switch (eSpace) {
    case LocalSpace:
        m_Transform_LS.Rotation = eRotationQuaternion;
        if (m_Parent != nullptr) {
            m_Transform_WS.Rotation = m_Transform_LS.Rotation * m_Parent->Rotation(WorldSpace);
        } else {
            m_Transform_WS.Rotation = m_Transform_LS.Rotation;
        }
        UpdateLocalSpaceModelMatrix();
        UpdateWorldSpaceModelMatrix();
        break;

    case WorldSpace:
        m_Transform_WS.Rotation = eRotationQuaternion;
        if (m_Parent != nullptr) {
            UpdateWorldSpaceModelMatrix();
            m_Transform_LS.Rotation = conjugate(m_Parent->Rotation(WorldSpace)) * m_Transform_WS.Rotation;
            UpdateLocalSpaceModelMatrix();
        } else {
            m_Transform_LS.Rotation = m_Transform_WS.Rotation;
            UpdateLocalSpaceModelMatrix();
            UpdateWorldSpaceModelMatrix();
        }
        break;
    }

    for (auto &child : m_Children) {
        child->SetPosition(child->m_Transform_LS.Position, LocalSpace);
        child->SetRotation(child->m_Transform_LS.Rotation, LocalSpace);
        child->SetScale(child->m_Transform_LS.Scale, LocalSpace);
    }
}

void ktn3DElement::SetScale(float eScale, ReferenceSpace eSpace) {
    SetScale(vec3(eScale), eSpace);
}

void ktn3DElement::SetScale(const vec3 &eScale, ReferenceSpace eSpace) {
    switch (eSpace) {
    case LocalSpace:
        m_Transform_LS.Scale = eScale;
        if (m_Parent != nullptr) {
            m_Transform_WS.Scale = m_Parent->m_Transform_WS.Scale * m_Transform_LS.Scale;
        } else {
            m_Transform_WS.Scale = m_Transform_LS.Scale;
        }
        UpdateLocalSpaceModelMatrix();
        UpdateWorldSpaceModelMatrix();
        break;

    case WorldSpace:
        m_Transform_WS.Scale = eScale;
        if (m_Parent != nullptr) {
            UpdateWorldSpaceModelMatrix();
            m_Transform_LS.Scale.x = m_Transform_WS.Scale.x / m_Parent->m_Transform_WS.Scale.x;
            m_Transform_LS.Scale.x = m_Transform_WS.Scale.y / m_Parent->m_Transform_WS.Scale.y;
            m_Transform_LS.Scale.x = m_Transform_WS.Scale.z / m_Parent->m_Transform_WS.Scale.z;
            UpdateLocalSpaceModelMatrix();
        } else {
            m_Transform_LS.Scale = m_Transform_WS.Scale;
            UpdateLocalSpaceModelMatrix();
            UpdateWorldSpaceModelMatrix();
        }
        break;
    }

    for (auto &child : m_Children) {
        child->SetPosition(child->m_Transform_LS.Position, LocalSpace);
        child->SetRotation(child->m_Transform_LS.Rotation, LocalSpace);
        child->SetScale(child->m_Transform_LS.Scale, LocalSpace);
    }
}

void ktn3DElement::SetWorldSpacePosition(glm::vec3 ePosition) {
    SetPosition(ePosition, WorldSpace);
}

void ktn3DElement::SetWorldSpaceRotation(glm::quat eRotation) {
    SetRotation(eRotation, WorldSpace);
}

void ktn3DElement::OffsetPosition(const vec3 &offset, ReferenceSpace eSpace) {
    switch (eSpace) {
    case LocalSpace:
        m_Transform_LS.Position += offset;
        if (m_Parent != nullptr) {
            vec4 temp = m_Parent->ModelMatrix(WorldSpace) * vec4(m_Transform_LS.Position, 1);
            m_Transform_WS.Position = temp;
        } else {
            m_Transform_WS.Position = m_Transform_LS.Position;
        }
        UpdateLocalSpaceModelMatrix();
        UpdateWorldSpaceModelMatrix();
        break;

    case WorldSpace:
        m_Transform_WS.Position += offset;
        if (m_Parent != nullptr) {
            UpdateWorldSpaceModelMatrix();
            vec4 temp = inverse(m_Transform_WS.ModelMatrix) * vec4(m_Transform_WS.Position, 1);
            m_Transform_LS.Position = temp;
            UpdateLocalSpaceModelMatrix();
        } else {
            m_Transform_LS.Position = m_Transform_WS.Position;
            UpdateLocalSpaceModelMatrix();
            UpdateWorldSpaceModelMatrix();
        }
        break;
    }

    for (auto &child : m_Children) {
        child->SetPosition(child->m_Transform_LS.Position, LocalSpace);
        child->SetRotation(child->m_Transform_LS.Rotation, LocalSpace);
        child->SetScale(child->m_Transform_LS.Scale, LocalSpace);
    }
}

void ktn3DElement::ResetAnimatedTransforms() {
    for (auto &[actionName, locrotscale] : AnimatedInfluences) { locrotscale = ktnPosRotScaleInfluence(); }
}

quat AverageQuaternion_Inaccurate(const std::map<std::string, ktnPosRotScaleInfluence> eAnimatedInfluences) { // TODO: implement accurate version
    if (eAnimatedInfluences.empty()) { return DefaultRotation; }

    quat temp(0, 0, 0, 0);
    for (const auto &[index, value] : eAnimatedInfluences) { //
        if (length(value.Rotation - eAnimatedInfluences.begin()->second.Rotation) //
            < length(-value.Rotation - eAnimatedInfluences.begin()->second.Rotation)) {
            temp += glm::lerp(quat(0, 0, 0, 0), value.Rotation, value.Influence);
        } else {
            temp -= glm::lerp(quat(0, 0, 0, 0), value.Rotation, value.Influence);
        }
    }

    return normalize(temp);
}

void ktn3DElement::UpdateAnimatedLocalSpaceModelMatrix() {
    if (AnimatedInfluences.empty()) {
        m_AnimatedMatrix_LS = IDENTITY_MATRIX_4;
        return;
    }

    vec3 pos = DefaultTranslation;
    vec3 scl = DefaultScale;

    float total_influence = 0;
    for (auto &[key, value] : AnimatedInfluences) {
        if (Utils::CompareFloat(value.Influence, 0, 1e-6) == Utils::CompareFloatResult::Greater) {
            pos += value.Position * value.Influence;

            // TODO: add explanation, preferably in a md file or this function's doc
            scl = scl * (DefaultScale + ((value.Scale - DefaultScale) * value.Influence));
            total_influence += value.Influence;
        }
    }

    quat rot(AverageQuaternion_Inaccurate(AnimatedInfluences));

    if (Utils::CompareFloat(total_influence, 0, 1e-6) == Utils::CompareFloatResult::Equal) {
        m_AnimatedMatrix_LS = IDENTITY_MATRIX_4;
        return;
    }

    m_AnimatedMatrix_LS = translate(IDENTITY_MATRIX_4, pos / total_influence);
    m_AnimatedMatrix_LS *= toMat4(rot);
    m_AnimatedMatrix_LS = scale(m_AnimatedMatrix_LS, scl);
}

void ktn3DElement::UpdateAnimatedWorldSpaceModelMatrix() {
    if (nullptr == m_Parent) {
        m_AnimatedMatrix_WS = m_AnimatedMatrix_LS * m_Transform_WS.ModelMatrix;
    } else {
        // matrix multiplication back to front
        m_AnimatedMatrix_WS = IDENTITY_MATRIX_4;
        m_AnimatedMatrix_WS *= m_Parent->m_AnimatedMatrix_WS; // at the end, transform based on animated model matrix of parent
        m_AnimatedMatrix_WS *= m_AnimatedMatrix_LS; // transform based on the local space model matrix (non-animated)
    }
    for (auto &child : m_Children) { //
        child->UpdateAnimatedWorldSpaceModelMatrix();
    }
}

#ifndef NDEBUG
void ktn3DElement::PrintDebugInfo(std::ostream &os) {
    os << "ktn3DElement: ";
    ktnObject::PrintDebugInfo();
    {
        os << "Transformation: " << endl;
        os << "[Local space]";
        os << "    Position: " //
           << m_Transform_LS.Position.x << " " << m_Transform_LS.Position.y << " " << m_Transform_LS.Position.z;
        os << "    Rotation: " //
           << m_Transform_LS.Rotation.x << " " << m_Transform_LS.Rotation.y << " " << m_Transform_LS.Rotation.z << " " << m_Transform_LS.Rotation.z;
        os << "    Scale: " << //
            m_Transform_LS.Scale.x << " " << m_Transform_LS.Scale.y << " " << m_Transform_LS.Scale.z << endl;

        auto &tempModel = m_Transform_LS.ModelMatrix;
        os << "[Local space]";
        os << "    Model Matrix: ";
        os << "[[" << tempModel[0][0] << " " << tempModel[0][1] << " " << tempModel[0][2] << " " << tempModel[0][3] << "],"
           << " [" << tempModel[1][0] << " " << tempModel[1][1] << " " << tempModel[1][2] << " " << tempModel[1][3] << "],"
           << " [" << tempModel[2][0] << " " << tempModel[2][1] << " " << tempModel[2][2] << " " << tempModel[2][3] << "], "
           << " [" << tempModel[3][0] << " " << tempModel[3][1] << " " << tempModel[3][2] << " " << tempModel[3][3] << "]]" << endl;
    }
    {
        os << "[World space]";
        os << "    Position: " //
           << m_Transform_WS.Position.x << " " << m_Transform_WS.Position.y << " " << m_Transform_WS.Position.z;
        os << "    Rotation: " //
           << m_Transform_WS.Rotation.x << " " << m_Transform_WS.Rotation.y << " " << m_Transform_WS.Rotation.z << " " << m_Transform_WS.Rotation.z;
        os << "    Scale: " //
           << m_Transform_WS.Scale.x << " " << m_Transform_WS.Scale.y << " " << m_Transform_WS.Scale.z << endl;

        auto &tempModel = m_Transform_WS.ModelMatrix;
        os << "[World space]";
        os << "    Model Matrix: ";
        os << "[[" << tempModel[0][0] << " " << tempModel[0][1] << " " << tempModel[0][2] << " " << tempModel[0][3] << "],"
           << " [" << tempModel[1][0] << " " << tempModel[1][1] << " " << tempModel[1][2] << " " << tempModel[1][3] << "],"
           << " [" << tempModel[2][0] << " " << tempModel[2][1] << " " << tempModel[2][2] << " " << tempModel[2][3] << "],"
           << " [" << tempModel[3][0] << " " << tempModel[3][1] << " " << tempModel[3][2] << " " << tempModel[3][3] << "]]" << endl;
    }
}
#endif

void ktn3DElement::UpdateLocalSpaceModelMatrix() {
    m_Transform_LS.ModelMatrix = translate(IDENTITY_MATRIX_4, m_Transform_LS.Position);
    auto rotationMatrix = toMat4(m_Transform_LS.Rotation);
    m_Transform_LS.ModelMatrix *= rotationMatrix;
    m_Transform_LS.ModelMatrix = scale(m_Transform_LS.ModelMatrix, m_Transform_LS.Scale);
}

void ktn3DElement::UpdateWorldSpaceModelMatrix() {
    m_Transform_WS.ModelMatrix = translate(IDENTITY_MATRIX_4, m_Transform_WS.Position);
    auto rotationMatrix = toMat4(m_Transform_WS.Rotation);
    m_Transform_WS.ModelMatrix *= rotationMatrix;
    m_Transform_WS.ModelMatrix = scale(m_Transform_WS.ModelMatrix, m_Transform_WS.Scale);
}

void ktn3DElement::AddChild(ktn3DElement *eChild) {
    m_Children.emplace_back(eChild);
}

void ktn3DElement::RemoveChild(ktn3DElement *eChild) {
    m_Children.erase(remove(m_Children.begin(), m_Children.end(), eChild));
}

quat ktn3DElement::ValidateRotationQuaternion(const quat &eQuaternion) {
#ifndef NDEBUG
    quat temp(normalize(eQuaternion));
    if (!Utils::AreEqual(eQuaternion, temp, 1e-3F)) {
        cerr << __FUNCTION__ << ": the given rotation quaternion is not normalized." << endl;
        cerr << "  Original: " << eQuaternion.x << " " << eQuaternion.y << " " << eQuaternion.z << " " << eQuaternion.w << endl;
        cerr << "  Normalized: " << temp.x << " " << temp.y << " " << temp.z << " " << temp.w << endl;
    }
    return temp;
#else
    return eQuaternion;
#endif
}
} // namespace ktn::Core
