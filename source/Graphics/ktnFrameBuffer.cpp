#include <ktnEngine/Graphics/ktnFrameBuffer.hpp>

#include <iostream>

using namespace std;

namespace ktn::Graphics {
ktnFrameBuffer::ktnFrameBuffer() = default;

ktnFrameBuffer::~ktnFrameBuffer() = default;
} // namespace ktn::Graphics
