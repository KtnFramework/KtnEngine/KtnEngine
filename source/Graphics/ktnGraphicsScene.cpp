﻿#include <ktnEngine/Graphics/ktnGraphicsScene.hpp>

#include <iostream>

using ktn::Core::ktnName;

using namespace std;

namespace ktn::Graphics {
ktnGraphicsScene::ktnGraphicsScene() = default;

ktnGraphicsScene::~ktnGraphicsScene() = default;

void ktnGraphicsScene::AddCamera(shared_ptr<ktnCamera> eCamera) {
    if (Cameras.empty()) {
        eCamera->Name.AppendSuffix();
        Cameras.push_back(eCamera);
        return;
    }

    ktnName tempName;
    tempName = eCamera->Name;
    tempName.AppendSuffix();

    bool duplicateNameFound = false;
    for (const auto &camera : Cameras) {
        if (*camera == *eCamera) { return; }
        if (camera->Name == tempName) {
            tempName.BumpSuffix();
            duplicateNameFound = true;
        }
    }

    if (duplicateNameFound) {
        cout << "ktnGraphicsScene: --duplicate name detected" << endl;
        cout << "ktnGraphicsScene: --changing name of new camera to \"" << tempName.ToStdString() << "\"" << endl;
        eCamera->Name = tempName;
    }
    Cameras.push_back(eCamera);
}

void ktnGraphicsScene::RemoveCamera(std::shared_ptr<ktnCamera> eCamera) {
    Cameras.erase(std::remove(Cameras.begin(), Cameras.end(), eCamera), Cameras.end());

    if (eCamera == m_CurrentCamera) {
        if (Cameras.empty()) { cerr << "Warning: no camera left in scene after removal." << endl; }
    }
}

void ktnGraphicsScene::AddLight(shared_ptr<ktnLight> eLight) {
    if (Lights.empty()) {
        eLight->Name.AppendSuffix();
        Lights.push_back(eLight);
        return;
    }

    ktnName tempName;
    tempName = eLight->Name;
    tempName.AppendSuffix();

    bool duplicateNameFound = false;
    for (const auto &light : Lights) {
        if (light == eLight) { return; }
        if (light->Name == tempName) {
            tempName.BumpSuffix();
            duplicateNameFound = true;
        }
    }

    if (duplicateNameFound) {
        cout << "ktnGraphicsScene: --duplicate name detected" << endl;
        cout << "ktnGraphicsScene: --changing name of new light to \"" << tempName.ToStdString() << "\"" << endl;
        eLight->Name = tempName;
    }
    Lights.push_back(eLight);
}

void ktnGraphicsScene::RemoveLight(shared_ptr<ktnLight> eLight) {
    Lights.erase(std::remove(Lights.begin(), Lights.end(), eLight), Lights.end());
}

void ktnGraphicsScene::AddModel(shared_ptr<ktnModel> eModel) {
    eModel->Name.AppendSuffix();
    ktnName tempName = eModel->Name;
    if (!Models.empty()) {
        for (auto model : Models) {
            if (*model == *eModel) { return; }
            if (model->Name == tempName) { tempName.BumpSuffix(); }
        }
        if (tempName != eModel->Name) {
            cout << "ktnGraphicsScene: --duplicate name detected" << endl;
            cout << "ktnGraphicsScene: --changing name of new model to \"" << tempName.ToStdString() << "\"" << endl;
            eModel->Name = tempName;
        }
    }
    Models.emplace_back(eModel);
}

void ktnGraphicsScene::RemoveModel(shared_ptr<ktnModel> eModel) {
    Models.erase(std::remove(Models.begin(), Models.end(), eModel), Models.end());
}

auto ktnGraphicsScene::CurrentCamera() const -> shared_ptr<ktnCamera> {
    return m_CurrentCamera;
}

void ktnGraphicsScene::SetCurrentCamera(shared_ptr<ktnCamera> eCamera) {
    for (auto &camera : Cameras) {
        if (eCamera == camera) {
            m_CurrentCamera = camera;
            return;
        }
    }

    AddCamera(eCamera);
    SetCurrentCamera(eCamera);
}
} // namespace ktn::Graphics
