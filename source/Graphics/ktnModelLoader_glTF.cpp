#include <ktnEngine/Graphics/ktnModelLoader_glTF.hpp>

namespace ktn::Graphics {
using namespace Core;
using namespace gleb;
using namespace glm;
using namespace std;

ktnModelLoader_glTF::~ktnModelLoader_glTF() = default;

void ktnModelLoader_glTF::Initialize(shared_ptr<gleb::glTF> eGltf) {
    m_Gltf = eGltf;
}

std::map<size_t, std::shared_ptr<ktnAction>> ktnModelLoader_glTF::LoadActions() {
    std::map<size_t, std::shared_ptr<ktnAction>> actions;
    for (size_t index_animation = 0; index_animation < m_Gltf.lock()->animations.size(); ++index_animation) {
        auto gltfAnimation = m_Gltf.lock()->animations.at(index_animation);
        auto action = make_shared<ktnAction>();
        action->LoadFromGLTF(*m_Gltf.lock(), index_animation);
        actions[index_animation] = action;
    }

    return actions;
}

map<size_t, ktnMesh *> ktnModelLoader_glTF::LoadMeshes(ktn3DElement *eParent) {
    map<size_t, ktnMesh *> meshes;
    if (m_Gltf.lock()->scene.has_value()) {
        auto index_default_scene = m_Gltf.lock()->scene.value();
        for (auto &index_root_node : m_Gltf.lock()->scenes[index_default_scene].nodes) { //
            LoadMeshFromNode(m_Gltf.lock(), index_root_node, meshes, eParent);
        }
    } else {
        throw invalid_argument("No default scene found in " + m_Gltf.lock()->Source);
    }

    return meshes;
}

map<size_t, ktnSkin *> ktnModelLoader_glTF::LoadSkins() {
    map<size_t, ktnSkin *> Skins;
    for (size_t index_skin = 0; index_skin < m_Gltf.lock()->skins.size(); ++index_skin) { Skins[index_skin] = LoadSkin(m_Gltf.lock(), index_skin); }
    return Skins;
}

ktnSkin *ktnModelLoader_glTF::LoadSkin(const shared_ptr<glTF> eGltf, size_t eIndex) {
    auto *skin = new ktnSkin(eGltf->skins[eIndex].name.value_or("skin"), nullptr);
    skin->Joints.resize(eGltf->skins[eIndex].joints.size());

    // load all joints
    for (size_t i = 0; i < eGltf->skins[eIndex].joints.size(); ++i) {
        skin->Joints[i] = make_pair(eGltf->skins[eIndex].joints[i], LoadJoint(eGltf, eGltf->skins[eIndex].joints[i], skin));
    }

    // load all inverse bind matrices and assign them to their respective joints
    if (eGltf->skins.at(eIndex).inverseBindMatrices.has_value()) {
        auto index_accessor_ibm = eGltf->skins[eIndex].inverseBindMatrices.value();
        auto index_bufferView_ibm = eGltf->accessors[index_accessor_ibm].bufferView.value();
        auto index_buffer_ibm = eGltf->bufferViews.at(index_bufferView_ibm).buffer;
        auto byteLength_buffer_ibm = eGltf->bufferViews.at(index_bufferView_ibm).byteLength;
        auto byteOffset_buffer_ibm = eGltf->bufferViews.at(index_bufferView_ibm).byteOffset;
        std::vector<mat4> ibms;
        ibms.resize(eGltf->accessors[index_accessor_ibm].count);
        memcpy(ibms.data(), eGltf->buffers[index_buffer_ibm].data().data() + byteOffset_buffer_ibm.value_or(0), byteLength_buffer_ibm);
        for (size_t i = 0; i < ibms.size(); ++i) { skin->Joints.at(i).second->SetInverseBindMatrix(ibms.at(i)); }
    } else {
        cerr << "No inverse bind matrices buffer found." << endl;
    }

    // connect joints with their parents
    for (size_t i = 0; i < eGltf->skins[eIndex].joints.size(); ++i) {
        auto index_joint = eGltf->skins[eIndex].joints[i];
        auto parent = std::find_if(skin->Joints.begin(), skin->Joints.end(), [&index_joint](auto value) { return value.first == index_joint; });
        for (auto &index_child : eGltf->nodes[index_joint].children) {
            for (auto joint : skin->Joints) {
                if (index_child == joint.first) { joint.second->SetParent(parent->second); }
            }
        }
    }
    return skin;
}

ktnJoint *ktnModelLoader_glTF::LoadJoint(const shared_ptr<glTF> eGltf, size_t eIndex, ktn3DElement *eSkin) {
    auto node = eGltf->nodes[eIndex];
    auto *joint = new ktnJoint(node.name.value_or(eGltf->nodes[eIndex].Identifier), eSkin);

    if (node.rotation.has_value()) {
        auto rot = quat(
            node.rotation.value()[3],
            vec3{
                node.rotation.value()[0],
                node.rotation.value()[1],
                node.rotation.value()[2],
            });
        joint->SetRotation(rot, LocalSpace);
    }
    if (node.scale.has_value()) {
        auto scale = vec3{node.scale.value()[0], node.scale.value()[1], node.scale.value()[2]};
        joint->SetScale(scale, LocalSpace);
    }
    if (node.translation.has_value()) {
        auto translation = vec3{node.translation.value()[0], node.translation.value()[1], node.translation.value()[2]};
        joint->SetPosition(translation, LocalSpace);
    }
    return joint;
}

void ktnModelLoader_glTF::LoadMeshFromNode(
    std::shared_ptr<gleb::glTF> eGltf, size_t eNodeIndex, std::map<size_t, ktnMesh *> &eMeshMap, Core::ktn3DElement *eParent) {
    // TODO: a mesh may have more than one primitives. Add a class to handle this.
    // The current version assumes the mesh contains only 1 primitive.

    if (eGltf->nodes.at(eNodeIndex).mesh.has_value()) {
        auto index_mesh = eGltf->nodes.at(eNodeIndex).mesh.value();
        if (eGltf->meshes[index_mesh].primitives.size() > 1) { throw runtime_error(string(__FUNCTION__) + ": multiple primitives per mesh not yet supported"); }

        auto *mesh = new ktnMesh(eGltf->meshes[index_mesh].Identifier, eParent);
        auto primitive = eGltf->meshes[index_mesh].primitives.at(0);
        size_t buffer_size = 0;

        size_t byteLength_position = 0;
        { // assume POSITION always exists
            uint32_t index_accessor_position = primitive.attributes["POSITION"].GetUint();
            size_t index_bv_position = eGltf->accessors.at(index_accessor_position).bufferView.value();
            size_t index_buffer_position = eGltf->bufferViews.at(index_bv_position).buffer;
            byteLength_position = eGltf->bufferViews.at(index_bv_position).byteLength;
            size_t count_position = eGltf->accessors.at(index_accessor_position).count;
            mesh->Positions.resize(count_position);
            memcpy(mesh->Positions.data(), eGltf->buffers.at(index_buffer_position).data().data(), byteLength_position);
            buffer_size += count_position * 3;
        }

        size_t byteLength_normal = 0;
        if (primitive.attributes.HasMember("NORMAL")) {
            uint32_t index_accessor_normal = primitive.attributes["NORMAL"].GetUint();
            size_t index_bv_normal = eGltf->accessors.at(index_accessor_normal).bufferView.value();
            size_t index_buffer_normal = eGltf->bufferViews.at(index_bv_normal).buffer;
            byteLength_normal = eGltf->bufferViews.at(index_bv_normal).byteLength;
            size_t offset = eGltf->bufferViews.at(index_bv_normal).byteOffset.value_or(gleb::default_byteOffset);

            size_t count_normal = eGltf->accessors.at(index_accessor_normal).count;

            mesh->Normals.resize(count_normal);
            memcpy(mesh->Normals.data(), eGltf->buffers.at(index_buffer_normal).data().data() + offset, byteLength_normal);
            buffer_size += count_normal * 3;
        }

        size_t byteLength_tangent = 0;
        if (primitive.attributes.HasMember("TANGENT")) {
            uint32_t index_accessor_tangent = primitive.attributes["TANGENT"].GetUint();
            size_t index_bv_tangent = eGltf->accessors.at(index_accessor_tangent).bufferView.value();
            size_t index_buffer_tangent = eGltf->bufferViews.at(index_bv_tangent).buffer;
            byteLength_tangent = eGltf->bufferViews.at(index_bv_tangent).byteLength;
            size_t offset = eGltf->bufferViews.at(index_bv_tangent).byteOffset.value_or(gleb::default_byteOffset);
            size_t count_tangent = eGltf->accessors.at(index_accessor_tangent).count;
            mesh->Tangents.resize(count_tangent);
            memcpy(mesh->Tangents.data(), eGltf->buffers.at(index_buffer_tangent).data().data() + offset, byteLength_tangent);
            buffer_size += count_tangent * 4;
        }

        size_t byteLength_texcoord = 0;
        if (primitive.attributes.HasMember("TEXCOORD_0")) {
            uint32_t index_accessor_texcoord = primitive.attributes["TEXCOORD_0"].GetUint();
            size_t index_bv_texcoord = eGltf->accessors.at(index_accessor_texcoord).bufferView.value();
            size_t index_buffer_texcoord = eGltf->bufferViews.at(index_bv_texcoord).buffer;
            byteLength_texcoord = eGltf->bufferViews.at(index_bv_texcoord).byteLength;
            size_t offset = eGltf->bufferViews.at(index_bv_texcoord).byteOffset.value_or(gleb::default_byteOffset);
            size_t count_texcoord = eGltf->accessors.at(index_accessor_texcoord).count;
            mesh->TexCoords.resize(count_texcoord);

            memcpy(mesh->TexCoords.data(), eGltf->buffers.at(index_buffer_texcoord).data().data() + offset, byteLength_texcoord);
            buffer_size += count_texcoord * 2;
        }

        size_t byteLength_joints0 = 0;
        if (primitive.attributes.HasMember("JOINTS_0")) {
            uint32_t index_accessor_joints0 = primitive.attributes["JOINTS_0"].GetUint();
            size_t index_bv_joints0 = eGltf->accessors.at(index_accessor_joints0).bufferView.value();
            size_t index_buffer_joints0 = eGltf->bufferViews.at(index_bv_joints0).buffer;
            byteLength_joints0 = eGltf->bufferViews.at(index_bv_joints0).byteLength;
            size_t offset = eGltf->bufferViews.at(index_bv_joints0).byteOffset.value_or(gleb::default_byteOffset);
            size_t count_joints0 = eGltf->accessors.at(index_accessor_joints0).count;
            mesh->Joints = vector<uvec4>();
            mesh->Joints.value().resize(count_joints0);

            // Joints are loaded into graphics card as 32-bit unsigned interger,
            // but is saved in gltf as 16 bit.
            auto temp = vector<array<uint16_t, 4>>();
            temp.resize(count_joints0);
            memcpy(temp.data(), eGltf->buffers.at(index_buffer_joints0).data().data() + offset, byteLength_joints0);
            for (size_t i = 0; i < temp.size(); ++i) {
                mesh->Joints.value().at(i).x = temp.at(i).at(0);
                mesh->Joints.value().at(i).y = temp.at(i).at(1);
                mesh->Joints.value().at(i).z = temp.at(i).at(2);
                mesh->Joints.value().at(i).w = temp.at(i).at(3);
            }

            buffer_size += count_joints0 * 4;
        } else {
            buffer_size += mesh->Positions.size() * 4;
        }

        size_t byteLength_weights0 = 0;
        if (primitive.attributes.HasMember("WEIGHTS_0")) {
            uint32_t index_accessor_weights0 = primitive.attributes["WEIGHTS_0"].GetUint();
            size_t index_bv_weights0 = eGltf->accessors.at(index_accessor_weights0).bufferView.value();
            size_t index_buffer_weights0 = eGltf->bufferViews.at(index_bv_weights0).buffer;
            byteLength_weights0 = eGltf->bufferViews.at(index_bv_weights0).byteLength;
            size_t offset = eGltf->bufferViews.at(index_bv_weights0).byteOffset.value_or(gleb::default_byteOffset);
            size_t count_weights0 = eGltf->accessors.at(index_accessor_weights0).count;
            mesh->Weights = vector<vec4>();
            mesh->Weights.value().resize(count_weights0);

            memcpy(mesh->Weights.value().data(), eGltf->buffers.at(index_buffer_weights0).data().data() + offset, byteLength_weights0);
            buffer_size += count_weights0 * 4;
        } else {
            buffer_size += mesh->Positions.size() * 4;
        }

        // TODO: this is probably slow but no other options now
        mesh->Buffer.resize(buffer_size);
        size_t buffer_copy_offset = 0;

        memcpy(mesh->Buffer.data(), mesh->Positions.data(), byteLength_position);
        buffer_copy_offset += mesh->Positions.size() * 3;

        memcpy(mesh->Buffer.data() + buffer_copy_offset, mesh->Normals.data(), byteLength_normal);
        buffer_copy_offset += mesh->Positions.size() * 3;

        memcpy(mesh->Buffer.data() + buffer_copy_offset, mesh->Tangents.data(), byteLength_tangent);
        buffer_copy_offset += mesh->Positions.size() * 4;

        memcpy(mesh->Buffer.data() + buffer_copy_offset, mesh->TexCoords.data(), byteLength_texcoord);
        buffer_copy_offset += mesh->Positions.size() * 2;

        memcpy(
            mesh->Buffer.data() + buffer_copy_offset,
            mesh->Joints.has_value() ? mesh->Joints.value().data() : vector(mesh->Positions.size(), uvec4(0)).data(),
            byteLength_joints0 * 2);
        buffer_copy_offset += mesh->Positions.size() * 4;

        memcpy(
            mesh->Buffer.data() + buffer_copy_offset,
            mesh->Weights.has_value() ? mesh->Weights.value().data() : vector(mesh->Positions.size(), vec4(0)).data(),
            byteLength_weights0);

        size_t index_accessor_index = primitive.indices.value();
        size_t index_bv_index = eGltf->accessors.at(index_accessor_index).bufferView.value();
        size_t index_buffer_index = eGltf->bufferViews.at(index_bv_index).buffer;
        size_t offset = eGltf->bufferViews.at(index_bv_index).byteOffset.value_or(gleb::default_byteOffset);
        size_t byteLength_index = eGltf->bufferViews.at(index_bv_index).byteLength;
        size_t count_index = eGltf->accessors.at(index_accessor_index).count;

        // indices are loaded into graphics card as 32-bit unsigned interger, but is saved in gltf as 16 bit
        mesh->Indices.resize(count_index);
        std::vector<uint16_t> temp;
        temp.resize(count_index);
        memcpy(temp.data(), eGltf->buffers.at(index_buffer_index).data().data() + offset, byteLength_index);
        for (size_t i = 0; i < temp.size(); ++i) { mesh->Indices.at(i) = temp.at(i); }

        if (primitive.material.has_value()) {
            size_t index_material = primitive.material.value();
            mesh->Material.doubleSided = eGltf->materials.at(index_material).doubleSided;
            mesh->Material.Name = eGltf->materials.at(index_material).name.value();

            // base color texture
            if (eGltf->materials.at(index_material).pbrMetallicRoughness != nullptr) {
                mesh->Material.pbrMetallicRoughness = make_shared<ktnMaterial::pbrMetallicRoughnessInfo>();

                auto *mat = &eGltf->materials.at(index_material);
                if (mat->pbrMetallicRoughness->baseColorTexture != nullptr) {
                    size_t index_image = mat->pbrMetallicRoughness->baseColorTexture->textureObject()->source.value();
                    string uri = eGltf->images.at(index_image).uri.value();
                    size_t texCoords = mat->pbrMetallicRoughness->baseColorTexture->texCoord;
                    mesh->Material.pbrMetallicRoughness->baseColorTexture =
                        make_shared<ktnMaterial::pbrMetallicRoughnessInfo::baseColorTexture_t>(uri, texCoords);
                }
            }
        }

        for (auto &index_child : eGltf->nodes.at(eNodeIndex).children) { LoadMeshFromNode(eGltf, index_child, eMeshMap, mesh); }

        eMeshMap[index_mesh] = mesh;
    }
}
} // namespace ktn::Graphics
