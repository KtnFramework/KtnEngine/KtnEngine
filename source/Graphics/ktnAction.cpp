#include <ktnEngine/Graphics/ktnAction.hpp>

#include <ktnEngine/Core/ktnCoreUtils.hpp>
#include <ktnEngine/Graphics/ktnGraphicsCommon.hpp>
#include <ktnEngine/Graphics/ktnGraphicsUtils.hpp>

#include <glm/glm.hpp>

#include <climits>

namespace ktn::Graphics {
using namespace glm;
using namespace std;
using namespace Core::Utils;

using ChannelPath = ktnAction::Channel::ChannelPath;
using InterpolationAlgorithm = ktnActionSampler::InterpolationAlgorithm;
using Core::Utils::CompareFloat;
using Core::Utils::CompareFloatResult;
constexpr float MS_IN_S(1000.0F); // number of milliseconds in a second

float ktnAction::Duration() const {
    assert(m_Duration != nullptr);
    return *m_Duration;
}

void ktnAction::CalculateDuration() {
    float maxTime = 0.F;
    float minTime = FLT_MAX;
    for (auto &sampler : Samplers) {
        switch (sampler.second->Type) {
        case ktnActionSampler::SamplerType::Rotation:
            for (auto &[timestamp, sample] : sampler.second->RotationSamples->Current) {
                if (timestamp >= maxTime) { maxTime = timestamp; }
                if (timestamp <= minTime) { minTime = timestamp; }
            }
            break;
        case ktnActionSampler::SamplerType::Scale:
            for (auto &[timestamp, sample] : sampler.second->ScaleSamples->Current) {
                if (timestamp >= maxTime) { maxTime = timestamp; }
                if (timestamp <= minTime) { minTime = timestamp; }
            }
            break;
        case ktnActionSampler::SamplerType::Translation:
            for (auto &[timestamp, sample] : sampler.second->TranslationSamples->Current) {
                if (timestamp >= maxTime) { maxTime = timestamp; }
                if (timestamp <= minTime) { minTime = timestamp; }
            }
            break;
        default:
            throw runtime_error("Unknown action sampler type");
        }
    }

    auto result = CompareFloat(maxTime, minTime);
    switch (result) {
    case CompareFloatResult::Greater:
        m_Duration = make_unique<float>(maxTime - minTime);
        break;
    case CompareFloatResult::Equal:
        m_Duration = make_unique<float>(0.0F);
        break;
    case CompareFloatResult::Smaller:
        throw runtime_error("The animation duration cannot be calculated.");
    }
}

bool ktnAction::IsPlaying() const {
    auto currentTime = Utils::FrameTime();
    return (currentTime / 1000 - m_StartTime - Duration() * MS_IN_S < 0);
}

void ktnAction::Interpolate() {
    float currentSecondsSinceStart = (Utils::FrameTime() / 1000 - m_StartTime) / MS_IN_S;
    CalculateInfluence(currentSecondsSinceStart);

    for (auto &channel : Channels) {
        size_t index_previous_time = 0;
        switch (channel.Path) {
        case ChannelPath::Rotation: {
            auto &samples = channel.Sampler->RotationSamples->Current;
            if (!samples.empty() && currentSecondsSinceStart < samples.at(0).first) {
                index_previous_time = 0;
            } else {
                for (size_t i = 0; i < samples.size(); ++i) {
                    if (samples.at(i).first < currentSecondsSinceStart) {
                        index_previous_time = i;
                    } else {
                        break;
                    }
                }
            }
            quat rotation = DefaultRotation;
            pair<float, quat> sample1;
            pair<float, quat> sample2;

            if (index_previous_time == samples.size() - 1) {
                rotation = samples[samples.size() - 1].second;
            } else {
                switch (channel.Sampler->Interpolation) {
                case InterpolationAlgorithm::LINEAR:
                    // TODO: use spline function for CUBICSPLINE instead of lerp
                case InterpolationAlgorithm::CUBICSPLINE: {
                    sample1 = samples.at(index_previous_time);
                    sample2 = samples.at(index_previous_time + 1);
                    // prevent sudden jump in 4d space by flipping the quaternion when the jump is too large
                    if (length(sample2.second - sample1.second) > length(sample2.second + sample1.second)) { sample2.second = -sample2.second; }
                    float progress = (currentSecondsSinceStart - sample1.first + samples.at(0).first) / (sample2.first - sample1.first);
                    progress = std::clamp(progress, 0.0F, 1.0F);
                    rotation = lerp(sample1.second, sample2.second, progress);
                } break;
                case InterpolationAlgorithm::STEP: {
                    throw runtime_error("The step interpolation is not yet implemented.");
                }
                }
            }
            if (rotation != DefaultRotation) {
                if (channel.Target != nullptr) {
                    channel.Target->AnimatedInfluences[Name].Rotation = rotation; // OffsetAnimatedRotation(rotation);
                    channel.Target->AnimatedInfluences[Name].Influence = Influence;
                } else {
                    throw runtime_error("Animation channel target not found.");
                }
            }
        } break;
        case ChannelPath::Scale: {
            auto &samples = channel.Sampler->ScaleSamples->Current;
            if (!samples.empty() && currentSecondsSinceStart < samples.at(0).first) {
                index_previous_time = 0;
            } else {
                for (size_t i = 0; i < samples.size(); ++i) {
                    if (samples.at(i).first < currentSecondsSinceStart) {
                        index_previous_time = i;
                    } else {
                        break;
                    }
                }
            }
            vec3 scale = DefaultScale;
            pair<float, vec3> sample1;
            pair<float, vec3> sample2;
            if (index_previous_time == samples.size() - 1) {
                scale = samples.at(index_previous_time).second;
            } else {
                switch (channel.Sampler->Interpolation) {
                case InterpolationAlgorithm::LINEAR:
                    // TODO: use spline function for CUBICSPLINE instead of lerp
                case InterpolationAlgorithm::CUBICSPLINE: {
                    sample1 = samples.at(index_previous_time);
                    sample2 = samples.at(index_previous_time + 1);
                    float progress = (currentSecondsSinceStart - sample1.first + samples.at(0).first) / (sample2.first - sample1.first);
                    progress = std::clamp(progress, 0.0F, 1.0F);
                    scale = glm::lerp(sample1.second, sample2.second, progress);
                } break;
                case InterpolationAlgorithm::STEP: {
                    throw runtime_error("The step interpolation is not yet implemented.");
                }
                }
            }

            if (scale != DefaultScale) {
                if (channel.Target != nullptr) {
                    channel.Target->AnimatedInfluences[Name].Scale = scale;
                    channel.Target->AnimatedInfluences[Name].Influence = Influence;
                } else {
                    throw runtime_error("Animation channel target not found.");
                }
            }
        } break;
        case ChannelPath::Translation: {
            auto &samples = channel.Sampler->TranslationSamples->Current;
            if (!samples.empty() && currentSecondsSinceStart < samples.at(0).first) {
                index_previous_time = 0;
            } else {
                for (size_t i = 0; i < samples.size(); ++i) {
                    if (samples.at(i).first < currentSecondsSinceStart) {
                        index_previous_time = i;
                    } else {
                        break;
                    }
                }
            }
            vec3 translation = DefaultTranslation;
            pair<float, vec3> sample1;
            pair<float, vec3> sample2;

            if (index_previous_time == samples.size() - 1) {
                translation = samples.at(index_previous_time).second;
            } else {
                switch (channel.Sampler->Interpolation) {
                case InterpolationAlgorithm::LINEAR:
                    // TODO: use spline function for CUBICSPLINE instead of lerp
                case InterpolationAlgorithm::CUBICSPLINE: {
                    sample1 = samples.at(index_previous_time);
                    sample2 = samples.at(index_previous_time + 1);
                    float progress = (currentSecondsSinceStart - sample1.first + samples.at(0).first) / (sample2.first - sample1.first);
                    progress = std::clamp(progress, 0.0F, 1.0F);
                    translation = glm::lerp(sample1.second, sample2.second, progress);
                } break;
                case InterpolationAlgorithm::STEP: {
                    throw runtime_error("The step interpolation is not yet implemented.");
                }
                }
            }

            if (translation != DefaultTranslation) {
                if (channel.Target != nullptr) {
                    channel.Target->AnimatedInfluences[Name].Position = translation;
                    channel.Target->AnimatedInfluences[Name].Influence = Influence;
                } else {
                    throw runtime_error("Animation channel target not found.");
                }
            }
        } break;
        case ChannelPath::Weights: {
            throw runtime_error("Not yet implemented");
        }
        }
    }
}

void ktnAction::LoadFromGLTF(const gleb::glTF &eGltf, size_t eIndex) {
    auto gltfAnimation = eGltf.animations.at(eIndex);
    if (gltfAnimation.name.has_value()) { Name = gltfAnimation.name.value(); }
    for (auto &channel : gltfAnimation.channels) {
        ktnAction::Channel c;

        // set channel path
        if (channel.target.path == "rotation") {
            c.Path = ChannelPath::Rotation;
        } else if (channel.target.path == "scale") {
            c.Path = ChannelPath::Scale;
        } else if (channel.target.path == "translation") {
            c.Path = ChannelPath::Translation;
        } else if (channel.target.path == "weights") {
            c.Path = ChannelPath::Weights;
        }

        // load the associated sampler
        auto sampler = gltfAnimation.samplers.at(channel.sampler);
        auto s = make_shared<ktnActionSampler>();

        // assign interpolation algorithm
        // it's LINEAR by default so that value doesn't need to be checked
        if (sampler.interpolation == "STEP") {
            s->Interpolation = InterpolationAlgorithm::STEP;
        } else if (sampler.interpolation == "CUBICSPLINE") {
            s->Interpolation = InterpolationAlgorithm::CUBICSPLINE;
        }

        // add the sample times
        auto accessor_input = eGltf.accessors.at(sampler.input);
        auto bufferview_input = eGltf.bufferViews.at(accessor_input.bufferView.value());
        auto buffer_input = eGltf.buffers.at(bufferview_input.buffer);
        s->LoadSampleTimes(accessor_input.count, bufferview_input, buffer_input);

        // add the sampled values based on the channel's path
        auto accessor_output = eGltf.accessors.at(sampler.output);
        auto bufferview_output = eGltf.bufferViews.at(accessor_output.bufferView.value());
        auto buffer_output = eGltf.buffers.at(bufferview_output.buffer);
        switch (c.Path) {
        case ChannelPath::Rotation: {
            s->LoadRotationSamples(accessor_output.count, bufferview_output, buffer_output);
        } break;

        case ChannelPath::Scale: {
            s->LoadScaleSamples(accessor_output.count, accessor_output.type, bufferview_output, buffer_output);
        } break;

        case ChannelPath::Translation: {
            s->LoadTranslationSamples(accessor_output.count, bufferview_output, buffer_output);
        } break;

        default:
            throw runtime_error(
                "The loading function for channel path " //
                + to_string(static_cast<unsigned int>(c.Path)) //
                + " is not yet implemented.");
        }

        c.Sampler = s;
        Channels.emplace_back(c);
        Samplers[channel.sampler] = s;
    }

    for (auto &[index, sampler] : Samplers) { sampler->Verify(); }
    // cout << "Action " << Name << ": " << Samplers.size() << " samplers passed integrity check." << endl;
    CalculateDuration();
}

void ktnAction::Start() {
    Completed = false;
    m_StartTime = Utils::FrameTime() / 1000;
    m_StopTime = 0;
}

void ktnAction::Stop() {
    Stop(Utils::FrameTime());
}

void ktnAction::CalculateInfluence(float currentSecondsSinceStart) {
    if (m_StopTime == 0 && currentSecondsSinceStart < Duration()) {
        if (currentSecondsSinceStart < m_TransitionDuration) {
            // starting transition
            Influence += float(Utils::FrameDuration()) / m_TransitionDuration / MS_IN_S;
            Influence = std::clamp(Influence, 0.F, 1.F);
        }
    }

    if (currentSecondsSinceStart >= Duration()) {
        if (Loop) { Start(); }
    }

    if (m_StopTime != 0) {
        // stopping transition
        Influence -= float(Utils::FrameDuration()) / m_TransitionDuration / MS_IN_S;
        Influence = std::clamp(Influence, 0.F, 1.F);
        Completed = true;

        // If influence == 0 and not looping, stop.
        // This line compress this 2 ifs so that it can run branchlessly.
        Stop(!Influence * !Loop * Utils::FrameTime() / 1000);
    }
}

void ktnAction::Stop(long currentTimeInMsSinceEpoch) {
    m_StopTime = currentTimeInMsSinceEpoch;
}
} // namespace ktn::Graphics
