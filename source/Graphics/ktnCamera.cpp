#include <ktnEngine/Graphics/ktnCamera.hpp>

#include <glm/gtc/matrix_transform.hpp>

using glm::mat4;
using glm::vec3;
using std::string;

namespace ktn::Graphics {
ktnCamera::ktnCamera(const vec3 &initialPosition, const Math::ktnEulerAngles &initialEulerAngles) : ktn3DElement("ktnCamera") {
    SetPosition(initialPosition, WorldSpace);
    EulerAngles = initialEulerAngles;
}

ktnCamera::~ktnCamera() = default;

auto ktnCamera::operator==(const ktnCamera &eCamera) const -> bool {
    return ktn3DElement::operator==(eCamera) //
           && m_ViewMatrix == eCamera.m_ViewMatrix //
           && m_Mode == eCamera.m_Mode //
           && m_Type == eCamera.m_Type;
}

auto ktnCamera::operator!=(const ktnCamera &eCamera) const -> bool {
    return !operator==(eCamera);
}

auto ktnCamera::operator=(const ktnCamera &eCamera) -> ktnCamera & {
    ktn3DElement::operator=(eCamera);

    EulerAngles = eCamera.EulerAngles;
    m_Direction = eCamera.Direction();
    m_Front = eCamera.Front();
    m_Mode = eCamera.Mode();
    m_Right = eCamera.Right();
    m_Sensitivity = eCamera.Sensitivity();
    m_Target = eCamera.Target();
    m_Top = eCamera.Top();
    m_Type = eCamera.Type();
    m_UniversalUp = eCamera.UniversalUp();
    m_ViewMatrix = eCamera.ViewMatrix();
    return *this;
}

void ktnCamera::SetMode(const CameraMode &Mode) {
    m_Mode = Mode;
}

void ktnCamera::SetType(const CameraType &Type) {
    m_Type = Type;
}

void ktnCamera::SetTarget(const vec3 &target) {
    m_Target = target;
    if (CameraMode::TRACKING == m_Mode) {
        UpdateRightAndTop();
        CONSIDER
        /* should this be in UpdateFront for the sake of consistency
         * or here for less overhead? */
        m_Front = glm::normalize(glm::cross(m_Top, m_Right));
    }
}

void ktnCamera::SetTarget(const float &x, const float &y, const float &z) {
    m_Target.x = x;
    m_Target.y = y;
    m_Target.z = z;
    if (CameraMode::TRACKING == m_Mode) {
        UpdateRightAndTop();
        CONSIDER
        /* should this be in UpdateFront for the sake of consistency
         * or here for less overhead? */
        m_Front = glm::normalize(glm::cross(m_Top, m_Right));
    }
}

void ktnCamera::SetDirection(const vec3 &dir) {
    m_Direction = dir;
}

void ktnCamera::SetDirection(const float &x, const float &y, const float &z) {
    m_Direction.x = x;
    m_Direction.y = y;
    m_Direction.z = z;
}

void ktnCamera::SetFront(const vec3 &front) {
    m_Front = front;
}

void ktnCamera::SetFront(const float &x, const float &y, const float &z) {
    m_Front.x = x;
    m_Front.y = y;
    m_Front.z = z;
}

void ktnCamera::SetUniversalUp(const vec3 &up) {
    m_UniversalUp = up;
}

void ktnCamera::MoveForward(const float &thismuch) {
    OffsetPosition(thismuch * m_Front, WorldSpace);
}

void ktnCamera::MoveBackward(const float &thismuch) {
    OffsetPosition(-thismuch * m_Front, WorldSpace);
}

void ktnCamera::MoveLeft(const float &thismuch) {
    OffsetPosition(-thismuch * m_Right, WorldSpace);
}

void ktnCamera::MoveRight(const float &thismuch) {
    OffsetPosition(thismuch * m_Right, WorldSpace);
}

void ktnCamera::RegulatePitch() {
    const float MAX_PITCH(89.0F);
    if (EulerAngles.Pitch > MAX_PITCH) { EulerAngles.Pitch = MAX_PITCH; }
    if (EulerAngles.Pitch < -MAX_PITCH) { EulerAngles.Pitch = -MAX_PITCH; }
}

void ktnCamera::TurnUp(const float &thismuch) {
    EulerAngles.Pitch += thismuch * m_Sensitivity;
    RegulatePitch();
}

void ktnCamera::TurnDown(const float &thismuch) {
    EulerAngles.Pitch -= thismuch * m_Sensitivity;
    RegulatePitch();
}

void ktnCamera::TurnLeft(const float &thismuch) {
    EulerAngles.Yaw -= thismuch * m_Sensitivity;
}

void ktnCamera::TurnRight(const float &thismuch) {
    EulerAngles.Yaw += thismuch * m_Sensitivity;
}

void ktnCamera::UpdateFront() {
    if (CameraMode::FREELOOK == m_Mode && CameraType::FIRSTPERSON == m_Type) {
        m_Front.x = cosf(Math::DegreeToRadian(EulerAngles.Pitch)) * cosf(Math::DegreeToRadian(EulerAngles.Yaw));
        m_Front.y = sinf(Math::DegreeToRadian(EulerAngles.Pitch));
        m_Front.z = cosf(Math::DegreeToRadian(EulerAngles.Pitch)) * sinf(Math::DegreeToRadian(EulerAngles.Yaw));
        m_Front = glm::normalize(m_Front);
        UpdateRightAndTop();
    }
}

void ktnCamera::UpdateRightAndTop() {
    if (CameraMode::FREELOOK == m_Mode && CameraType::FIRSTPERSON == m_Type) {
        m_Right = glm::normalize(glm::cross(m_Front, m_UniversalUp));
        m_Top = glm::normalize(glm::cross(m_Right, m_Front));
    } else if (CameraMode::TRACKING == m_Mode) {
        m_Direction = glm::normalize(Position(WorldSpace) - m_Target);
        m_Right = glm::normalize(glm::cross(m_UniversalUp, m_Direction));
        m_Top = glm::cross(m_Direction, m_Right);
    }
}

void ktnCamera::UpdateViewMatrix() {
    m_ViewMatrix = glm::lookAt(Position(WorldSpace), Position(WorldSpace) + m_Front, m_Top);
}

auto ktnCamera::Mode() const -> CameraMode {
    return m_Mode;
}

auto ktnCamera::Type() const -> CameraType {
    return m_Type;
}

auto ktnCamera::UniversalUp() const -> vec3 {
    return m_UniversalUp;
}

auto ktnCamera::Target() const -> vec3 {
    return m_Target;
}

auto ktnCamera::Direction() const -> vec3 {
    return m_Direction;
}

auto ktnCamera::Right() const -> vec3 {
    return m_Right;
}

auto ktnCamera::Top() const -> vec3 {
    return m_Top;
}

auto ktnCamera::Front() const -> vec3 {
    return m_Front;
}

auto ktnCamera::ViewMatrix() const -> mat4 {
    return m_ViewMatrix;
}

auto ktnCamera::Sensitivity() const -> float {
    return m_Sensitivity;
}
} // namespace ktn::Graphics
