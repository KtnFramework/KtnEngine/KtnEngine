#!/bin/bash

echo "Formatting shaders..."
declare -a UNFORMATTED_FILES

for filename in $(find -name '*.glsl'); do
    # compare the original file and the formatted file, then check the output to see if there are changes
    LINES_CHANGED=$(diff -u <(cat $filename) <(clang-format $filename) | grep -c ^)
    
    # add name of files that need formatting to the UNFORMATTED_FILES array
    if [ $LINES_CHANGED -ne 0 ]; then
        UNFORMATTED_FILES+=("$filename")
    fi
done

if [ ${#UNFORMATTED_FILES[@]} -eq 0 ]; then
    echo "No formatting needed."
else
    echo "Files that need formatting: "
    for filename in ${UNFORMATTED_FILES[@]}; do
        echo "$filename"
    done
    echo
    echo "Formatting files:"
    for filename in ${UNFORMATTED_FILES[@]}; do
        clang-format -i $filename && echo "[   OK   ] $filename" || {
            echo "[ FAILED ] $filename"
            declare AUTOFORMATTING_FAILED=1
        }
    done
fi
echo
if [ ! -z ${AUTOFORMATTING_FAILED+x} ]; then
    exit 1
fi



# Arguments:
# $1: source file
# $2: string name for file content
embed_source() {
    echo >> DefaultShaders.hpp
    echo "const inline static std::string $2(R\"(" >> DefaultShaders.hpp
    cat "$1" >> DefaultShaders.hpp || {
        exit 1;
    }
    echo ")\");" >> DefaultShaders.hpp
}



cd OpenGL
echo "Generating default OpenGL shader header..."
rm DefaultShaders.hpp

echo "#ifndef KTNENGINE_GRAPHICS_INTERNAL_OPENGL_DEFAULTSHADERS_HPP" >> DefaultShaders.hpp
echo "#define KTNENGINE_GRAPHICS_INTERNAL_OPENGL_DEFAULTSHADERS_HPP" >> DefaultShaders.hpp
echo "#include <string>" >> DefaultShaders.hpp
echo "namespace ktn::Graphics::OpenGL {" >> DefaultShaders.hpp

embed_source "bloom.frag.glsl" DEFAULT_SHADER_BLOOM_FRAG

embed_source "blur.frag.glsl" DEFAULT_SHADER_BLUR_FRAG

embed_source "default.frag.glsl" DEFAULT_SHADER_DEFAULT_FRAG
embed_source "default.vert.glsl" DEFAULT_SHADER_DEFAULT_VERT

embed_source "deferred_light.frag.glsl" DEFAULT_SHADER_DEFERRED_LIGHT_FRAG

embed_source "full_screen_rectangle.vert.glsl" DEFAULT_SHADER_FULL_SCREEN_RECTANGLE_VERT

embed_source "g_buffer.frag.glsl" DEFAULT_SHADER_G_BUFFER_FRAG
embed_source "g_buffer.vert.glsl" DEFAULT_SHADER_G_BUFFER_VERT

embed_source "screen.frag.glsl" DEFAULT_SHADER_SCREEN_FRAG

embed_source "shadow.frag.glsl" DEFAULT_SHADER_SHADOW_FRAG
embed_source "shadow.vert.glsl" DEFAULT_SHADER_SHADOW_VERT

echo "} // namespace ktn::Graphics::OpenGL" >> DefaultShaders.hpp
echo "#endif // KTNENGINE_GRAPHICS_INTERNAL_OPENGL_DEFAULTSHADERS_HPP" >> DefaultShaders.hpp
echo "Done."
cd ..



cd Vulkan
echo "Generating default Vulkan shader header..."
rm DefaultShaders.hpp

echo "#ifndef KTNENGINE_GRAPHICS_INTERNAL_VULKAN_DEFAULTSHADERS_HPP" >> DefaultShaders.hpp
echo "#define KTNENGINE_GRAPHICS_INTERNAL_VULKAN_DEFAULTSHADERS_HPP" >> DefaultShaders.hpp
echo "#include <string>" >> DefaultShaders.hpp
echo "namespace ktn::Graphics::Vulkan {" >> DefaultShaders.hpp

embed_source "default.frag" DEFAULT_SHADER_DEFAULT_FRAG
embed_source "default.vert" DEFAULT_SHADER_DEFAULT_VERT

echo "} // namespace ktn::Graphics::Vulkan" >> DefaultShaders.hpp
echo "#endif // KTNENGINE_GRAPHICS_INTERNAL_VULKAN_DEFAULTSHADERS_HPP" >> DefaultShaders.hpp
echo "Done."
cd ..
