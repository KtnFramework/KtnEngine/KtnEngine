#version 330 core
in vec2 TexCoords;

out vec4 FragColor;

uniform sampler2D gBuffer_Position;
uniform sampler2D gBuffer_Normal;
uniform sampler2D gBuffer_Diffuse;
uniform sampler2D gBuffer_TexCoords;

struct ktnPointLightData {
    bool Enabled;
    vec3 WorldSpacePosition;
    float Intensity;
    vec3 Color;
    float FalloffLinear;
    float FalloffQuadratic;
    float InfluenceRadius;
};

uniform int NumberOfLights;

uniform ktnPointLightData lights[100];

void main() {
    vec3 position = texture(gBuffer_Position, TexCoords).xyz;
    vec3 normal = normalize(texture(gBuffer_Normal, TexCoords).xyz);
    vec3 diffuse = texture(gBuffer_Diffuse, TexCoords).xyz;
    vec3 result = vec3(0, 0, 0);

    for (int i = 0; i < NumberOfLights; ++i) {
        if (lights[i].Enabled) {
            vec3 direction_fragment_to_light = normalize(lights[i].WorldSpacePosition - position);
            float distance = length(lights[i].WorldSpacePosition - position);
            float attenuation = clamp(lights[i].Intensity / (1. + lights[i].FalloffLinear * distance + lights[i].FalloffQuadratic * distance * distance), 0, 1);

            float diffuse_brightness = attenuation * max(dot(normal, direction_fragment_to_light), 0.);
            result += lights[i].Color * diffuse_brightness * diffuse;
        }
    }

    FragColor = vec4(result, 1.);
}
