#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D screenTexture;

void main() {
    // vec3 result = vec3(1.0 - texture(screenTexture, TexCoords));
    // for rgb
    vec3 result = vec3(texture(screenTexture, TexCoords));
    // for depth
    // float depthvalue = texture(screenTexture, TexCoords).r;
    // for perspective transformed z
    // float depthvalue = LinearizeDepth(texture(screenTexture, TexCoords).r);
    // vec3 result = vec3(depthvalue);
    FragColor = vec4(result, 1.0);
}
