#version 330 core
layout(location = 0) out vec3 gBuffer_Position;
layout(location = 1) out vec3 gBuffer_Normal;
layout(location = 2) out vec4 gBuffer_Diffuse;
layout(location = 3) out vec2 gBuffer_TexCoords;

in vec3 position;
in vec3 normal;
in vec2 texCoords;

struct Material {
    vec4 baseColorFactor;

    bool baseColorTextureIsSet;
    sampler2D baseColorTexture;

    sampler2D specular;
    float shininess;
};

uniform float pseudorandomnumber;
uniform vec3 viewpos;

uniform Material material;

void main() {
    gBuffer_Position = position;
    gBuffer_Normal = normal;
    if (material.baseColorTextureIsSet) {
        gBuffer_Diffuse = material.baseColorFactor * texture(material.baseColorTexture, texCoords);
    } else {
        gBuffer_Diffuse = material.baseColorFactor;
    }
    gBuffer_TexCoords = texCoords;
}
