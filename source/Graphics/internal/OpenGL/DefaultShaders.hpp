#ifndef KTNENGINE_GRAPHICS_INTERNAL_OPENGL_DEFAULTSHADERS_HPP
#define KTNENGINE_GRAPHICS_INTERNAL_OPENGL_DEFAULTSHADERS_HPP
#include <string>
namespace ktn::Graphics::OpenGL {

const inline static std::string DEFAULT_SHADER_BLOOM_FRAG(R"(
#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D inputImage;
uniform sampler2D blurredImage;
uniform sampler2D heavilyBlurredImage;
uniform float exposure;
uniform float bigBlurIntensity;
uniform float smallBlurIntensity;

void main() {
    const float gamma = 2.2;
    vec3 hdrColor = texture(inputImage, TexCoords).rgb;
    // vec3 bloomColor = vec3(0.0);
    vec3 blurColor = texture(blurredImage, TexCoords).rgb;
    vec3 heavyBlurColor = texture(heavilyBlurredImage, TexCoords).rgb;
    hdrColor += smallBlurIntensity * blurColor + bigBlurIntensity * heavyBlurColor; // additive blending
    // tone mapping
    // vec3 result = vec3(1.0) - exp(-hdrColor * exposure);
    // also gamma correct while we're at it
    // result = pow(result, vec3(1.0 / gamma));
    FragColor = vec4(hdrColor, 1.0);
}
)");

const inline static std::string DEFAULT_SHADER_BLUR_FRAG(R"(
#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D inputImage;

uniform bool orientation;
bool horizontal = true;
int range = 5;
float scale = 1.0;
void main() {
    vec2 tex_offset = scale / textureSize(inputImage, 0); // gets size of single texel
    vec3 result = vec3(0.0);
    if (orientation == horizontal) {
        for (int i = -range; i <= range; ++i) { result += texture(inputImage, TexCoords + vec2(tex_offset.x * i, 0.0)).rgb; }
    } else {
        for (int i = -range; i <= range; ++i) { result += texture(inputImage, TexCoords + vec2(0.0, tex_offset.y * i)).rgb; }
    }
    result /= range * 2 + 1;
    FragColor = vec4(result, 1.0);
}
)");

const inline static std::string DEFAULT_SHADER_DEFAULT_FRAG(R"(
#version 330 core
layout(location = 0) out vec4 FragColor;
layout(location = 1) out vec4 BrightColor;

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;
in vec4 FragPosInLightSpace;

// for the z buffer
float near = .1;
float far = 100.;

struct Material {
    vec4 baseColorFactor;

    bool baseColorTextureIsSet;
    sampler2D baseColorTexture;

    sampler2D specular;
    float shininess;
};

struct Cutoff {
    float inner;
    float outer;
};

struct Falloff {
    float linearterm;
    float quadraticterm;
};

struct Light {
    int type;
    float intensity;
    vec3 position;
    vec3 direction;
    vec3 color;

    // for falloff
    Falloff falloff;

    // for lighting cutoff
    Cutoff cutoff;
};

uniform float pseudorandomnumber;
uniform vec3 viewpos;

uniform Material material;
uniform Light light;

uniform sampler2D shadowmap;

float PCFShadow(vec4 FragPosInLightSpace) {
    float shadow = 0.;
    float bias = .001;
    // perform perspective divide
    vec3 projCoords = FragPosInLightSpace.xyz / FragPosInLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * .5 + .5;

    // soften shadow using Percentage closer filtering
    float blurScaleFactor = 1.;
    float currentDepth = projCoords.z;
    for (int i = -1; i <= 1; ++i) {
        for (int j = -1; j <= 1; ++j) {
            float pcf = texture(shadowmap, projCoords.xy + vec2(i, j) * blurScaleFactor / textureSize(shadowmap, 0)).r;
            float shadowdistance = currentDepth - pcf;
            shadow += (currentDepth - bias > pcf && currentDepth < 1.) ? 1. : 0.;
        }
    }
    shadow /= 9.;
    // if(shadow < 0.5) shadow = 0;
    // use hard shadow and soften in screenspace
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    // float closestDepth = texture(shadowmap, projCoords.xy).r;
    // get depth of current fragment from light's perspective
    // float currentDepth = projCoords.z;
    // check whether current frag pos is in shadow
    // shadow = (currentDepth - bias > closestDepth && currentDepth < 1.0) ? 1.0 : 0.0;

    return shadow;
}

float ChebyshevUpperBound(vec2 moments, float distanceToLight) {
    // inequality valid if distanceToLight > moments.x;
    float p = 0.;
    if (distanceToLight <= moments.x) { p = 1.; }

    // compute variance
    float variance = moments.y - moments.x * moments.x;
    float minVariance = .0001;
    variance = max(variance, minVariance);
    // compute probabilistic upper bound
    float d = distanceToLight - moments.x;
    float p_max = variance / (variance + d * d);
    return max(p, p_max);
}

float VarianceShadowMap(vec4 FragPosInLightSpace) {
    // perform perspective divide
    vec3 projCoords = FragPosInLightSpace.xyz / FragPosInLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * .5 + .5;
    float currentDepth = projCoords.z;

    vec2 moments = vec2(0., 0.);
    for (int i = -2; i <= 2; ++i) {
        for (int j = -2; j <= 2; ++j) {
            float temp = texture(shadowmap, projCoords.xy + vec2(i, j) / textureSize(shadowmap, 0)).r;
            moments.x += temp / 25;
            moments.y += temp * temp / 25;
        }
    }
    // moments.x = texture(shadowmap, projCoords.xy).r;
    float dx = dFdx(moments.x);
    float dy = dFdy(moments.x);
    // moments.y = moments.x * moments.x + 0.25*(dx*dx+dy*dy);
    float shadow = ChebyshevUpperBound(moments, currentDepth);
    return 1. - shadow;
}

vec3 screen(vec3 a, vec3 b) {
    return vec3(1. - (1. - a) * (1. - b));
}

void main() {
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(light.position - FragPos);

    // light falloff
    float distance = length(light.position - FragPos);
    float attenuation = 1. / (1. + light.falloff.linearterm * distance + light.falloff.quadraticterm * distance * distance);

    // ambient
    vec3 ambient = light.color * 0.05 * vec3(texture(material.baseColorTexture, TexCoords));

    // diffuse
    float diff = max(dot(norm, lightDir), 0.);
    vec3 diffuse = light.color * diff * vec3(texture(material.baseColorTexture, TexCoords));

    // specular shader
    vec3 viewDir = normalize(viewpos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.), material.shininess);
    vec3 specular = light.color * spec * vec3(texture(material.specular, TexCoords));

    // z-based fog
    // float z = gl_FragCoord.z * 2.0 - 1.0;
    // z = 2.0 * near * far / (far + near - z * (far - near));
    // z = 0.0;
    vec3 result;
    // light cutoff
    float theta = dot(lightDir, normalize(-light.direction));
    float epsilon = light.cutoff.inner - light.cutoff.outer;
    float intensity = clamp((theta - light.cutoff.outer) / epsilon, 0., 1.);
    diffuse *= intensity;
    specular *= intensity;

    // pcf shadow
    float shadow = PCFShadow(FragPosInLightSpace);

    // variance shadow map
    // float shadow = VarianceShadowMap(FragPosInLightSpace);

    // render normal
    // result = vec3(cross(Normal, viewDir));

    // test blank texture
    //     result = diffuse;

    // render texcoords
    //     result = vec3(TexCoords[0], TexCoords[1], 1);

    // render with shadow
    result = ambient + light.intensity * clamp((1.0 - shadow) * (diffuse + specular), 0, 1) * attenuation;

    // render shadow only
    // result = vec3(1.0 - shadow);

    // add z fog
    // result = screen(result, vec3(z/100.0));

    // gamma correction
    //     vec3 gamma = vec3 (1.0/2.2);
    //     result = pow(result, gamma);

    // dithering noise
    vec3 noise = vec3(sin(pseudorandomnumber * gl_FragCoord.x * gl_FragCoord.y)) / 255;
    result = result + noise;

    FragColor = vec4(result, 1.);
    BrightColor = clamp(pow(FragColor, vec4(8.)), 0., 1.);
}
)");

const inline static std::string DEFAULT_SHADER_DEFAULT_VERT(R"(
#version 330 core
layout(location = 0) in vec3 aPos;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec4 aTangent;
layout(location = 3) in vec2 aTexCoords;

out vec3 FragPos;
out vec3 Normal;
out vec2 TexCoords;
out vec4 FragPosInLightSpace;

uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 LightSpaceMatrix;

void main() {
    gl_Position = ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(aPos, 1.0);
    FragPos = vec3(ModelMatrix * vec4(aPos, 1.0));
    Normal = mat3(transpose(inverse(ModelMatrix))) * aNormal;
    TexCoords = aTexCoords;
    FragPosInLightSpace = LightSpaceMatrix * vec4(FragPos, 1.0);
}
)");

const inline static std::string DEFAULT_SHADER_DEFERRED_LIGHT_FRAG(R"(
#version 330 core
in vec2 TexCoords;

out vec4 FragColor;

uniform sampler2D gBuffer_Position;
uniform sampler2D gBuffer_Normal;
uniform sampler2D gBuffer_Diffuse;
uniform sampler2D gBuffer_TexCoords;

struct ktnPointLightData {
    bool Enabled;
    vec3 WorldSpacePosition;
    float Intensity;
    vec3 Color;
    float FalloffLinear;
    float FalloffQuadratic;
    float InfluenceRadius;
};

uniform int NumberOfLights;

uniform ktnPointLightData lights[100];

void main() {
    vec3 position = texture(gBuffer_Position, TexCoords).xyz;
    vec3 normal = normalize(texture(gBuffer_Normal, TexCoords).xyz);
    vec3 diffuse = texture(gBuffer_Diffuse, TexCoords).xyz;
    vec3 result = vec3(0, 0, 0);

    for (int i = 0; i < NumberOfLights; ++i) {
        if (lights[i].Enabled) {
            vec3 direction_fragment_to_light = normalize(lights[i].WorldSpacePosition - position);
            float distance = length(lights[i].WorldSpacePosition - position);
            float attenuation = clamp(lights[i].Intensity / (1. + lights[i].FalloffLinear * distance + lights[i].FalloffQuadratic * distance * distance), 0, 1);

            float diffuse_brightness = attenuation * max(dot(normal, direction_fragment_to_light), 0.);
            result += lights[i].Color * diffuse_brightness * diffuse;
        }
    }

    FragColor = vec4(result, 1.);
}
)");

const inline static std::string DEFAULT_SHADER_FULL_SCREEN_RECTANGLE_VERT(R"(
#version 330 core
layout(location = 0) in vec3 aPos;
layout(location = 1) in vec2 aTexCoords;

out vec2 TexCoords;

void main() {
    TexCoords = aTexCoords;
    gl_Position = vec4(aPos, 1.0);
}
)");

const inline static std::string DEFAULT_SHADER_G_BUFFER_FRAG(R"(
#version 330 core
layout(location = 0) out vec3 gBuffer_Position;
layout(location = 1) out vec3 gBuffer_Normal;
layout(location = 2) out vec4 gBuffer_Diffuse;
layout(location = 3) out vec2 gBuffer_TexCoords;

in vec3 position;
in vec3 normal;
in vec2 texCoords;

struct Material {
    vec4 baseColorFactor;

    bool baseColorTextureIsSet;
    sampler2D baseColorTexture;

    sampler2D specular;
    float shininess;
};

uniform float pseudorandomnumber;
uniform vec3 viewpos;

uniform Material material;

void main() {
    gBuffer_Position = position;
    gBuffer_Normal = normal;
    if (material.baseColorTextureIsSet) {
        gBuffer_Diffuse = material.baseColorFactor * texture(material.baseColorTexture, texCoords);
    } else {
        gBuffer_Diffuse = material.baseColorFactor;
    }
    gBuffer_TexCoords = texCoords;
}
)");

const inline static std::string DEFAULT_SHADER_G_BUFFER_VERT(R"(
#version 330 core
layout(location = 0) in vec3 aPos;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec4 aTangent;
layout(location = 3) in vec2 aTexCoords;
layout(location = 4) in uvec4 aJoints0;
layout(location = 5) in vec4 aWeights0;

out vec3 position;
out vec3 normal;
out vec2 texCoords;

const int Maximum_NumberOfJoints = 128;
uniform mat4[Maximum_NumberOfJoints] JointMatrices;

uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

void main() {
    mat4 skinMatrix;
    if (0 != length(aWeights0)) {
        skinMatrix = aWeights0.x * JointMatrices[aJoints0.x] //
                     + aWeights0.y * JointMatrices[aJoints0.y] //
                     + aWeights0.z * JointMatrices[aJoints0.z] //
                     + aWeights0.w * JointMatrices[aJoints0.w];
    } else {
        skinMatrix = mat4(1.0);
    }
    vec4 temp = ModelMatrix * skinMatrix * vec4(aPos, 1.0);
    position = vec3(temp);

    gl_Position = ProjectionMatrix * ViewMatrix * ModelMatrix * skinMatrix * vec4(aPos, 1.0);

    // See explanation here https://paroj.github.io/gltut/Illumination/Tut09%20Normal%20Transformation.html
    normal = mat3(transpose(inverse(ModelMatrix * skinMatrix))) * aNormal;
    texCoords = aTexCoords;
}
)");

const inline static std::string DEFAULT_SHADER_SCREEN_FRAG(R"(
#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D screenTexture;

void main() {
    // vec3 result = vec3(1.0 - texture(screenTexture, TexCoords));
    // for rgb
    vec3 result = vec3(texture(screenTexture, TexCoords));
    // for depth
    // float depthvalue = texture(screenTexture, TexCoords).r;
    // for perspective transformed z
    // float depthvalue = LinearizeDepth(texture(screenTexture, TexCoords).r);
    // vec3 result = vec3(depthvalue);
    FragColor = vec4(result, 1.0);
}
)");

const inline static std::string DEFAULT_SHADER_SHADOW_FRAG(R"(
#version 330 core

void main() {
    // gl_FragDepth = gl_FragCoord.z;
}
)");

const inline static std::string DEFAULT_SHADER_SHADOW_VERT(R"(
#version 330 core
layout(location = 0) in vec3 aPos;

uniform mat4 LightSpaceMatrix;
uniform mat4 ModelMatrix;

void main() {
    gl_Position = LightSpaceMatrix * ModelMatrix * vec4(aPos, 1.0);
}
)");
} // namespace ktn::Graphics::OpenGL
#endif // KTNENGINE_GRAPHICS_INTERNAL_OPENGL_DEFAULTSHADERS_HPP
