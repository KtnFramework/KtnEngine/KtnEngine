#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D inputImage;

uniform bool orientation;
bool horizontal = true;
int range = 5;
float scale = 1.0;
void main() {
    vec2 tex_offset = scale / textureSize(inputImage, 0); // gets size of single texel
    vec3 result = vec3(0.0);
    if (orientation == horizontal) {
        for (int i = -range; i <= range; ++i) { result += texture(inputImage, TexCoords + vec2(tex_offset.x * i, 0.0)).rgb; }
    } else {
        for (int i = -range; i <= range; ++i) { result += texture(inputImage, TexCoords + vec2(0.0, tex_offset.y * i)).rgb; }
    }
    result /= range * 2 + 1;
    FragColor = vec4(result, 1.0);
}
