#version 330 core
layout(location = 0) in vec3 aPos;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec4 aTangent;
layout(location = 3) in vec2 aTexCoords;
layout(location = 4) in uvec4 aJoints0;
layout(location = 5) in vec4 aWeights0;

out vec3 position;
out vec3 normal;
out vec2 texCoords;

const int Maximum_NumberOfJoints = 128;
uniform mat4[Maximum_NumberOfJoints] JointMatrices;

uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

void main() {
    mat4 skinMatrix;
    if (0 != length(aWeights0)) {
        skinMatrix = aWeights0.x * JointMatrices[aJoints0.x] //
                     + aWeights0.y * JointMatrices[aJoints0.y] //
                     + aWeights0.z * JointMatrices[aJoints0.z] //
                     + aWeights0.w * JointMatrices[aJoints0.w];
    } else {
        skinMatrix = mat4(1.0);
    }
    vec4 temp = ModelMatrix * skinMatrix * vec4(aPos, 1.0);
    position = vec3(temp);

    gl_Position = ProjectionMatrix * ViewMatrix * ModelMatrix * skinMatrix * vec4(aPos, 1.0);

    // See explanation here https://paroj.github.io/gltut/Illumination/Tut09%20Normal%20Transformation.html
    normal = mat3(transpose(inverse(ModelMatrix * skinMatrix))) * aNormal;
    texCoords = aTexCoords;
}
