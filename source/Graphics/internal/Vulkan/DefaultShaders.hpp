#ifndef KTNENGINE_GRAPHICS_INTERNAL_VULKAN_DEFAULTSHADERS_HPP
#define KTNENGINE_GRAPHICS_INTERNAL_VULKAN_DEFAULTSHADERS_HPP
#include <string>
namespace ktn::Graphics::Vulkan {

const inline static std::string DEFAULT_SHADER_DEFAULT_FRAG(R"(
#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 outColor;
layout(location = 0) in vec3 fragColor;

void main() {
    outColor = vec4(fragColor, 1.0);
}
)");

const inline static std::string DEFAULT_SHADER_DEFAULT_VERT(R"(
#version 450
#extension GL_ARB_separate_shader_objects : enable

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0) out vec3 fragColor;

vec2 positions[3] = vec2[3]( //
    vec2(0.0, -0.5),
    vec2(0.5, 0.5),
    vec2(-0.5, 0.5));

vec3 colors[3] = vec3[3]( //
    vec3(1.0, 0.0, 0.0),
    vec3(0.0, 1.0, 0.0),
    vec3(0.0, 0.0, 1.0));

void main() {
    gl_Position = vec4(positions[gl_VertexIndex], 0.0, 1.0);
    fragColor = colors[gl_VertexIndex];
}
)");
} // namespace ktn::Graphics::Vulkan
#endif // KTNENGINE_GRAPHICS_INTERNAL_VULKAN_DEFAULTSHADERS_HPP
