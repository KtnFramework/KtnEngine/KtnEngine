#include <ktnEngine/Graphics/ktnMaterial.hpp>

using namespace std;

namespace ktn::Graphics {
ktnMaterial::pbrMetallicRoughnessInfo::baseColorTexture_t::baseColorTexture_t(const std::string &eUri, size_t eTexCoords)
    : Texture(make_unique<ktnTexture>(eUri)) {
    Texture->CreateOnGPU(TextureType::DIFFUSE);
    TextureCoordinates = eTexCoords;
}

ktnMaterial::pbrMetallicRoughnessInfo::baseColorTexture_t::~baseColorTexture_t() {}

ktnMaterial::pbrMetallicRoughnessInfo::pbrMetallicRoughnessInfo() = default;

ktnMaterial::pbrMetallicRoughnessInfo::pbrMetallicRoughnessInfo(ktnMaterial::pbrMetallicRoughnessInfo &&other) {
    baseColorFactor = other.baseColorFactor;
    other.baseColorFactor = Defaults::BaseColorFactor;

    baseColorTexture = other.baseColorTexture;

    metallicFactor = other.metallicFactor;
    other.metallicFactor = Defaults::MetallicFactor;

    roughnessFactor = other.roughnessFactor;
    other.roughnessFactor = Defaults::RoughnessFactor;
}

ktnMaterial::~ktnMaterial() {
    doubleSided = false;
}
} // namespace ktn::Graphics
