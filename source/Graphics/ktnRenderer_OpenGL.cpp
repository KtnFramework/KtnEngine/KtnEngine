#include <ktnEngine/Graphics/ktnRenderer_OpenGL.hpp>

#include "internal/OpenGL/DefaultShaders.hpp"

#include <ktnEngine/Graphics/ktnGraphicsUtils.hpp>

#include <epoxy/gl.h>
#include <glm/gtc/matrix_transform.hpp>
#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"

#include <chrono>
#include <iostream>

using namespace glm;
using namespace std;
using namespace std::chrono;

using namespace ktn::Graphics;
namespace ktn {
const float FIELD_OF_VIEW_Y(radians(45.F));

const float Z_NEAR(0.1F);
const float Z_FAR(100.F);

ktnRenderer_OpenGL::ktnRenderer_OpenGL() : IktnRenderer("OpenGL Renderer") {
    system_clock::time_point tp = system_clock::now();
    system_clock::duration dtn = tp.time_since_epoch();
    m_DebugText.emplace_back(std::pair<size_t, string>(dtn.count() * system_clock::period::num / system_clock::period::den, "OpenGL renderer instantiated."));

    Shaders[StandardShaderString::DeferredLight] = nullptr;
    Shaders[StandardShaderString::GBuffer] = nullptr;
}

ktnRenderer_OpenGL::~ktnRenderer_OpenGL() {
    CleanUp();
}

ktnGraphicsBackend ktnRenderer_OpenGL::Backend() {
    return ktnGraphicsBackend::OPENGL;
}

void ktnRenderer_OpenGL::HandleEvent_FramebufferSizeChanged(int width, int height) {
    m_Extent2D = vk::Extent2D(width, height);
    WIP; // this is only partially done, the buffer does change but program crashes after changing framebuffer size a few time. No diagnosis yet.
    CleanUpFrameBuffers();
    SetUpFramebuffers(m_Extent2D);
    const float ASPECT_RATIO(static_cast<float>(m_Extent2D.width) / m_Extent2D.height);
    mat4 ProjectionMatrix = perspective(FIELD_OF_VIEW_Y, ASPECT_RATIO, Z_NEAR, Z_FAR);
    if (Shaders[StandardShaderString::GBuffer] != nullptr) {
        auto *shader = Shaders[StandardShaderString::GBuffer];
        shader->Use();
        shader->SetUniformMat4("ProjectionMatrix", ProjectionMatrix);
    }
}

void ktnRenderer_OpenGL::SetContext(void * /*eContext*/) {}

void ktnRenderer_OpenGL::SetShader(const string &name, ktnShaderProgram *shader) {
    if (Shaders[name] != nullptr) { delete Shaders[name]; }
    Shaders[name] = shader;
    const float ASPECT_RATIO(static_cast<float>(m_Extent2D.width) / m_Extent2D.height);
    mat4 ProjectionMatrix = perspective(FIELD_OF_VIEW_Y, ASPECT_RATIO, Z_NEAR, Z_FAR);

    if (StandardShaderString::GBuffer == name) {
        shader->Use();
        shader->SetUniformMat4("ProjectionMatrix", ProjectionMatrix);
    }
}

#ifndef NDEBUG // TODO: put all debug helpers into separate files
string Stringify_OpenGLSeverity(GLenum eSeverity) {
    string severity;
    switch (eSeverity) {
    case GL_DEBUG_SEVERITY_NOTIFICATION:
        severity = "notification";
        break;
    case GL_DEBUG_SEVERITY_HIGH:
        severity = "high";
        break;
    case GL_DEBUG_SEVERITY_MEDIUM:
        severity = "medium";
        break;
    case GL_DEBUG_SEVERITY_LOW:
        severity = "low";
        break;
    default:
        severity = "unknown";
    }
    return severity;
}

// The callback for all opengl debug messages
// Note that the last parameter, const void *userParam is not used, therefore not named
void GLAPIENTRY dmc(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void * /*userParam*/) {
    if (severity == GL_DEBUG_SEVERITY_NOTIFICATION) { return; }

    cout << "OpenGL debug message received:\n" //
         << "  source: 0x" << hex << source //
         << ", type: 0x" << hex << type //
         << ", id: " << dec << id //
         << ", severity: " << Stringify_OpenGLSeverity(severity) //
         << ", length: " << dec << length << "\n" //
         << "  message: " << message << "\n"
         << endl;
    throw runtime_error("OpenGL Error");
}
#endif

void ktnRenderer_OpenGL::Initialize(const vk::Extent2D &eExtent2D, unsigned int shadowMapSize) {
#ifndef NDEBUG
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(dmc, nullptr);
#endif
    auto width = eExtent2D.width;
    auto height = eExtent2D.height;

    glEnable(GL_DEPTH_TEST); // enable depth test
    // 3 lines for backface culling
    glFrontFace(GL_CCW); // set front face mode, clockwise or counter clockwise
    glEnable(GL_CULL_FACE); // enable culling
    glCullFace(GL_BACK); // set culliing mode to front, back or both

    m_ShadowMapSize = shadowMapSize;
    ktnFrameBuffer *shadowbuffer = new ktnFrameBuffer();
    CreateOnGPU(shadowbuffer, FrameBufferType::DEPTH, vk::Extent2D(m_ShadowMapSize, m_ShadowMapSize));
    ShadowBuffer_Directional.push_back(shadowbuffer);

    m_Extent2D.setWidth(width);
    m_Extent2D.setHeight(height);

    SetUpFramebuffers(m_Extent2D);
    // set up fullscreen quad
    float quadVertices[] = {// positions       // texCoords
                            -1.0F, 1.0F,  0.0F, 0.0F, 1.0F, //
                            -1.0F, -1.0F, 0.0F, 0.0F, 0.0F, //
                            1.0F,  1.0F,  0.0F, 1.0F, 1.0F, //
                            1.0F,  -1.0F, 0.0F, 1.0F, 0.0F};

    glGenVertexArrays(1, &m_FullscreenRectangleVertexArray);
    glGenBuffers(1, &m_FullscreenRectangleVertexBuffer);
    glBindVertexArray(m_FullscreenRectangleVertexArray);
    glBindBuffer(GL_ARRAY_BUFFER, m_FullscreenRectangleVertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);

    const int SIZE_OF_POSITION(3);
    const int SIZE_OF_VERTEX(5);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, SIZE_OF_VERTEX * sizeof(float), nullptr);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, SIZE_OF_VERTEX * sizeof(float), (void *)(SIZE_OF_POSITION * sizeof(float)));

    //    mat4 ProjectionMatrix = perspective(FIELD_OF_VIEW_Y, ASPECT_RATIO, Z_NEAR, Z_FAR);

    // initialize shaders
    if (Shaders[StandardShaderString::DeferredLight] == nullptr) {
        ktnShaderProgram *deferredLightShader = new ktnShaderProgram( //
            OpenGL::DEFAULT_SHADER_FULL_SCREEN_RECTANGLE_VERT,
            OpenGL::DEFAULT_SHADER_DEFERRED_LIGHT_FRAG);
        Shaders["DeferredLight"] = deferredLightShader;
    }

    if (Shaders[StandardShaderString::GBuffer] == nullptr) {
        ktnShaderProgram *gBufferShader = new ktnShaderProgram( //
            OpenGL::DEFAULT_SHADER_G_BUFFER_VERT,
            OpenGL::DEFAULT_SHADER_G_BUFFER_FRAG);
        SetShader(StandardShaderString::GBuffer, gBufferShader);
    }

    ktnShaderProgram *fullscreenRectangleShader = new ktnShaderProgram( //
        OpenGL::DEFAULT_SHADER_FULL_SCREEN_RECTANGLE_VERT,
        OpenGL::DEFAULT_SHADER_SCREEN_FRAG);
    Shaders["FullscreenRectangle"] = fullscreenRectangleShader;

    /////////////////////////////////////
    // TODO: remove the following shaders
    /////////////////////////////////////
    //    ktnShaderProgram *defaultColorShader = new ktnShaderProgram(OpenGL::DEFAULT_SHADER_DEFAULT_VERT, OpenGL::DEFAULT_SHADER_DEFAULT_FRAG);
    //    defaultColorShader->Pass = ShaderPass::COLOR;
    //    defaultColorShader->Use();
    //    defaultColorShader->SetUniformMat4("ProjectionMatrix", ProjectionMatrix);

    //    ktnShaderProgram *blurShader = new ktnShaderProgram(OpenGL::DEFAULT_SHADER_FULL_SCREEN_RECTANGLE_VERT, OpenGL::DEFAULT_SHADER_BLUR_FRAG);
    //    blurShader->Pass = ShaderPass::POSTPROCESS;

    //    ktnShaderProgram *bloomShader = new ktnShaderProgram(OpenGL::DEFAULT_SHADER_FULL_SCREEN_RECTANGLE_VERT, OpenGL::DEFAULT_SHADER_BLOOM_FRAG);

    ktnShaderProgram *shadowShader = new ktnShaderProgram(OpenGL::DEFAULT_SHADER_SHADOW_VERT, OpenGL::DEFAULT_SHADER_SHADOW_FRAG);
    shadowShader->Pass = ShaderPass::SHADOW;

    //    Shaders["Bloom"] = bloomShader;
    //    Shaders["Blur"] = blurShader;
    //    Shaders["Color"] = defaultColorShader;
    Shaders["Shadow"] = shadowShader;

    m_Initialized = true;
}

void ktnRenderer_OpenGL::WaitIdle() {
    //
}

void ktnRenderer_OpenGL::CleanUp() {
    if (m_Initialized) {
        CleanUpFrameBuffers();
        for (auto &shader : Shaders) {
            delete shader.second;
            shader.second = nullptr;
        }
        glDeleteVertexArrays(1, &m_FullscreenRectangleVertexArray);
        glDeleteBuffers(1, &m_FullscreenRectangleVertexBuffer);
        m_Initialized = false;
    }
}

void ktnRenderer_OpenGL::Bind(ktnFrameBuffer *eBuffer) {
    glBindFramebuffer(GL_FRAMEBUFFER, eBuffer->ID);
}

void ktnRenderer_OpenGL::CreateOnGPU(ktnFrameBuffer *eBuffer, const FrameBufferType &type, const vk::Extent2D &eExtent2D, const unsigned int &nTextures) {
    if (nTextures < 1) { throw invalid_argument("ktnFrameBuffer: invalid number of textures"); }
    glGenFramebuffers(1, &eBuffer->ID);
    glBindFramebuffer(GL_FRAMEBUFFER, eBuffer->ID);

    switch (type) {
    case FrameBufferType::COLOR_RGB: {
        vector<unsigned int> attachments;
        // attach the color textures to this framebuffer in the GPU
        for (unsigned int i = 0; i < nTextures; ++i) {
            attachments.push_back(GL_COLOR_ATTACHMENT0 + i);
            auto texture = make_shared<ktnTexture>(eExtent2D);
            texture->CreateOnGPU(TextureType::DIFFUSE);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, texture->ID, 0);
            eBuffer->Textures.emplace_back(texture);
        }
        glDrawBuffers(static_cast<int>(eBuffer->Textures.size()), attachments.data());
    } break;

    case FrameBufferType::DEPTH: { // attach the texture to this framebuffer in the GPU
        auto texture = make_shared<ktnTexture>(eExtent2D);
        texture->CreateOnGPU(TextureType::DEPTH);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texture->ID, 0);
        eBuffer->Textures.emplace_back(texture);
        glDrawBuffer(GL_NONE);
        glReadBuffer(GL_NONE);
    } break;

    case FrameBufferType::G_BUFFER: {
        vector<unsigned int> attachments;

        // position attachment
        attachments.push_back(GL_COLOR_ATTACHMENT0);
        auto position = make_shared<ktnTexture>(eExtent2D);
        position->CreateOnGPU(TextureType::G_BUFFER_POSITION);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, position->ID, 0);
        eBuffer->Textures.emplace_back(position);

        // normal attachment
        attachments.push_back(GL_COLOR_ATTACHMENT1);
        auto normal = make_shared<ktnTexture>(eExtent2D);
        normal->CreateOnGPU(TextureType::G_BUFFER_NORMAL);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, normal->ID, 0);
        eBuffer->Textures.emplace_back(normal);

        // diffuse attachment
        attachments.push_back(GL_COLOR_ATTACHMENT2);
        auto diffuse = make_shared<ktnTexture>(eExtent2D);
        diffuse->CreateOnGPU(TextureType::G_BUFFER_DIFFUSE);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, diffuse->ID, 0);
        eBuffer->Textures.emplace_back(diffuse);

        // texture coordinates attachment
        attachments.push_back(GL_COLOR_ATTACHMENT3);
        auto texCoords = make_shared<ktnTexture>(eExtent2D);
        texCoords->CreateOnGPU(TextureType::G_BUFFER_TEXCOORDS);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, texCoords->ID, 0);
        eBuffer->Textures.emplace_back(texCoords);

        glDrawBuffers(static_cast<int>(eBuffer->Textures.size()), attachments.data());
    } break;

    default:
        throw runtime_error("Unknown frame buffer type");
    }

    if (type != FrameBufferType::DEPTH) {
        /*
        the rbo in here is used to store depth and stencil values
        we do not need to read these values
        but they are needed for depth checking
        and therefore will be kept here with a local variable
        */
        GLuint rbo;
        glGenRenderbuffers(1, &rbo);
        glBindRenderbuffer(GL_RENDERBUFFER, rbo);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, static_cast<int>(eExtent2D.width), static_cast<int>(eExtent2D.height));
        glBindRenderbuffer(GL_RENDERBUFFER, 0);

        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);
    }
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        cerr << "ktnFrameBuffer::ktnFrameBuffer: Frame buffer is not complete." << endl;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    eBuffer->Extent2D = eExtent2D;
}

void ktnRenderer_OpenGL::LoadToGPU(ktnGraphicsScenePtr eScene) {
    for (auto &model : eScene->Models) { LoadToGPU(model.get()); }
}

void ktnRenderer_OpenGL::LoadToGPU(ktnMesh *eMesh) {
    if (eMesh->Material.pbrMetallicRoughness->baseColorTexture != nullptr) { //
        LoadToGPU(eMesh->Material.pbrMetallicRoughness->baseColorTexture->Texture.get());
    }
    glGenVertexArrays(1, &eMesh->VertexArrayObject);
    glGenBuffers(1, &eMesh->VertexBufferObject);
    glGenBuffers(1, &eMesh->ElementBufferObject);

    glBindVertexArray(eMesh->VertexArrayObject);
    glBindBuffer(GL_ARRAY_BUFFER, eMesh->VertexBufferObject);

    glBufferData(
        GL_ARRAY_BUFFER, // TODO add description comments for the parameters
        eMesh->Buffer.size() * sizeof(float),
        eMesh->Buffer.data(),
        GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eMesh->ElementBufferObject);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, eMesh->Indices.size() * sizeof(uint32_t), eMesh->Indices.data(), GL_STATIC_DRAW);

    size_t offset = 0;

    // vertex positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), (void *)offset);

    // vertex normals
    glEnableVertexAttribArray(1);
    offset += sizeof(vec3) * eMesh->Positions.size();
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), (void *)offset);

    // vertex tangent
    glEnableVertexAttribArray(2);
    offset += sizeof(vec3) * eMesh->Positions.size();
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(vec4), (void *)offset);

    // vertex texture coords
    glEnableVertexAttribArray(3);
    offset += sizeof(vec4) * eMesh->Positions.size();
    glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(vec2), (void *)offset);

    // vertex joints
    glEnableVertexAttribArray(4);
    offset += sizeof(vec2) * eMesh->Positions.size();
    glVertexAttribIPointer(4, 4, GL_INT, sizeof(uvec4), (void *)offset);

    // vertex weights
    glEnableVertexAttribArray(5);
    offset += sizeof(uvec4) * eMesh->Positions.size();
    glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(vec4), (void *)offset);

    glBindVertexArray(0);
}

void ktnRenderer_OpenGL::LoadToGPU(ktnModel *eModel) {
    for (auto &mesh : eModel->Meshes) { LoadToGPU(mesh.second); }
}

void ktnRenderer_OpenGL::LoadToGPU(ktnTexture *eTexture) {
    glGenTextures(1, &eTexture->ID);
    int width = 0;
    int height = 0;
    int channels = 0;
    unsigned char *data = stbi_load(eTexture->m_Path.string().c_str(), &width, &height, &channels, 0);
    if (data != nullptr) {
        eTexture->m_Extent2D.width = static_cast<uint32_t>(width);
        eTexture->m_Extent2D.height = static_cast<uint32_t>(height);
        eTexture->m_nChannels = static_cast<uint8_t>(channels);
        GLenum format;
        if (eTexture->m_nChannels == 1) {
            format = GL_RED;
        } else if (eTexture->m_nChannels == 3) {
            format = GL_RGB;
        } else if (eTexture->m_nChannels == 4) {
            format = GL_RGBA;
        } else { // to prevent format being uninitialized
            cout << "ktnTexture: The texture at " << eTexture->m_Path << " has " << eTexture->m_nChannels << " color channels, which is not supported. "
                 << "An image with 1, 3 or 4 channels is needed." << endl;
            stbi_image_free(data);
            return;
        }
        glBindTexture(GL_TEXTURE_2D, eTexture->ID);
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            static_cast<GLint>(format),
            static_cast<GLsizei>(eTexture->m_Extent2D.width),
            static_cast<GLsizei>(eTexture->m_Extent2D.height),
            0,
            format,
            GL_UNSIGNED_BYTE,
            data);
        glGenerateMipmap(GL_TEXTURE_2D);

        // set texture wrapping parameters
        eTexture->SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        eTexture->SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        // set texture filtering parameters
        eTexture->SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        eTexture->SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glBindTexture(GL_TEXTURE_2D, 0);
        stbi_image_free(data);
    } else {
        stbi_image_free(data);
        throw std::runtime_error("ktnTexture: Failed to load texture: " + eTexture->m_Path.string());
    }
}

void ktnRenderer_OpenGL::UnbindFrameBuffers() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void ktnRenderer_OpenGL::UnloadFromGPU(ktnFrameBuffer *eBuffer) {
    for (auto &texture : eBuffer->Textures) { UnloadFromGPU(texture.get()); }
    glDeleteFramebuffers(1, &eBuffer->ID);
}

void ktnRenderer_OpenGL::UnloadFromGPU(ktnGraphicsScenePtr /*eScene*/) {
    WIP // not yet implemented, most likely why valgrind complained about external errors before
}

void ktnRenderer_OpenGL::UnloadFromGPU(ktnTexture *eTexture) {
    glDeleteTextures(1, &eTexture->ID);
}

void ktnRenderer_OpenGL::RenderFrame(ktnGraphicsScenePtr scene) {
    assert(scene->CurrentCamera() != nullptr);
    RenderShadow(scene);
    RenderGBuffer(scene);
    RenderDeferred(scene);

    //    RenderColor(scene);
    //    RenderBlurs();
    //    RenderBloom();
    RenderCombined();
    RenderGUI(scene);
    FrameRendered.Emit();
    auto tmp = Core::Utils::CurrentMicrosecondsSinceEpoch();
    Utils::SetFrameDuration(tmp - Utils::FrameTime());
    Utils::SetFrameTime(tmp);
}

void ktnRenderer_OpenGL::RenderDeferred(const ktnGraphicsScenePtr scene) {
    auto *shader = Shaders["DeferredLight"];
    glViewport(0, 0, m_Extent2D.width, m_Extent2D.height);
    Bind(m_FrameBuffer_Deferred.get());

    shader->Use();
    glDisable(GL_DEPTH_TEST);

    auto gBuffer_Position = m_GBuffer->Textures[0]->ID;
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gBuffer_Position);
    shader->SetUniformInt("gBuffer_Position", 0);

    auto gBuffer_Normal = m_GBuffer->Textures[1]->ID;
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, gBuffer_Normal);
    shader->SetUniformInt("gBuffer_Normal", 1);

    auto gBuffer_Diffuse = m_GBuffer->Textures[2]->ID;
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, gBuffer_Diffuse);
    shader->SetUniformInt("gBuffer_Diffuse", 2);

    auto gBuffer_TexCoords = m_GBuffer->Textures[3]->ID;
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, gBuffer_TexCoords);
    shader->SetUniformInt("gBuffer_TexCoords", 3);

    shader->SetUniformInt("NumberOfLights", (int)scene->Lights.size());
    for (size_t i = 0; i < scene->Lights.size(); ++i) {
        shader->SetUniformBool("lights[" + to_string(i) + "].Enabled", true);
        shader->SetUniformVec3("lights[" + to_string(i) + "].WorldSpacePosition", scene->Lights[i]->Position(WorldSpace));
        shader->SetUniformFloat("lights[" + to_string(i) + "].Intensity", scene->Lights[i]->Intensity);
        shader->SetUniformVec3("lights[" + to_string(i) + "].Color", scene->Lights[i]->Color());
        shader->SetUniformFloat("lights[" + to_string(i) + "].FalloffLinear", scene->Lights[i]->Falloff.LinearTerm);
        shader->SetUniformFloat("lights[" + to_string(i) + "].FalloffQuadratic", scene->Lights[i]->Falloff.QuadraticTerm);
        shader->SetUniformFloat("lights[" + to_string(i) + "].InfluenceRadius", scene->Lights[i]->InfluenceRadius());
    }

    glBindVertexArray(m_FullscreenRectangleVertexArray);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glEnable(GL_DEPTH_TEST);
    UnbindFrameBuffers();
}

void ktnRenderer_OpenGL::RenderGBuffer(const ktnGraphicsScenePtr scene) {
    auto *shader = Shaders["GBuffer"];
    glViewport(0, 0, static_cast<int>(m_GBuffer->Extent2D.width), static_cast<int>(m_GBuffer->Extent2D.height));
    Bind(m_GBuffer.get());
    shader->Use();
    shader->SetUniformFloat("material.shininess", 32.0F);

    float temp = static_cast<float>(steady_clock::now().time_since_epoch().count() % 10);
    shader->SetUniformFloat("pseudorandomnumber", temp);

    shader->SetUniformVec3("viewpos", scene->CurrentCamera()->Position(WorldSpace));
    shader->SetUniformMat4("ViewMatrix", scene->CurrentCamera()->ViewMatrix());

    glEnable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    DrawScene(scene, shader);
    UnbindFrameBuffers();
}

void ktnRenderer_OpenGL::RenderGUI(const ktnGraphicsScenePtr scene) {}

void ktnRenderer_OpenGL::RenderBloom() {
    auto *shader = Shaders["Bloom"];
    glViewport(0, 0, static_cast<int>(m_Extent2D.width), static_cast<int>(m_Extent2D.height));
    // apply blur to get bloom
    Bind(m_BloomBuffer.get());
    shader->Use();

    glActiveTexture(GL_TEXTURE0 + m_FrameBuffer->Textures[0]->ID);
    glBindTexture(GL_TEXTURE_2D, m_FrameBuffer->Textures[0]->ID);
    shader->SetUniformInt("inputImage", static_cast<int>(m_FrameBuffer->Textures[0]->ID));

    unsigned int lightBlurImgID = m_BlurBuffersMedium[1]->Textures[0]->ID;
    glActiveTexture(GL_TEXTURE0 + lightBlurImgID);
    glBindTexture(GL_TEXTURE_2D, lightBlurImgID);
    shader->SetUniformInt("blurredImage", static_cast<int>(lightBlurImgID));

    unsigned int heavyBlurImgID = m_BlurBuffersSmall[1]->Textures[0]->ID;
    glActiveTexture(GL_TEXTURE0 + heavyBlurImgID);
    glBindTexture(GL_TEXTURE_2D, heavyBlurImgID);
    shader->SetUniformInt("heavilyBlurredImage", static_cast<int>(heavyBlurImgID));

    shader->SetUniformFloat("exposure", 1.0);
    shader->SetUniformFloat("bigBlurIntensity", 0.5F);
    shader->SetUniformFloat("smallBlurIntensity", 0.8F);
    WIP; // the values of big blur and small blur intensities should be variables
    glBindVertexArray(m_FullscreenRectangleVertexArray);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    UnbindFrameBuffers();
}

void ktnRenderer_OpenGL::RenderBlurs() {
    auto *blurShader = Shaders["Blur"];
    auto *fullscreenRectangleShader = Shaders["FullscreenRectangle"];

    glBindVertexArray(m_FullscreenRectangleVertexArray);
    blurShader->Use();
    /*
     * for the lack of a better term,
     * orientation can be used to describe either horizontal or vertical,
     * with horizontal being true
     */
    size_t orientation = 1;
    bool firstIteration = true;

    glDisable(GL_DEPTH_TEST);
    // resize the original high-contrast image
    glViewport(0, 0, m_FrameBufferMedium->Extent2D.width, m_FrameBufferMedium->Extent2D.height);
    Bind(m_FrameBufferMedium.get());
    fullscreenRectangleShader->Use();
    glActiveTexture(GL_TEXTURE0 + m_FrameBuffer->Textures[1]->ID);
    glBindTexture(GL_TEXTURE_2D, m_FrameBuffer->Textures[1]->ID);
    fullscreenRectangleShader->SetUniformInt("screenTexture", m_FrameBuffer->Textures[1]->ID);
    glBindVertexArray(m_FullscreenRectangleVertexArray);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    // framebufferMedium.Unbind();

    blurShader->Use();
    const unsigned int howmanytimes = 8;
    glDisable(GL_DEPTH_TEST); // so that the quad is not discarded
    for (unsigned int i = 0; i < howmanytimes; ++i) {
        Bind(m_BlurBuffersMedium.at(orientation).get());
        blurShader->SetUniformInt("orientation", static_cast<int>(orientation));
        unsigned int textureNumber =
            firstIteration ? m_FrameBufferMedium->Textures[0]->ID : m_BlurBuffersMedium.at(static_cast<size_t>(orientation == 0U))->Textures[0]->ID;
        glActiveTexture(GL_TEXTURE0 + textureNumber);
        glBindTexture(GL_TEXTURE_2D, textureNumber);
        blurShader->SetUniformInt("inputImage", static_cast<int>(textureNumber));
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        orientation = static_cast<size_t>(orientation == 0U);
        if (firstIteration) { firstIteration = false; }
    }
    // glEnable(GL_DEPTH_TEST);

    // shrink the image down even more
    glViewport(0, 0, m_FrameBufferSmall->Extent2D.width, m_FrameBufferSmall->Extent2D.height);
    Bind(m_FrameBufferSmall.get());
    fullscreenRectangleShader->Use();
    glActiveTexture(GL_TEXTURE0 + m_FrameBufferMedium->Textures[0]->ID);
    glBindTexture(GL_TEXTURE_2D, m_FrameBufferMedium->Textures[0]->ID);
    fullscreenRectangleShader->SetUniformInt("screenTexture", m_FrameBufferMedium->Textures[0]->ID);
    glBindVertexArray(m_FullscreenRectangleVertexArray);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    blurShader->Use();
    /*
     * for the lack of a better term,
     * orientation can be used to describe either horizontal or vertical,
     * with horizontal being true
     */
    orientation = 1;
    firstIteration = true;
    glDisable(GL_DEPTH_TEST); // so that the quad is not discarded
    for (unsigned int i = 0; i < howmanytimes; ++i) {
        Bind(m_BlurBuffersSmall.at(orientation).get());
        blurShader->SetUniformInt("orientation", static_cast<int>(orientation));
        unsigned int textureNumber =
            firstIteration ? m_FrameBufferSmall->Textures[0]->ID : m_BlurBuffersSmall.at(static_cast<size_t>(orientation == 0U))->Textures[0]->ID;
        glActiveTexture(GL_TEXTURE0 + textureNumber);
        glBindTexture(GL_TEXTURE_2D, textureNumber);
        blurShader->SetUniformInt("inputImage", textureNumber);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        orientation = static_cast<size_t>(orientation == 0U);
        if (firstIteration) { firstIteration = false; }
    }
    // glEnable(GL_DEPTH_TEST);
    UnbindFrameBuffers();
}

void ktnRenderer_OpenGL::RenderColor(ktnGraphicsScenePtr scene) {
    auto *shader = Shaders["Color"];
    glViewport(0, 0, m_FrameBuffer->Extent2D.width, m_FrameBuffer->Extent2D.height);
    Bind(m_FrameBuffer.get());
    shader->Use();
    shader->SetUniformFloat("material.shininess", 32.0F);
    shader->SetUniformInt("shadowmap", static_cast<int>(ShadowBuffer_Directional[0]->ID));
    shader->SetUniformMat4("LightSpaceMatrix", scene->Lights[0]->ProjectionMatrix * scene->Lights[0]->ViewMatrix);

    shader->SetUniformFloat("light.intensity", scene->Lights[0]->Intensity);
    shader->SetUniformVec3("light.position", scene->Lights[0]->Position(WorldSpace));
    shader->SetUniformVec3("light.direction", scene->Lights[0]->Direction());
    shader->SetUniformFloat("light.cutoff.inner", scene->Lights[0]->Cutoff.InnerCos());
    shader->SetUniformFloat("light.cutoff.outer", scene->Lights[0]->Cutoff.OuterCos());
    shader->SetUniformVec3("light.color", scene->Lights[0]->Color());
    shader->SetUniformFloat("light.falloff.linearterm", scene->Lights[0]->Falloff.LinearTerm);
    shader->SetUniformFloat("light.falloff.quadraticterm", scene->Lights[0]->Falloff.QuadraticTerm);

    auto temp = static_cast<float>(steady_clock::now().time_since_epoch().count() % 10);
    shader->SetUniformFloat("pseudorandomnumber", temp);

    shader->SetUniformVec3("viewpos", scene->CurrentCamera()->Position(WorldSpace));
    shader->SetUniformMat4("ViewMatrix", scene->CurrentCamera()->ViewMatrix());

    glEnable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    DrawScene(scene, shader);
    UnbindFrameBuffers();
}

void ktnRenderer_OpenGL::RenderCombined() {
    auto *shader = Shaders["FullscreenRectangle"];
    glViewport(0, 0, m_Extent2D.width, m_Extent2D.height);
    // use the fullscreen rectangle shader to render an arbitrary image from any of the previous stages
    shader->Use();
    glDisable(GL_DEPTH_TEST);
    auto textureNumber = m_FrameBuffer_Deferred->Textures[0]->ID;
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureNumber);
    shader->SetUniformInt("screenTexture", 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0); // set to draw to the on-screen framebuffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glBindVertexArray(m_FullscreenRectangleVertexArray);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glEnable(GL_DEPTH_TEST);
}

void ktnRenderer_OpenGL::RenderShadow(ktnGraphicsScenePtr scene) {
    auto *shader = Shaders["Shadow"];
    glEnable(GL_DEPTH_TEST); // enable depth test
    if (!ShadowBuffer_Directional.empty()) {
        ktnFrameBuffer *shadowBuffer = ShadowBuffer_Directional.at(0);
        const int width = shadowBuffer->Extent2D.width;
        const int height = shadowBuffer->Extent2D.height;
        glViewport(0, 0, width, height);

        unsigned int depthMapLocation = ShadowBuffer_Directional[0]->ID;
        Bind(shadowBuffer);

        const float nearplane(0.1F);
        const float farplane(20.0F);
        glm::mat4 lightspace = IDENTITY_MATRIX_4;
        shader->Use();
        if (scene->Lights.empty()) {
            return; // prevent crash
        }

        shader->SetUniformMat4("ModelMatrix", scene->Lights[0]->ModelMatrix(WorldSpace));
        constexpr float ortho_halfHeight(2.0F); // half of the orthographic projection's height
        constexpr float ortho_halfWidth(2.0F); // half of the orthographic projection's width
        scene->Lights[0]->ProjectionMatrix = glm::ortho(-ortho_halfWidth, ortho_halfWidth, -ortho_halfHeight, ortho_halfHeight, nearplane, farplane);
        scene->Lights[0]->ViewMatrix = glm::lookAt(
            scene->Lights[0]->Position(WorldSpace),
            scene->Lights[0]->Position(WorldSpace) + scene->Lights[0]->Direction(),
            scene->CurrentCamera()->UniversalUp());
        lightspace = scene->Lights[0]->ProjectionMatrix * scene->Lights[0]->ViewMatrix;
        shader->SetUniformMat4("LightSpaceMatrix", lightspace);

        glActiveTexture(GL_TEXTURE0 + depthMapLocation);
        glBindTexture(GL_TEXTURE_2D, shadowBuffer->Textures[0]->ID);
        // render objects
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        DrawScene(scene, shader);
        UnbindFrameBuffers();
    }
}

void ktnRenderer_OpenGL::CleanUpFrameBuffers() {
    UnloadFromGPU(m_FrameBuffer.get());
    m_FrameBuffer.reset();

    UnloadFromGPU(m_FrameBufferMedium.get());
    m_FrameBufferMedium.reset();
    UnloadFromGPU(m_BlurBuffersMedium[0].get());
    m_BlurBuffersMedium[0].reset();
    UnloadFromGPU(m_BlurBuffersMedium[1].get());
    m_BlurBuffersMedium[1].reset();

    UnloadFromGPU(m_FrameBufferSmall.get());
    m_FrameBufferSmall.reset();
    UnloadFromGPU(m_BlurBuffersSmall[0].get());
    m_BlurBuffersSmall[0].reset();
    UnloadFromGPU(m_BlurBuffersSmall[1].get());
    m_BlurBuffersSmall[1].reset();

    UnloadFromGPU(m_BloomBuffer.get());
    m_BloomBuffer.reset();

    UnloadFromGPU(m_GBuffer.get());
    m_GBuffer.reset();
    UnloadFromGPU(m_FrameBuffer_Deferred.get());
    m_FrameBuffer_Deferred.reset();
}

void ktnRenderer_OpenGL::DrawMesh(ktnMesh *eMesh, ktnShaderProgram *eShader) {
#ifdef KTN_USE_OPENGL
    eShader->SetUniformMat4("ModelMatrix", eMesh->AnimatedModelMatrix(WorldSpace));
    if (nullptr != eMesh->Skin) {
        if (0 != eMesh->Skin->Joints.size()) {
            for (size_t i = 0; i < eMesh->Skin->Joints.size(); ++i) {
                eShader->SetUniformMat4("JointMatrices[" + to_string(i) + "]", eMesh->Skin->Joints[i].second->AnimatedModelMatrix(WorldSpace));
            }
        }
    }

    // TODO: remove ShaderPass::COLOR eventually, as it is too generic
    if (ShaderPass::COLOR == eShader->Pass || ShaderPass::G_BUFFER == eShader->Pass) {
        eShader->SetUniformVec4("material.baseColorFactor", eMesh->Material.pbrMetallicRoughness->baseColorFactor);
        if (eMesh->Material.pbrMetallicRoughness->baseColorTexture.get() != nullptr) {
            eShader->SetUniformBool("material.baseColorTextureIsSet", true);
            ktnTexture *texture = eMesh->Material.pbrMetallicRoughness->baseColorTexture->Texture.get();
            glActiveTexture(GL_TEXTURE0); // we have to set the active texture before binding any texture to it
            eShader->SetUniformInt("material.baseColorTexture", 0);
            glBindTexture(GL_TEXTURE_2D, texture->ID);
        }
    }
    glActiveTexture(GL_TEXTURE0); // reset active texture

    if (ShaderPass::SHADOW == eShader->Pass) {
        // don't do any of the texture gymnastics
    }
    glBindVertexArray(eMesh->VertexArrayObject);
    glDrawElements(
        GL_TRIANGLES, // TODO add description comments for the parameters
        static_cast<int>(eMesh->Indices.size()),
        GL_UNSIGNED_INT,
        nullptr);

    glBindVertexArray(0); // reset vertex array object
#endif
}

void ktnRenderer_OpenGL::DrawModel(std::shared_ptr<ktnModel> eModel, ktnShaderProgram *eShader) {
    // interpolate the local space transformations of each action's channels' target
    for (auto &mesh : eModel->Meshes) { mesh.second->ResetAnimatedTransforms(); }
    for (auto &skin : eModel->Skins) {
        for (auto &joint : skin.second->Joints) { joint.second->ResetAnimatedTransforms(); }
    }
    for (auto &action : eModel->CurrentActions) { action->Interpolate(); }
    Core::Utils::EraseRemove(eModel->CurrentActions, [](shared_ptr<ktnAction> action) { return action->Completed; });

    // update all meshes' animated world space transformations
    vector<ktnMesh *> rootMeshes;
    for (auto &mesh : eModel->Meshes) {
        if (nullptr == dynamic_cast<ktnMesh *>(mesh.second->Parent())) { //
            rootMeshes.push_back(mesh.second);
        }
    }
    for (auto &mesh : rootMeshes) { mesh->UpdateAnimatedLocalSpaceModelMatrix(); }

    // update all joints' animated world space transformations
    vector<Core::ktn3DElement *> rootJoints;
    for (auto &skin : eModel->Skins) {
        for (auto &joint : skin.second->Joints) {
            if (nullptr == dynamic_cast<ktnJoint *>(joint.second->Parent())) { //
                rootJoints.push_back(joint.second);
            }
        }
    }

    for (auto &skin : eModel->Skins) {
        for (auto &joint : skin.second->Joints) { joint.second->UpdateAnimatedLocalSpaceModelMatrix(); }
    }
    for (auto &joint : rootJoints) { joint->UpdateAnimatedWorldSpaceModelMatrix(); }

    eModel->UpdateAnimatedWorldSpaceModelMatrix();

    // Assuming the shader is already in use - this should be done by the renderer, since one shader is used for many models.
    for (auto &mesh : eModel->Meshes) { DrawMesh(mesh.second, eShader); }
}

void ktnRenderer_OpenGL::DrawScene(ktnGraphicsScenePtr eGraphicsScene, ktnShaderProgram *eShader) {
    for (const auto &model : eGraphicsScene->Models) { DrawModel(model, eShader); }
}

void ktnRenderer_OpenGL::SetUpFramebuffers(const vk::Extent2D &eExtent2D) {
    // Set up the frame buffers
    m_FrameBuffer = make_unique<ktnFrameBuffer>();
    CreateOnGPU(m_FrameBuffer.get(), FrameBufferType::COLOR_RGB, eExtent2D, 2);

    vk::Extent2D downscaledExtent_Medium = vk::Extent2D(eExtent2D.width / m_DownscaleFactor_Medium, eExtent2D.height / m_DownscaleFactor_Medium);
    m_FrameBufferMedium = make_unique<ktnFrameBuffer>();
    CreateOnGPU(m_FrameBufferMedium.get(), FrameBufferType::COLOR_RGB, downscaledExtent_Medium);
    m_BlurBuffersMedium[0] = make_unique<ktnFrameBuffer>();
    CreateOnGPU(m_BlurBuffersMedium[0].get(), FrameBufferType::COLOR_RGB, downscaledExtent_Medium);
    m_BlurBuffersMedium[1] = make_unique<ktnFrameBuffer>();
    CreateOnGPU(m_BlurBuffersMedium[1].get(), FrameBufferType::COLOR_RGB, downscaledExtent_Medium);

    vk::Extent2D downscaledExtent_Small = vk::Extent2D(eExtent2D.width / m_DownscaleFactor_Small, eExtent2D.height / m_DownscaleFactor_Small);
    m_FrameBufferSmall = make_unique<ktnFrameBuffer>();
    CreateOnGPU(m_FrameBufferSmall.get(), FrameBufferType::COLOR_RGB, downscaledExtent_Small);
    m_BlurBuffersSmall[0] = make_unique<ktnFrameBuffer>();
    CreateOnGPU(m_BlurBuffersSmall[0].get(), FrameBufferType::COLOR_RGB, downscaledExtent_Small);
    m_BlurBuffersSmall[1] = make_unique<ktnFrameBuffer>();
    CreateOnGPU(m_BlurBuffersSmall[1].get(), FrameBufferType::COLOR_RGB, downscaledExtent_Small);

    m_BloomBuffer = make_unique<ktnFrameBuffer>();
    CreateOnGPU(m_BloomBuffer.get(), FrameBufferType::COLOR_RGB, eExtent2D);

    m_GBuffer = make_unique<ktnFrameBuffer>();
    CreateOnGPU(m_GBuffer.get(), FrameBufferType::G_BUFFER, eExtent2D, 2);
    m_FrameBuffer_Deferred = make_unique<ktnFrameBuffer>();
    CreateOnGPU(m_FrameBuffer_Deferred.get(), FrameBufferType::COLOR_RGB, eExtent2D);
}
} // namespace ktn
