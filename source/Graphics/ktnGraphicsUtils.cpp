#include <ktnEngine/Graphics/ktnGraphicsUtils.hpp>

namespace ktn::Graphics::Utils {
static long StaticFrameDuration = 0;
static long StaticFrameTime = 0;

long FrameDuration() {
    return StaticFrameDuration;
}
long FrameTime() {
    return StaticFrameTime;
}

void SetFrameDuration(long value) {
    StaticFrameDuration = value;
}

void SetFrameTime(long value) {
    StaticFrameTime = value;
}
} // namespace ktn::Graphics::Utils
