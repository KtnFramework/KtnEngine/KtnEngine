#include <ktnEngine/Graphics/ktnLight.hpp>

using namespace std;

using glm::vec3;
using ktn::Core::ktnName;
using ktn::Core::Utils::CompareFloat;
using ktn::Core::Utils::CompareFloatResult;

namespace ktn::Graphics {
////////////////////
// ktnLight::hCutoff
ktnLight::hCutoff::hCutoff(const float &eInnerAngle, const float &eOuterAngle) {
    if (eInnerAngle > eOuterAngle) { throw invalid_argument("The inner angle is larger than the outer angle."); }
    m_Inner = glm::cos(glm::radians(eInnerAngle));
    m_Outer = glm::cos(glm::radians(eOuterAngle));
}

auto ktnLight::hCutoff::operator==(const hCutoff &eCutoff) const -> bool {
    return CompareFloat(m_Inner, eCutoff.InnerCos()) == CompareFloatResult::Equal //
           && CompareFloat(m_Outer, eCutoff.OuterCos()) == CompareFloatResult::Equal;
}

auto ktnLight::hCutoff::operator!=(hCutoff eCutoff) const -> bool {
    return !operator==(eCutoff);
}

/////////////////////
// ktnLight::hFalloff
ktnLight::hFalloff::hFalloff(const float &eLinearTerm, const float &eQuadraticTerm) {
    LinearTerm = eLinearTerm;
    QuadraticTerm = eQuadraticTerm;
}

auto ktnLight::hFalloff::operator==(hFalloff eFalloff) const -> bool {
    return CompareFloat(LinearTerm, eFalloff.LinearTerm) == CompareFloatResult::Equal //
           && CompareFloat(QuadraticTerm, eFalloff.QuadraticTerm) == CompareFloatResult::Equal;
}

auto ktnLight::hFalloff::operator!=(hFalloff eFalloff) const -> bool {
    return !operator==(eFalloff);
}

///////////
// ktnLight
ktnLight::ktnLight(const string &eName) : ktn3DElement(eName) {}

ktnLight::ktnLight(ktnLight const &other) = default;

ktnLight::ktnLight(ktnLight &&other) = default;

ktnLight::~ktnLight() = default;

auto ktnLight::operator=(ktnLight const &other) -> ktnLight & = default;

auto ktnLight::operator=(ktnLight &&other) -> ktnLight & = default;

auto ktnLight::operator==(const ktnLight &eLight) const -> bool {
    return ktn3DElement::operator==(eLight) //
           && Cutoff == eLight.Cutoff //
           && Falloff == eLight.Falloff //
           && CompareFloat(Intensity, eLight.Intensity) == CompareFloatResult::Equal //
           && IsCastingShadows == eLight.IsCastingShadows //
           && m_Color == eLight.Color() //
           && m_Direction == eLight.Direction() //
           && ProjectionMatrix == eLight.ProjectionMatrix //
           && Type == eLight.Type //
           && ViewMatrix == eLight.ViewMatrix;
}

auto ktnLight::operator!=(const ktnLight &eLight) const -> bool {
    return !operator==(eLight);
}

auto ktnLight::Color() const -> vec3 {
    return m_Color;
}

auto ktnLight::Direction() const -> vec3 {
    return m_Direction;
}

auto ktnLight::InfluenceRadius() const -> float {
    return m_InfluenceRadius;
}

void ktnLight::SetColor(const vec3 &col) {
    m_Color.r = clamp(col.r, 0.0F, 1.0F);
    m_Color.g = clamp(col.g, 0.0F, 1.0F);
    m_Color.b = clamp(col.b, 0.0F, 1.0F);
    switch (Type) {
    case LightType::POINT:
        CalculateInfluenceRadius();
        break;
    default:
        break;
    }
    CalculateInfluenceRadius();
}

void ktnLight::SetColor(const float &r, const float &g, const float &b) {
    m_Color.r = r;
    m_Color.g = g;
    m_Color.b = b;
}

void ktnLight::SetDirection(const vec3 &dir) {
    m_Direction = dir;
}

void ktnLight::CalculateInfluenceRadius() {
    float colorIntensity = max({m_Color.r, m_Color.g, m_Color.b});
    float l = Falloff.LinearTerm;
    float q = Falloff.QuadraticTerm;
    m_InfluenceRadius = (sqrtf(powf(l, 2) - q * (4 - 1024 * Intensity * colorIntensity)) - l) / (2 * q);
}

} // namespace ktn::Graphics
