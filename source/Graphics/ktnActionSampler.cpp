#include <ktnEngine/Graphics/ktnActionSampler.hpp>

namespace ktn::Graphics {
using namespace std;
using namespace glm;
using namespace gleb;

void ktnActionSampler::LoadRotationSamples(size_t eSampleCount, const bufferView &eBufferView, const buffer &eBuffer) {
    Type = SamplerType::Rotation;
    RotationSamples.reset(new SamplePack<quat>());
    ScaleSamples.reset();
    TranslationSamples.reset();

    vector<quat> samples_rotation;
    samples_rotation.resize(eSampleCount);
    memcpy(samples_rotation.data(), eBuffer.data().data() + eBufferView.byteOffset.value_or(default_byteOffset), eBufferView.byteLength);

    switch (Interpolation) {
    case InterpolationAlgorithm::CUBICSPLINE:
        RotationSamples->PreviousTangent = vector<pair<float, quat>>();
        RotationSamples->NextTangent = vector<pair<float, quat>>();

        for (size_t i = 0; i < eSampleCount / 3; ++i) {
            RotationSamples->PreviousTangent->push_back(pair<float, quat>(SampleTimes[i], samples_rotation[i * 3]));
            RotationSamples->Current.push_back(pair<float, quat>(SampleTimes[i], samples_rotation[i * 3 + 1]));
            RotationSamples->NextTangent->push_back(pair<float, quat>(SampleTimes[i], samples_rotation[i * 3 + 2]));
        }
        break;
    default:
        for (size_t i = 0; i < eSampleCount; ++i) { RotationSamples->Current.push_back(pair<float, quat>(SampleTimes[i], samples_rotation[i])); }
    }
}

void ktnActionSampler::LoadSampleTimes(size_t eSampleCount, bufferView eBufferView, buffer eBuffer) {
    SampleTimes.resize(eSampleCount);
    memcpy( //
        SampleTimes.data(),
        eBuffer.data().data() + eBufferView.byteOffset.value_or(default_byteOffset),
        eBufferView.byteLength);
}

void ktnActionSampler::LoadScaleSamples(size_t eSampleCount, string eType, const bufferView &eBufferView, const buffer &eBuffer) {
    Type = SamplerType::Scale;
    RotationSamples.reset();
    ScaleSamples.reset(new SamplePack<vec3>());
    TranslationSamples.reset();

    if (eType == "SCALAR") { throw runtime_error("Not yet implemented"); }

    if (eType == "VEC3") {
        vector<vec3> samples_scale;
        samples_scale.resize(eSampleCount);
        memcpy(samples_scale.data(), eBuffer.data().data() + eBufferView.byteOffset.value_or(default_byteOffset), eBufferView.byteLength);

        switch (Interpolation) {
        case InterpolationAlgorithm::CUBICSPLINE:
            ScaleSamples->PreviousTangent = vector<pair<float, vec3>>();
            ScaleSamples->NextTangent = vector<pair<float, vec3>>();
            for (size_t i = 0; i < eSampleCount / 3; ++i) {
                ScaleSamples->PreviousTangent->push_back(pair<float, vec3>(SampleTimes[i], samples_scale[i * 3]));
                ScaleSamples->Current.push_back(pair<float, vec3>(SampleTimes[i], samples_scale[i * 3 + 1]));
                ScaleSamples->NextTangent->push_back(pair<float, vec3>(SampleTimes[i], samples_scale[i * 3 + 2]));
            }
            break;
        default:
            for (size_t i = 0; i < eSampleCount; ++i) { //
                ScaleSamples->Current.push_back(pair<float, vec3>(SampleTimes[i], samples_scale[i]));
            }
        }
    }
}

void ktnActionSampler::LoadTranslationSamples(size_t eSampleCount, const bufferView &eBufferView, const buffer &eBuffer) {
    Type = SamplerType::Translation;
    RotationSamples.reset();
    ScaleSamples.reset();
    TranslationSamples.reset(new SamplePack<vec3>());

    vector<vec3> samples_translation;
    samples_translation.resize(eSampleCount);
    memcpy(
        samples_translation.data(), //
        eBuffer.data().data() + eBufferView.byteOffset.value_or(default_byteOffset),
        eBufferView.byteLength);

    switch (Interpolation) {
    case InterpolationAlgorithm::CUBICSPLINE:
        TranslationSamples->PreviousTangent = vector<pair<float, vec3>>();
        TranslationSamples->NextTangent = vector<pair<float, vec3>>();
        for (size_t i = 0; i < eSampleCount / 3; ++i) {
            TranslationSamples->PreviousTangent->push_back(pair<float, vec3>(SampleTimes[i], samples_translation[i * 3]));
            TranslationSamples->Current.push_back(pair<float, vec3>(SampleTimes[i], samples_translation[i * 3 + 1]));
            TranslationSamples->NextTangent->push_back(pair<float, vec3>(SampleTimes[i], samples_translation[i * 3 + 2]));
        }
        break;
    default:
        for (size_t i = 0; i < eSampleCount; ++i) { //
            TranslationSamples->Current.push_back(pair<float, vec3>(SampleTimes[i], samples_translation[i]));
        }
    }
}

void ktnActionSampler::Verify() {
    if (SampleTimes.empty()) { throw runtime_error("Action contains no sample times"); }
    switch (Type) {
    case SamplerType::Rotation:
        if (RotationSamples == nullptr || ScaleSamples != nullptr || TranslationSamples != nullptr) {
            throw runtime_error("Inconsistent sample type information");
        }
        if (SampleTimes.size() != RotationSamples->Current.size()) { throw runtime_error("Inconsistent sample information"); }
        RotationSamples->Verify();
        break;
    case SamplerType::Scale:
        if (RotationSamples != nullptr || ScaleSamples == nullptr || TranslationSamples != nullptr) {
            throw runtime_error("Inconsistent sample type information");
        }
        if (SampleTimes.size() != ScaleSamples->Current.size()) { throw runtime_error("Inconsistent sample information"); }
        ScaleSamples->Verify();
        break;
    case SamplerType::Translation:
        if (RotationSamples != nullptr || ScaleSamples != nullptr || TranslationSamples == nullptr) {
            throw runtime_error("Inconsistent sample type information");
        }
        if (SampleTimes.size() != TranslationSamples->Current.size()) { throw runtime_error("Inconsistent sample information"); }
        TranslationSamples->Verify();
        break;
    default:
        throw runtime_error("Unknown Sampler type");
    }
}
} // namespace ktn::Graphics
