﻿#include "ktnEngine/Graphics/ktnRenderer_Vulkan.hpp"

#include "internal/Vulkan/DefaultShaders.hpp"

#include <GLFW/glfw3.h>
#include <shaderc/shaderc.hpp>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <set>

using namespace std;

static const vector<const char *> validationLayers = {"VK_LAYER_KHRONOS_validation"};

static const vector<const char *> requiredPhysicalDeviceExtensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};

static const unsigned int MAX_NFRAMES = 3;

#ifdef KTN_DEBUG
static const bool validationLayersEnabled = true;

// Note that the last parameter, void *pUserData is not used, and therefore not named.
static VKAPI_ATTR VkBool32 VKAPI_CALL validationCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT severity, // TODO add description comments for the parameters
    VkDebugUtilsMessageTypeFlagsEXT type,
    const VkDebugUtilsMessengerCallbackDataEXT *pData,
    void *) {
    // A logger library should be used for this
    // for now everything is redirected to stdout
    if (severity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT) {
        cout << "Error: ";
    } else if (severity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {
        cout << "Warning: ";
    }

    //    else if(severity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT)
    //        cout << "Validation layers: Info: " << pData->pMessage << endl;
    //    else if(severity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT)
    //        cout << "Validation layers: Verbose: " << pData->pMessage << endl;
    else {
        return true;
    }; // no other case

    cout << "Type: " << type << ", Message: " << pData->pMessage << endl;
    return VK_FALSE;
}

#else
static const bool validationLayersEnabled = false;
#endif

namespace ktn::Graphics {
ktnRenderer_Vulkan::ktnRenderer_Vulkan() : IktnRenderer("Vulkan Renderer") {}

ktnRenderer_Vulkan::~ktnRenderer_Vulkan() = default;

ktnGraphicsBackend ktnRenderer_Vulkan::Backend() {
    return ktnGraphicsBackend::VULKAN;
}

void ktnRenderer_Vulkan::SetContext(void *eContext) {
    m_Window = (GLFWwindow *)eContext;
}
void ktnRenderer_Vulkan::SetShader(const std::string & /*name*/, ktnShaderProgram * /*shader*/) {
    // This function exists only to satisfy the interface of ktnRenderer
}

void ktnRenderer_Vulkan::Initialize(const vk::Extent2D &eExtent2D, unsigned int /*shadowMapSize*/) {
    m_Extent2D = eExtent2D;
    m_SwapchainExtent = m_Extent2D;

    try {
        CreateInstance();
        SetUpDebugCallback();
        CreateSurface();
        PickPhysicalDevice();
        CreateLogicalDevice();
        CreateSwapchain();
        CreateImageViews();
        CreateRenderPass();
        CreatePipeline();
        CreateFramebuffers();
        CreateCommandPool();
        CreateCommandBuffers();
        CreateSyncObjects();
    } catch (const exception &e) {
        cerr << e.what() << endl;
        throw e;
    }
}

void ktnRenderer_Vulkan::CreateInstance() {
    if (validationLayersEnabled) {
        if (CheckValidationLayer() == ExecutionStatus::FAILURE) { throw runtime_error("Could not find required validation layers."); }
    }
    WIP; // all parameters should be variables given by the user
    vk::ApplicationInfo appInfo(
        "Game", // TODO add description comments for the parameters
        VK_MAKE_VERSION(1, 0, 0),
        "KtnEngine",
        VK_MAKE_VERSION(1, 0, 0),
        VK_API_VERSION_1_0);

    auto extensions = GetRequiredExtentions();
    vk::InstanceCreateInfo ici(
        vk::InstanceCreateFlags(), // InstanceCreateFlags object
        &appInfo, // ApplicationInfo object
        validationLayersEnabled ? static_cast<uint32_t>(validationLayers.size()) : 0, // TODO add description comments for the parameters
        validationLayersEnabled ? validationLayers.data() : nullptr,
        static_cast<uint32_t>(extensions.size()),
        extensions.data());

    vk::Result result = vk::createInstance(
        &ici, // InstanceCreateInfo object
        nullptr, // TODO add description comments for the parameters
        &m_Instance);
    if (result != vk::Result::eSuccess) { throw runtime_error("ktnRenderer_Vulkan::CreateInstance: Could not create Vulkan instance."); }
}

void ktnRenderer_Vulkan::SetUpDebugCallback() {
#ifdef KTN_DEBUG
    // TODO using m_DebugUtilsMessenger causes crashes in Debug on Windows.
    VkDebugUtilsMessengerCreateInfoEXT dumci;
    dumci.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    dumci.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
                            VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    dumci.messageType =
        VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    dumci.pfnUserCallback = validationCallback;
    dumci.flags = 0;
    dumci.pNext = nullptr;

    PFN_vkCreateDebugUtilsMessengerEXT vkCreateDebugUtilsMessengerEXT =
        reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(m_Instance.getProcAddr("vkCreateDebugUtilsMessengerEXT"));

    vkCreateDebugUtilsMessengerEXT(
        m_Instance, // TODO add description comments for the parameters
        &dumci,
        nullptr,
        &m_DebugUtilsMessenger);
#endif
}

void ktnRenderer_Vulkan::CreateSurface() {
    if (glfwCreateWindowSurface(m_Instance, m_Window, nullptr, &m_Surface) != VK_SUCCESS) {
        throw runtime_error("ktnRenderer_Vulkan::CreateSurface: Could not create Vulkan surface");
    }
}

void ktnRenderer_Vulkan::PickPhysicalDevice() {
    vector<vk::PhysicalDevice> availableDevices = m_Instance.enumeratePhysicalDevices();
    if (availableDevices.empty()) { return; }

    bool devicePicked = false;
    for (auto device : availableDevices) {
        if (ExecutionStatus::OK == CheckPhysicalDevice(device)) {
            WIP;
            // instead of picking the first suitable device, we can iterate through a list of devices
            // and pick the ones with the best features.
            devicePicked = true;
            m_PhysicalDevice = device;
            break;
        }
    }
    if (!devicePicked) { throw runtime_error("ktnRenderer_Vulkan::PickPhysicalDevice: No suitable device found."); }
}

auto ktnRenderer_Vulkan::CreateLogicalDevice() -> ExecutionStatus {
    // create graphics and present queues
    if (!m_QueueFamilies.AreComplete()) { return ExecutionStatus::FAILURE; }

    vector<vk::DeviceQueueCreateInfo> dqcis;

    set<uint32_t> familiesIndexSet = {m_QueueFamilies.Graphics.Index().value(), m_QueueFamilies.Present.Index().value()};

    float priority = 1.0F;
    for (auto familyIndex : familiesIndexSet) {
        vk::DeviceQueueCreateInfo dqci = vk::DeviceQueueCreateInfo(vk::DeviceQueueCreateFlags(), dqci.queueFamilyIndex = familyIndex, 1, &priority);
        dqcis.push_back(dqci);
    }

    // for the time being we don't specify any neccessary physical device features
    vk::PhysicalDeviceFeatures pdf;

    vk::DeviceCreateInfo dci(
        vk::DeviceCreateFlags(), // DeviceCreateFlags object
        static_cast<uint32_t>(dqcis.size()), // DeviceQueueCreateInfo count
        dqcis.data(), // DeviceQueueCreateInfo objects
        validationLayersEnabled ? static_cast<uint32_t>(validationLayers.size()) : 0, // validation layer count
        validationLayersEnabled ? validationLayers.data() : nullptr, // validation layer names
        static_cast<uint32_t>(requiredPhysicalDeviceExtensions.size()), // device extension count
        requiredPhysicalDeviceExtensions.data(), // device extension names
        &pdf // PhysicalDeviceFeatures object
    );

    vk::Result r = m_PhysicalDevice.createDevice(
        &dci, // DeviceCreateInfo object
        nullptr, // allocation callback
        &m_LogicalDevice // Device object
    );
    if (vk::Result::eSuccess != r) { return ExecutionStatus::FAILURE; }

    m_GraphicsQueue = m_LogicalDevice.getQueue(m_QueueFamilies.Graphics.Index().value(), 0);
    m_PresentQueue = m_LogicalDevice.getQueue(m_QueueFamilies.Present.Index().value(), 0);
    return ExecutionStatus::OK;
}

auto ktnRenderer_Vulkan::CreateSwapchain() -> ExecutionStatus {
    vk::SurfaceFormatKHR format = ChooseSwapSurfaceFormat(m_SurfaceFormats);
    vk::PresentModeKHR mode = ChooseSwapchainPresentMode(m_SwapchainPresentModes);

    uint32_t nImages = m_SurfaceCapabilities.minImageCount + 1; // in case we want to use triple buffering
    // if maxImageCount == minImageCount then the above is obviously not possible and will produce error
    // 0 means there is no limits save for memory
    if (m_SurfaceCapabilities.maxImageCount > 0 && nImages > m_SurfaceCapabilities.maxImageCount) { nImages = m_SurfaceCapabilities.maxImageCount; }

    uint32_t queueFamilyIndices[2] = {m_QueueFamilies.Graphics.Index().value(), m_QueueFamilies.Present.Index().value()};

    vk::SwapchainCreateInfoKHR scik(
        vk::SwapchainCreateFlagsKHR(), // SwapchainCreateFlagsKHR object
        m_Surface, // VkSurfaceKHR object
        nImages, // number of images in the Swapchain
        format.format, // VkSurfaceFormatKHR object
        format.colorSpace, // the color space of that image format
        m_SwapchainExtent, // Extent2D object
        1,
        vk::ImageUsageFlags(vk::ImageUsageFlagBits::eColorAttachment),
        m_QueueFamilies.Graphics.Index() != m_QueueFamilies.Present.Index() ? vk::SharingMode::eConcurrent : vk::SharingMode::eExclusive,
        m_QueueFamilies.Graphics.Index() != m_QueueFamilies.Present.Index() ? 2 : 0,
        m_QueueFamilies.Graphics.Index() != m_QueueFamilies.Present.Index() ? queueFamilyIndices : nullptr,
        m_SurfaceCapabilities.currentTransform,
        vk::CompositeAlphaFlagBitsKHR::eOpaque,
        mode,
        VK_TRUE,
        vk::SwapchainKHR());

    m_Swapchain = m_LogicalDevice.createSwapchainKHR(scik);
    if (m_Swapchain == vk::SwapchainKHR()) { return ExecutionStatus::FAILURE; }

    m_SwapchainImages = m_LogicalDevice.getSwapchainImagesKHR(m_Swapchain);
    if (m_SwapchainImages.empty()) { return ExecutionStatus::FAILURE; }

    m_SwapchainImageFormat = format.format;

    return ExecutionStatus::OK;
}

auto ktnRenderer_Vulkan::CreateImageViews() -> ExecutionStatus {
    m_SwapchainImageViews.clear();
    for (auto m_SwapchainImage : m_SwapchainImages) {
        vk::ImageViewCreateInfo ivci(
            vk::ImageViewCreateFlags(), // TODO add description comments for the parameters
            m_SwapchainImage,
            vk::ImageViewType::e2D,
            m_SwapchainImageFormat,
            vk::ComponentMapping(),
            vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1));
        m_SwapchainImageViews.push_back(m_LogicalDevice.createImageView(ivci));
    }
    return ExecutionStatus::OK;
}

auto ktnRenderer_Vulkan::CreateRenderPass() -> ExecutionStatus {
    vk::AttachmentDescription ad(
        vk::AttachmentDescriptionFlags(), // TODO add description comments for the parameters
        m_SwapchainImageFormat,
        vk::SampleCountFlagBits::e1,
        vk::AttachmentLoadOp::eClear,
        vk::AttachmentStoreOp::eStore,
        vk::AttachmentLoadOp::eDontCare,
        vk::AttachmentStoreOp::eDontCare,
        vk::ImageLayout::eUndefined,
        vk::ImageLayout::ePresentSrcKHR);

    vk::AttachmentReference ar(0, vk::ImageLayout::eColorAttachmentOptimal);

    vk::SubpassDescription sd(
        vk::SubpassDescriptionFlags(), // TODO add description comments for the parameters
        vk::PipelineBindPoint::eGraphics,
        0,
        nullptr,
        1,
        &ar,
        nullptr,
        nullptr,
        0,
        nullptr);

    vk::SubpassDependency dependency(
        VK_SUBPASS_EXTERNAL, // TODO add description comments for the parameters
        0,
        vk::PipelineStageFlagBits::eColorAttachmentOutput,
        vk::PipelineStageFlagBits::eColorAttachmentOutput,
        vk::AccessFlags(),
        vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite);

    vk::RenderPassCreateInfo rpci(
        vk::RenderPassCreateFlags(), // TODO add description comments for the parameters
        1,
        &ad,
        1,
        &sd,
        1,
        &dependency);

    m_RenderPass = m_LogicalDevice.createRenderPass(rpci);

    return ExecutionStatus::OK;
}

auto ktnRenderer_Vulkan::CreatePipeline() -> ExecutionStatus {
    {
        WIP // we want to have flexibility in this. Right now everything is hardcoded to show a triangle
    }

    auto shaderSource_Fragment = Vulkan::DEFAULT_SHADER_DEFAULT_FRAG;
    auto shaderSource_Vertex = Vulkan::DEFAULT_SHADER_DEFAULT_VERT;

    shaderc::Compiler shaderCompiler;
    auto compileResultFrag = shaderCompiler.CompileGlslToSpv(shaderSource_Fragment, shaderc_fragment_shader, "default.frag");
    auto compileResultVert = shaderCompiler.CompileGlslToSpv(shaderSource_Vertex, shaderc_vertex_shader, "default.vert");

    if (compileResultFrag.GetCompilationStatus()) { throw runtime_error("Could not compile fragment shader."); };
    if (compileResultVert.GetCompilationStatus()) { throw runtime_error("Could not compile vertex shader."); };

    auto fragShaderModule = CreateShaderModule(vector<uint32_t>{compileResultFrag.begin(), compileResultFrag.end()});
    auto vertShaderModule = CreateShaderModule(vector<uint32_t>{compileResultVert.begin(), compileResultVert.end()});

    vk::PipelineShaderStageCreateInfo pssci_frag(
        vk::PipelineShaderStageCreateFlags(), // flags
        vk::ShaderStageFlagBits::eFragment, // stage
        fragShaderModule, // shader module
        "main" // entry point
    );

    vk::PipelineShaderStageCreateInfo pssci_vert(
        vk::PipelineShaderStageCreateFlags(), // flags
        vk::ShaderStageFlagBits::eVertex, // stage
        vertShaderModule, // shader module
        "main" // entry point
    );

    vector<vk::PipelineShaderStageCreateInfo> stages = {pssci_vert, pssci_frag};

    // start of fixed functions:
    auto pvisci = vk::PipelineVertexInputStateCreateInfo();
    {
        WIP // all default values because we hard coded the vertex in the shader
    }

    vk::PipelineInputAssemblyStateCreateInfo piasci(
        vk::PipelineInputAssemblyStateCreateFlags(), // TODO add description comments for the parameters
        vk::PrimitiveTopology::eTriangleList,
        VK_FALSE);

    vk::Viewport viewport(
        0.0F, // TODO add description comments for the parameters
        0.0F,
        static_cast<float>(m_SwapchainExtent.width),
        static_cast<float>(m_SwapchainExtent.height),
        0.0F,
        1.0F);

    vk::Rect2D scissor(vk::Offset2D(0, 0), m_SwapchainExtent);

    vk::PipelineViewportStateCreateInfo pvsci(
        vk::PipelineViewportStateCreateFlags(), // TODO add description comments for the parameters
        1,
        &viewport,
        1,
        &scissor);

    vk::PipelineRasterizationStateCreateInfo prsci(
        vk::PipelineRasterizationStateCreateFlags(), // TODO add description comments for the parameters
        VK_FALSE,
        VK_FALSE,
        vk::PolygonMode::eFill,
        vk::CullModeFlagBits::eBack,
        vk::FrontFace::eClockwise,
        VK_FALSE,
        0.0F,
        0.0F,
        0.0F,
        1.0F);

    vk::PipelineMultisampleStateCreateInfo pmsci(
        vk::PipelineMultisampleStateCreateFlags(), // TODO add description comments for the parameters
        vk::SampleCountFlagBits::e1,
        VK_FALSE,
        1.0F,
        nullptr,
        VK_FALSE,
        VK_FALSE);

    vk::PipelineColorBlendAttachmentState pcbas(
        VK_FALSE, // TODO add description comments for the parameters
        vk::BlendFactor::eOne,
        vk::BlendFactor::eZero,
        vk::BlendOp::eAdd,
        vk::BlendFactor::eOne,
        vk::BlendFactor::eZero,
        vk::BlendOp::eAdd,
        vk::ColorComponentFlags(
            vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA));

    vk::PipelineColorBlendStateCreateInfo pcbsci(
        vk::PipelineColorBlendStateCreateFlags(), // TODO add description comments for the parameters
        VK_FALSE,
        vk::LogicOp::eCopy,
        1,
        &pcbas,
        {{0.0F, 0.0F, 0.0F, 0.0F}});

    //    vk::DynamicState dynamicStates[] = {vk::DynamicState::eViewport,
    //                                        vk::DynamicState::eLineWidth};

    //    vk::PipelineDynamicStateCreateInfo(vk::PipelineDynamicStateCreateFlags(),
    //                                       2,
    //                                       dynamicStates);

    auto plci = vk::PipelineLayoutCreateInfo();
    m_PipelineLayout = m_LogicalDevice.createPipelineLayout(plci);

    // this part has weird line breaks because the IDE doesn't show hints after a certain amount of lines
    vk::GraphicsPipelineCreateInfo gpci(
        vk::PipelineCreateFlags(), // TODO add description comments for the parameters
        2,
        stages.data(),
        &pvisci,
        &piasci,
        nullptr,
        &pvsci,
        &prsci,
        &pmsci,
        nullptr,
        &pcbsci,
        nullptr,
        m_PipelineLayout,
        m_RenderPass,
        0,
        vk::Pipeline(),
        -1);
#if VK_HEADER_VERSION >= 148
    auto resultValue = m_LogicalDevice.createGraphicsPipeline(m_PipelineCache, gpci);
    if (resultValue.result == vk::Result::eSuccess) { m_Pipeline = resultValue.value; }
#else
    m_Pipeline = m_LogicalDevice.createGraphicsPipeline(m_PipelineCache, gpci);
#endif
    m_LogicalDevice.destroyShaderModule(vertShaderModule);
    m_LogicalDevice.destroyShaderModule(fragShaderModule);
    return ExecutionStatus::OK;
}

auto ktnRenderer_Vulkan::CreateFramebuffers() -> ExecutionStatus {
    m_SwapchainFramebuffers.clear();
    for (auto attachment : m_SwapchainImageViews) {
        vk::FramebufferCreateInfo fci(
            vk::FramebufferCreateFlags(), // TODO add description comments for the parameters
            m_RenderPass,
            1,
            &attachment,
            m_SwapchainExtent.width,
            m_SwapchainExtent.height,
            1);
        m_SwapchainFramebuffers.push_back(m_LogicalDevice.createFramebuffer(fci));
    }
    return ExecutionStatus::OK;
}

auto ktnRenderer_Vulkan::CreateCommandPool() -> ExecutionStatus {
    vk::CommandPoolCreateInfo cpci(
        vk::CommandPoolCreateFlags(), // TODO add description comments for the parameters
        m_QueueFamilies.Graphics.Index().value());
    m_CommandPool = m_LogicalDevice.createCommandPool(cpci);
    return ExecutionStatus::OK;
}

auto ktnRenderer_Vulkan::CreateCommandBuffers() -> ExecutionStatus {
    vk::CommandBufferAllocateInfo cbai(
        m_CommandPool, // TODO add description comments for the parameters
        vk::CommandBufferLevel::ePrimary,
        static_cast<uint32_t>(m_SwapchainFramebuffers.size()));
    m_CommandBuffers = m_LogicalDevice.allocateCommandBuffers(cbai);

    vk::ClearValue clearColor(array<float, 4>({0.0F, 0.0F, 0.0F, 1.0F}));

    for (size_t i = 0; i < m_CommandBuffers.size(); ++i) {
        vk::CommandBufferBeginInfo cbbi(vk::CommandBufferUsageFlagBits::eSimultaneousUse, nullptr);
        m_CommandBuffers[i].begin(cbbi);

        vk::RenderPassBeginInfo rpbi(m_RenderPass, m_SwapchainFramebuffers[i], vk::Rect2D(vk::Offset2D(0, 0), m_SwapchainExtent), 1, &clearColor);
        m_CommandBuffers[i].beginRenderPass(rpbi, vk::SubpassContents::eInline);
        m_CommandBuffers[i].bindPipeline(vk::PipelineBindPoint::eGraphics, m_Pipeline);
        m_CommandBuffers[i].draw(3, 1, 0, 0);
        m_CommandBuffers[i].endRenderPass();
        m_CommandBuffers[i].end();
    }

    return ExecutionStatus::OK;
}

auto ktnRenderer_Vulkan::CreateSyncObjects() -> ExecutionStatus {
    vk::SemaphoreCreateInfo sci;
    vk::FenceCreateInfo fci(vk::FenceCreateFlagBits::eSignaled);
    m_Semaphores_ImgAvailable.clear();
    m_Semaphores_RenderDone.clear();
    m_Fences.clear();
    for (unsigned int i = 0; i < MAX_NFRAMES; ++i) {
        m_Semaphores_ImgAvailable.push_back(m_LogicalDevice.createSemaphore(sci));
        m_Semaphores_RenderDone.push_back(m_LogicalDevice.createSemaphore(sci));
        m_Fences.push_back(m_LogicalDevice.createFence(fci));
    }
    return ExecutionStatus::OK;
}

void ktnRenderer_Vulkan::RecreateSwapchain() {
    m_LogicalDevice.waitIdle();

    CleanUpSwapchain();

    // create new Swapchain
    CreateSwapchain();
    CreateImageViews();
    CreateRenderPass();
    CreatePipeline();
    CreateFramebuffers();
    CreateCommandBuffers();
}

void ktnRenderer_Vulkan::Draw() {
    m_LogicalDevice.waitForFences(m_Fences[m_CurrentFrame], VK_TRUE, numeric_limits<uint64_t>::max());

    uint32_t index;

    auto result = m_LogicalDevice.acquireNextImageKHR(
        m_Swapchain, // TODO add description comments for the parameters
        numeric_limits<uint64_t>::max(),
        m_Semaphores_ImgAvailable[m_CurrentFrame],
        vk::Fence(),
        &index);

    if (result == vk::Result::eErrorOutOfDateKHR) {
        RecreateSwapchain();
        return;
    }
    if (result != vk::Result::eSuccess && result == vk::Result::eSuboptimalKHR) { throw std::runtime_error("Could not acquire next image from swapchain."); }

    vk::Semaphore waitSemaphores[] = {m_Semaphores_ImgAvailable[m_CurrentFrame]};
    vk::Semaphore signalSemaphores[] = {m_Semaphores_RenderDone[m_CurrentFrame]};
    vk::PipelineStageFlags waitStages[] = {vk::PipelineStageFlagBits::eColorAttachmentOutput};
    vk::SubmitInfo si(
        1,
        waitSemaphores, // TODO add description comments for the parameters
        waitStages,
        1,
        &m_CommandBuffers[index],
        1,
        signalSemaphores);
    m_LogicalDevice.resetFences(m_Fences[m_CurrentFrame]);
    m_GraphicsQueue.submit(1, &si, m_Fences[m_CurrentFrame]);

    vk::SwapchainKHR chains[] = {m_Swapchain};
    vk::PresentInfoKHR presentInfo(1, signalSemaphores, 1, chains, &index, nullptr);

    result = m_PresentQueue.presentKHR(&presentInfo);
    if (result == vk::Result::eErrorOutOfDateKHR || result == vk::Result::eSuboptimalKHR || m_FramebufferSizeChanged) {
        m_FramebufferSizeChanged = false;
        RecreateSwapchain();
    } else if (result != vk::Result::eSuccess) {
        throw std::runtime_error("Could not present swapchain image.");
    }

    m_CurrentFrame = (m_CurrentFrame + 1) % MAX_NFRAMES;

    m_PresentQueue.waitIdle();
}

auto ktnRenderer_Vulkan::CheckValidationLayer() -> ExecutionStatus {
    vector<vk::LayerProperties> availableLayers = vk::enumerateInstanceLayerProperties();

    // search for available validation layers, and see if all the layers defined in validationLayers are found
    for (const auto *layer : validationLayers) {
        bool layerAvailable = false;
        for (auto availableLayer : availableLayers) {
            // Comparing arrays of characters means comparing the values that the pointer points to
            if (*availableLayer.layerName == *layer) {
                layerAvailable = true;
                break;
            }
        }
        if (!layerAvailable) {
            cerr << "ktnRenderer_Vulkan::CheckValidationLayer: Validation layer " << layer << " not found" << endl;
            return ExecutionStatus::FAILURE;
        }
    }
    return ExecutionStatus::OK;
}

auto ktnRenderer_Vulkan::CheckQueueFamilies(vk::PhysicalDevice device) -> ExecutionStatus {
    vector<vk::QueueFamilyProperties> vQFP = device.getQueueFamilyProperties();
    if (vQFP.empty()) { return ExecutionStatus::FAILURE; }
    // Find graphics queue family
    for (size_t i = 0; i < vQFP.size(); ++i) {
        if (vQFP[i].queueCount > 0) {
            if (vQFP[i].queueFlags & vk::QueueFlagBits::eGraphics) {
                m_QueueFamilies.Graphics.SetIndex(static_cast<uint32_t>(i));
                m_QueueFamilies.Graphics.SetCount(vQFP[i].queueCount);
                break;
            }
        }
    }

    // Find present queue family
    for (size_t i = 0; i < vQFP.size(); ++i) {
        if (vQFP[i].queueCount > 0) {
            VkBool32 presentModeSupported = 1U;
            vkGetPhysicalDeviceSurfaceSupportKHR(device, i, m_Surface, &presentModeSupported);
            if (presentModeSupported != 0U) {
                m_QueueFamilies.Present.SetIndex(static_cast<uint32_t>(i));
                m_QueueFamilies.Present.SetCount(vQFP[i].queueCount);
                break;
            }
        }
    }

    if (m_QueueFamilies.AreComplete()) { return ExecutionStatus::OK; }
    return ExecutionStatus::FAILURE;
}

auto ktnRenderer_Vulkan::CheckPhysicalDevice(vk::PhysicalDevice device) -> ExecutionStatus {
    // Check for certain features/properties of the physical devices
    // For the time being it is not needed.
    //    vk::PhysicalDeviceFeatures devFeats = device.getFeatures();
    //    vk::PhysicalDeviceProperties devProps = device.getProperties();

    // Check for the queue families of the physical devices to make sure that we can submit commands to queues later on.
    if (ExecutionStatus::OK == CheckQueueFamilies(device)) {
        if (ExecutionStatus::OK == CheckPhysicalDeviceExtensionsSupport(device)) {
            if (ExecutionStatus::OK == CheckPhysicalDeviceSwapchainSupport(device)) { return ExecutionStatus::OK; }
        }
    }
    return ExecutionStatus::FAILURE;
}

auto ktnRenderer_Vulkan::CheckPhysicalDeviceExtensionsSupport(vk::PhysicalDevice device) -> ExecutionStatus {
    vector<vk::ExtensionProperties> availableExtensions = device.enumerateDeviceExtensionProperties();
    set<string> requiredExtensions(requiredPhysicalDeviceExtensions.begin(), requiredPhysicalDeviceExtensions.end());

    for (auto extension : availableExtensions) { requiredExtensions.erase(static_cast<char *>(extension.extensionName)); }

    if (requiredExtensions.empty()) { return ExecutionStatus::OK; }
    return ExecutionStatus::FAILURE;
}

auto ktnRenderer_Vulkan::CheckPhysicalDeviceSwapchainSupport(vk::PhysicalDevice device) -> ExecutionStatus {
    // get surface capabilities
    m_SurfaceCapabilities = device.getSurfaceCapabilitiesKHR(m_Surface);
    if (m_SurfaceCapabilities == vk::SurfaceCapabilitiesKHR()) { return ExecutionStatus::FAILURE; }

    // get surface formats
    m_SurfaceFormats = device.getSurfaceFormatsKHR(m_Surface);
    if (m_SurfaceFormats.empty()) { return ExecutionStatus::FAILURE; }
    // get surface present modes
    m_SwapchainPresentModes = device.getSurfacePresentModesKHR(m_Surface);
    if (m_SwapchainPresentModes.empty()) { return ExecutionStatus::FAILURE; }
    return ExecutionStatus::OK;
}

auto ktnRenderer_Vulkan::ChooseSwapSurfaceFormat(const vector<vk::SurfaceFormatKHR> &formats) -> vk::SurfaceFormatKHR {
    vk::SurfaceFormatKHR preferedFormat{};
    preferedFormat.format = vk::Format::eB8G8R8A8Unorm;
    preferedFormat.colorSpace = vk::ColorSpaceKHR::eSrgbNonlinear;

    // best case: surface has no prefered format, take our preferedFormat
    if (formats.size() == 1 && formats[0].format == vk::Format::eUndefined) { return preferedFormat; }
    // otherwise, see if there's any match to our prefered format;
    for (auto const &format : formats) {
        if (format.format == vk::Format::eB8G8R8A8Unorm //
            && format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear) {
            return preferedFormat;
        }
    }
    // otherwise, find way to settle with the "best matched" format
    { WIP } // settle for the first supported format in the mean time
    return formats[0];
}

auto ktnRenderer_Vulkan::ChooseSwapchainPresentMode(const vector<vk::PresentModeKHR> &modes) -> vk::PresentModeKHR {
    { WIP }
    // The individual application should be able to set this instead of letting the engine choose automatically.

    // Triple buffering if possible
    for (auto const &mode : modes) {
        if (mode == vk::PresentModeKHR::eMailbox) { return mode; }
    }
    // Otherwise double buffering
    for (auto const &mode : modes) {
        if (mode == vk::PresentModeKHR::eFifo) { return mode; }
    }
    // Otherwise relaxed double buffering, may introduce some tearing when an image is displayed too long
    for (auto const &mode : modes) {
        if (mode == vk::PresentModeKHR::eFifoRelaxed) { return mode; }
    }
    // Otherwise no v-sync whatsoever
    return vk::PresentModeKHR::eImmediate;
}

auto ktnRenderer_Vulkan::CreateShaderModule(const vector<char> &buffer) -> vk::ShaderModule {
    vk::ShaderModuleCreateInfo smci(
        vk::ShaderModuleCreateFlags(), // flags
        buffer.size(), // code size in bytes
        reinterpret_cast<const uint32_t *>(buffer.data()) // code
    );
    return m_LogicalDevice.createShaderModule(smci);
}

auto ktnRenderer_Vulkan::CreateShaderModule(const vector<uint32_t> &buffer) -> vk::ShaderModule {
    vk::ShaderModuleCreateInfo smci(
        vk::ShaderModuleCreateFlags(), // flags
        buffer.size() * 4, // code size in bytes
        buffer.data() // code
    );
    return m_LogicalDevice.createShaderModule(smci);
}

auto ktnRenderer_Vulkan::GetRequiredExtentions() -> vector<const char *> {
    uint32_t nGlfwExtensions = 0;
    const char **glfwExtensions = glfwGetRequiredInstanceExtensions(&nGlfwExtensions);
    vector<const char *> extensions(glfwExtensions, glfwExtensions + nGlfwExtensions);

#ifdef KTN_DEBUG
    extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif
    return extensions;
}

auto ktnRenderer_Vulkan::GetShaderFromFile(const string &filepath) -> vector<char> {
    // read file as binary from the end of file
    // ate is there so that we can allocate a buffer with the right size
    ifstream file(filepath, ios::ate | ios::binary);
    if (!file.is_open()) { throw runtime_error("ktnRenderer_Vulkan::GetShaderFromFile: Could not open " + filepath); }
    int64_t howBig = file.tellg();
    vector<char> buffer(static_cast<size_t>(howBig));
    file.seekg(0);
    file.read(buffer.data(), howBig);
    file.close();
    return buffer;
}

void ktnRenderer_Vulkan::RenderFrame(ktnGraphicsScenePtr /*scene*/) {
    Draw();
    //    cout << "swap chain extent: " << m_SwapchainExtent.width << ", " << m_SwapchainExtent.height << endl;
}

void ktnRenderer_Vulkan::WaitIdle() {
    m_LogicalDevice.waitIdle();
}

void ktnRenderer_Vulkan::CleanUp() {
    CleanUpSwapchain();
    for (unsigned int i = 0; i < MAX_NFRAMES; ++i) {
        m_LogicalDevice.destroyFence(m_Fences[i]);
        m_LogicalDevice.destroySemaphore(m_Semaphores_ImgAvailable[i]);
        m_LogicalDevice.destroySemaphore(m_Semaphores_RenderDone[i]);
    }
    m_LogicalDevice.destroyCommandPool(m_CommandPool);
    m_LogicalDevice.destroy();
#ifdef KTN_DEBUG
    // dynamic loader for vkDestroyDebugUtilsMessengerEXT, since it's not a core functionality of vulkan
    PFN_vkDestroyDebugUtilsMessengerEXT vkDestroyDebugUtilsMessengerEXT =
        reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(m_Instance.getProcAddr("vkDestroyDebugUtilsMessengerEXT"));
    vkDestroyDebugUtilsMessengerEXT(m_Instance, m_DebugUtilsMessenger, nullptr);
#endif

    m_Instance.destroySurfaceKHR(m_Surface);
    m_Instance.destroy();
}

void ktnRenderer_Vulkan::CleanUpSwapchain() {
    for (auto &fb : m_SwapchainFramebuffers) { m_LogicalDevice.destroyFramebuffer(fb); }

    m_LogicalDevice.freeCommandBuffers(m_CommandPool, m_CommandBuffers.size(), m_CommandBuffers.data());
    m_LogicalDevice.destroyPipeline(m_Pipeline);
    m_LogicalDevice.destroyPipelineLayout(m_PipelineLayout);
    m_LogicalDevice.destroyRenderPass(m_RenderPass);

    for (auto &sciv : m_SwapchainImageViews) { m_LogicalDevice.destroyImageView(sciv); }
    m_LogicalDevice.destroySwapchainKHR(m_Swapchain);
}

void ktnRenderer_Vulkan::HandleEvent_FramebufferSizeChanged(int width, int height) {
    m_FramebufferSizeChanged = true;
    m_SwapchainExtent.width = width;
    m_SwapchainExtent.height = height;
}
} // namespace ktn::Graphics
