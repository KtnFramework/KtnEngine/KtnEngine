#include <ktnEngine/Graphics/ktnJoint.hpp>

namespace ktn::Graphics {
using namespace glm;

ktnJoint::ktnJoint(const std::string &eName, ktn3DElement *eParent) : ktn3DElement(eName, eParent) {}

void ktnJoint::UpdateAnimatedWorldSpaceModelMatrix() {
    if (nullptr == m_Parent) {
        m_AnimatedMatrix_WS = m_AnimatedMatrix_LS * m_Transform_WS.ModelMatrix;
    } else {
        // matrix multiplication back to front
        m_AnimatedMatrix_WS = IDENTITY_MATRIX_4;

        // at the end, transform based on animated model matrix of parent
        m_AnimatedMatrix_WS *= m_Parent->AnimatedModelMatrix(WorldSpace);
        m_AnimatedMatrix_WS *= m_AnimatedMatrix_LS;
    }
    for (auto &child : m_Children) { //
        child->UpdateAnimatedWorldSpaceModelMatrix();
    }

    m_AnimatedMatrix_WS *= m_InverseBind;
}

void ktnJoint::SetInverseBindMatrix(const glm::mat4 &eInverseBindMatrix) {
    m_InverseBind = eInverseBindMatrix;
}
} // namespace ktn::Graphics
