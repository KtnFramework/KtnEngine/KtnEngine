#include <ktnEngine/Graphics/ktnMesh.hpp>

namespace ktn::Graphics {
ktnMesh::ktnMesh(const std::string &eName, ktn3DElement *eParent) : ktn3DElement(eName, eParent) {}

ktnMesh::~ktnMesh() = default;
} // namespace ktn::Graphics
