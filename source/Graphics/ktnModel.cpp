#include "ktnEngine/Graphics/ktnModel.hpp"

#include <ktnEngine/Core/ktnCoreUtils.hpp>
#include <ktnEngine/Graphics/ktnModelLoader_glTF.hpp>
#include <ktnEngine/Physics/ktnPhysicsUtils.hpp>

#include <chrono/physics/ChBody.h>
#include <chrono/physics/ChBodyEasy.h>
#include <chrono/physics/ChMaterialSurfaceNSC.h>

namespace ktn::Graphics {
using namespace glm;
using namespace Core::Utils;
using namespace Physics;
using namespace std;

ktnBoundingBoxMeasurements operator*(ktnBoundingBoxMeasurements eMeasurement, glm::vec3 eScale) {
    ktnBoundingBoxMeasurements temp;
    temp.Max.x = eMeasurement.Max.x * eScale.x;
    temp.Max.y = eMeasurement.Max.y * eScale.y;
    temp.Max.z = eMeasurement.Max.z * eScale.z;

    temp.Median.x = eMeasurement.Median.x * eScale.x;
    temp.Median.y = eMeasurement.Median.y * eScale.y;
    temp.Median.z = eMeasurement.Median.z * eScale.z;

    temp.Min.x = eMeasurement.Min.x * eScale.x;
    temp.Min.y = eMeasurement.Min.y * eScale.y;
    temp.Min.z = eMeasurement.Min.z * eScale.z;
    return temp;
}

ktnModel::ktnModel(const string &ePath, const string &eName) : ktn3DElement(eName, nullptr) {
    Path = ePath;
    LoadFromDisk();
}

ktnModel::~ktnModel() {
    for (auto &[index, mesh] : Meshes) { delete mesh; }
    for (auto &[index, skin] : Skins) { delete skin; }
}

auto ktnModel::operator==(const ktnModel &eModel) -> bool {
    return (ktn3DElement::operator==(eModel) && Meshes == eModel.Meshes && m_IsLoadedToGPU == eModel.m_IsLoadedToGPU && Path == eModel.Path);
}

auto ktnModel::operator!=(const ktnModel &eModel) -> bool {
    return !operator==(eModel);
}

auto ktnModel::BoundingBox() -> ktnBoundingBoxMeasurements {
    return m_BoundingBox * Scale(WorldSpace);
}

void ktnModel::StartAction(std::string eActionName) {
    bool actionAlreadyPlaying = false;
    for (auto &action : CurrentActions) {
        if (eActionName == action->Name) {
            if (!action->IsPlaying()) { action->Start(); }
            actionAlreadyPlaying = true;
            break;
        }
    }

    if (!actionAlreadyPlaying) {
        for (auto &action : Actions) {
            if (eActionName == action.second->Name) {
                action.second->Start();
                CurrentActions.emplace_back(action.second);
                return;
            }
        }
        throw runtime_error("There is no action with the name \"" + eActionName + "\".");
    }
}

void ktnModel::StopAction(std::string eActionName) {
    for (auto &action : CurrentActions) {
        if (eActionName == action->Name) {
            action->Stop();
            break;
        }
    }
}

void ktnModel::InitializePhysicalObject(float density, bool shouldMove) {
    PhysicalObject = make_shared<ktnPhysicalObject>();
    vec3 origin = m_Transform_WS.Position;
    quat rotation = m_Transform_WS.Rotation;
    vec3 boundingBoxDims = BoundingBox().Max - BoundingBox().Min;
    auto body = chrono_types::make_shared<::chrono::ChBodyEasyBox>( //
        boundingBoxDims.x,
        boundingBoxDims.y,
        boundingBoxDims.z,
        density,
        chrono_types::make_shared<::chrono::ChMaterialSurfaceNSC>(),
        ::chrono::collision::ChCollisionSystemType::BULLET);

    body->SetBodyFixed(!shouldMove);

    ::chrono::ChCoordsys coords;
    body->SetRot(Physics::Utils::QuatConvert_glm2ch(rotation));
    body->SetPos(Physics::Utils::Vec3Convert_glm2ch(origin));

    PhysicalObject->RigidBody = make_shared<ktnRigidBody>();
    PhysicalObject->RigidBody->Body = body;

    PhysicalObject->RigidBody->PositionUpdated.Connect(this, &ktn3DElement::SetWorldSpacePosition);
    PhysicalObject->RigidBody->RotationUpdated.Connect(this, &ktn3DElement::SetWorldSpaceRotation);
}

void ktnModel::LoadFromDisk() {
    if (Path.empty()) { throw invalid_argument(string(__FUNCTION__) + ": Path is empty."); }
    auto gltf = make_shared<gleb::glTF>(Path.string());
    gltf->read();

    shared_ptr<IktnModelLoader> loader;

    if (filesystem::path(Path).extension() == ".gltf") {
        auto *temp = new ktnModelLoader_glTF();
        temp->Initialize(gltf);
        loader.reset(temp);
    } else {
        throw runtime_error(string(__FUNCTION__) + ": Unsupported model file extension: " + filesystem::path(Path).extension().string());
    }

    Skins = loader->LoadSkins();
    Meshes = loader->LoadMeshes(this);
    Actions = loader->LoadActions();

    // assign skins to meshes
    for (auto &scene : gltf->scenes) {
        for (auto &index_node : scene.nodes) {
            if (gltf->nodes[index_node].mesh.has_value() && gltf->nodes[index_node].skin.has_value()) {
                Meshes[gltf->nodes[index_node].mesh.value()]->Skin = Skins[gltf->nodes[index_node].skin.value()];
            }
        }
    }

    // assign target to mesh/joint
    for (size_t i = 0; i < gltf->animations.size(); ++i) {
        auto gltfAnimation = gltf->animations[i];
        auto action = Actions.at(i);

        for (size_t index_channel = 0; index_channel < gltfAnimation.channels.size(); ++index_channel) {
            auto index_node = gltfAnimation.channels.at(index_channel).target.node.value();

            if (gltf->nodes.at(index_node).mesh.has_value()) {
                auto index_mesh = gltf->nodes.at(index_node).mesh.value();
                action->Channels.at(index_channel).Target = Meshes[index_mesh];
                Meshes[index_mesh]->AnimatedInfluences.insert({action->Name, Core::ktnPosRotScaleInfluence()});
            } else {
                ktn3DElement *target = nullptr;
                for (auto &skin : Skins) {
                    for (auto &joint : skin.second->Joints) {
                        if (index_node == joint.first) { //
                            target = joint.second;
                            // TODO: break here?
                        }
                    }
                }
                if (target != nullptr) {
                    action->Channels.at(index_channel).Target = target;
                    target->AnimatedInfluences.insert({action->Name, Core::ktnPosRotScaleInfluence()});
                }
            }
        }
    }

    // calculate bounding box
    WIP; // candidate for branchless, if performance becomes an issue
    for (auto &[index, mesh] : Meshes) {
        for (auto &pos : mesh->Positions) {
            if (pos.x > m_BoundingBox.Max.x) { m_BoundingBox.Max.x = pos.x; }
            if (pos.y > m_BoundingBox.Max.y) { m_BoundingBox.Max.y = pos.y; }
            if (pos.z > m_BoundingBox.Max.z) { m_BoundingBox.Max.z = pos.z; }

            if (pos.x < m_BoundingBox.Min.x) { m_BoundingBox.Min.x = pos.x; }
            if (pos.y < m_BoundingBox.Min.y) { m_BoundingBox.Min.y = pos.y; }
            if (pos.z < m_BoundingBox.Min.z) { m_BoundingBox.Min.z = pos.z; }
        }
        m_BoundingBox.Median = (m_BoundingBox.Max + m_BoundingBox.Min) / 2.0F;
    }
}
} // namespace ktn::Graphics
