#include "ktnEngine/Graphics/ktnRenderer_OpenGL_ImGui.hpp"

#include <imgui/imgui.h>

#include <imgui/backends/imgui_impl_opengl3.h>

#include <imgui/backends/imgui_impl_glfw.h>

namespace ktn::Graphics {
using namespace std;

ktnRenderer_OpenGL_ImGui::ktnRenderer_OpenGL_ImGui() {}

void ktnRenderer_OpenGL_ImGui::SetContext(void *eContext) {
    m_Window = (GLFWwindow *)eContext;
}

void ktnRenderer_OpenGL_ImGui::Initialize(const vk::Extent2D &eExtent2D, unsigned int shadowMapSize) {
    ktnRenderer_OpenGL::Initialize(eExtent2D, shadowMapSize);
    ImGui::CreateContext();

    ImGui_ImplGlfw_InitForOpenGL(m_Window, true);
    ImGui_ImplOpenGL3_Init("#version 130");
}

void ktnRenderer_OpenGL_ImGui::RenderGUI(const ktnGraphicsScenePtr scene) {
    bool p_open = true;
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
    const float DISTANCE = 10.0F;
    static int corner = 3;
    ImGuiIO &io = ImGui::GetIO();

    ImVec2 window_pos = ImVec2((corner & 1) != 0 ? io.DisplaySize.x - DISTANCE : DISTANCE, (corner & 2) != 0 ? io.DisplaySize.y - DISTANCE : DISTANCE);
    ImVec2 window_pos_pivot = ImVec2((corner & 1) != 0 ? 1.0F : 0.0F, (corner & 2) != 0 ? 1.0F : 0.0F);
    ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);

    constexpr float opacity(0.2F);
    ImGui::SetNextWindowBgAlpha(opacity);
    if (ImGui::Begin(
            "FPS overlay",
            &p_open,
            ImGuiWindowFlags_NoMove //
                | ImGuiWindowFlags_NoDecoration //
                | ImGuiWindowFlags_AlwaysAutoResize //
                | ImGuiWindowFlags_NoSavedSettings //
                | ImGuiWindowFlags_NoFocusOnAppearing //
                | ImGuiWindowFlags_NoNav)) {

        constexpr double MS_IN_S(1000.0); // number of milliseconds in a second
        ImGui::Text(
            "Average frametime: %.3f ms/frame (%.1f FPS)",
            MS_IN_S / static_cast<double>(ImGui::GetIO().Framerate),
            static_cast<double>(ImGui::GetIO().Framerate));
    }

    ImGui::End();
    ImGui::Render();
    int display_w;
    int display_h;
    glfwGetFramebufferSize(m_Window, &display_w, &display_h);
    glViewport(0, 0, display_w, display_h);
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}
} // namespace ktn::Graphics
