#include <ktnEngine/Graphics/ktnSkin.hpp>

using namespace std;

namespace ktn::Graphics {
ktnSkin::ktnSkin(const std::string &eName, ktn3DElement *eParent) : ktn3DElement(eName, eParent) {}
ktnSkin::~ktnSkin() {
    for (auto &[index, joint] : Joints) { delete joint; }
}
} // namespace ktn::Graphics
