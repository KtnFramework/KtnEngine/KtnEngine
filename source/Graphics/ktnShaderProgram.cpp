﻿#include <ktnEngine/Graphics/ktnShaderProgram.hpp>

#include <epoxy/gl.h>

#include <fstream>
#include <iostream>
#include <sstream>

namespace ktn::Graphics {
using namespace glm;
using namespace std;

constexpr int messageLength(1024);

ktnShaderProgram::ktnShaderProgram(
    const filesystem::path &VertexShaderPath, //
    const filesystem::path &FragmentShaderPath,
    const filesystem::path &GeometryShaderPath) {

    ProgramID = glCreateProgram();
    WIP;
    // assigning a certain pass should be done in the constructor, here we manipulate it later, and set the default
    // pass to color
    Pass = ShaderPass::COLOR;
    SetShader(GetShaderCode(VertexShaderPath.string()), ShaderType::VERTEX);
    SetShader(GetShaderCode(FragmentShaderPath.string()), ShaderType::FRAGMENT);
    if (!GeometryShaderPath.empty()) { SetShader(GeometryShaderPath.string(), ShaderType::GEOMETRY); }

    glLinkProgram(ProgramID);
    CheckLinkErrors(ProgramID);
    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);
    if (!GeometryShaderPath.empty()) { glDeleteShader(GeometryShaderID); }
}

ktnShaderProgram::ktnShaderProgram( //
    const string &eVertexShaderSource,
    const string &eFragmentShaderSource) {
    Pass = ShaderPass::COLOR;

    ProgramID = glCreateProgram();
    Pass = ShaderPass::COLOR;
    SetShader(eVertexShaderSource, ShaderType::VERTEX);
    SetShader(eFragmentShaderSource, ShaderType::FRAGMENT);

    glLinkProgram(ProgramID);
    CheckLinkErrors(ProgramID);
    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);
}

void ktnShaderProgram::CheckCompileErrors(GLuint ShaderID, ShaderType type) {
    GLint success;
    GLchar log[messageLength];
    glGetShaderiv(ShaderID, GL_COMPILE_STATUS, &success);
    if (success == 0) {
        glGetShaderInfoLog(ShaderID, messageLength, nullptr, log);
        cerr << "Error compiling " << ShaderTypeString[type].c_str() << " shader." << endl << log << endl;
    } else {
        glGetShaderInfoLog(ShaderID, messageLength, nullptr, log);
    }
}

void ktnShaderProgram::CheckLinkErrors(GLuint ProgramID) {
    GLint success;
    constexpr int messageLength(1024);
    GLchar log[messageLength];
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &success);
    if (success == 0) {
        glGetProgramInfoLog(ProgramID, messageLength, nullptr, log);
        throw runtime_error("Error linking shader program: " + string(log));
    }
}

void ktnShaderProgram::Update() {
    // needs testing to see if we can update shaders on the fly
}

void ktnShaderProgram::SetShader(const string &eSource, const ShaderType &type) {
    GLuint ID = CompileShader(eSource, type);
    switch (type) {
    case ShaderType::VERTEX:
        VertexShaderID = ID;
        CheckCompileErrors(VertexShaderID, ShaderType::VERTEX);
        glAttachShader(ProgramID, VertexShaderID);
        break;
    case ShaderType::FRAGMENT:
        FragmentShaderID = ID;
        CheckCompileErrors(VertexShaderID, ShaderType::FRAGMENT);
        glAttachShader(ProgramID, FragmentShaderID);
        break;
    case ShaderType::GEOMETRY:
        GeometryShaderID = ID;
        CheckCompileErrors(VertexShaderID, ShaderType::GEOMETRY);
        glAttachShader(ProgramID, GeometryShaderID);
        break;
    }
}

string ktnShaderProgram::GetShaderCode(const string &Path) {
    ifstream file;
    file.open(Path.c_str());
    if (!file) { throw runtime_error("Cannot open shader file: " + Path); }
    stringstream stream;
    stream << file.rdbuf();
    return stream.str();
}

auto ktnShaderProgram::CompileShader(const std::string &source, const ShaderType &type) -> unsigned int {
    GLuint shaderID;
    switch (type) {
    case ShaderType::VERTEX:
        shaderID = glCreateShader(GL_VERTEX_SHADER);
        break;
    case ShaderType::FRAGMENT:
        shaderID = glCreateShader(GL_FRAGMENT_SHADER);
        break;
    case ShaderType::GEOMETRY:
        shaderID = glCreateShader(GL_GEOMETRY_SHADER);
        break;
    default:
        throw runtime_error(std::string(__FUNCTION__) + ": Unknown shader type.");
    }
    const char *RawShaderCode = source.c_str();
    glShaderSource(shaderID, 1, &RawShaderCode, nullptr);
    glCompileShader(shaderID);
    return shaderID;
}

void ktnShaderProgram::Use() const {
    glUseProgram(ProgramID);
}

//-----Set Uniform Bool-----//
void ktnShaderProgram::SetUniformBool(const string &name, const bool &value) const {
    glUniform1i(glGetUniformLocation(ProgramID, name.c_str()), static_cast<GLint>(value));
}
void ktnShaderProgram::SetUniformBool(const string &name, const bool &&value) const {
    glUniform1i(glGetUniformLocation(ProgramID, name.c_str()), static_cast<GLint>(value));
}

//-----Set Uniform Float-----//
void ktnShaderProgram::SetUniformFloat(const string &name, const float &value) const {
    glUniform1f(glGetUniformLocation(ProgramID, name.c_str()), value);
}
void ktnShaderProgram::SetUniformFloat(const string &name, const float &&value) const {
    glUniform1f(glGetUniformLocation(ProgramID, name.c_str()), value);
}

//-----Set Uniform Int-----//
void ktnShaderProgram::SetUniformInt(const string &name, const int &value) const {
    glUniform1i(glGetUniformLocation(ProgramID, name.c_str()), value);
}
void ktnShaderProgram::SetUniformInt(const string &name, const int &&value) const {
    glUniform1i(glGetUniformLocation(ProgramID, name.c_str()), value);
}

//-----Set Uniform Mat2-----//
void ktnShaderProgram::SetUniformMat2(const string &name, const mat2 &mat) const {
    glUniformMatrix2fv(glGetUniformLocation(ProgramID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}
void ktnShaderProgram::SetUniformMat2(const string &name, const mat2 &&mat) const {
    glUniformMatrix2fv(glGetUniformLocation(ProgramID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

//-----Set Uniform Mat3-----//
void ktnShaderProgram::SetUniformMat3(const string &name, const mat3 &mat) const {
    glUniformMatrix3fv(glGetUniformLocation(ProgramID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}
void ktnShaderProgram::SetUniformMat3(const string &name, const mat3 &&mat) const {
    glUniformMatrix3fv(glGetUniformLocation(ProgramID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

//-----Set Uniform Mat4-----//
void ktnShaderProgram::SetUniformMat4(const string &name, const mat4 &mat) const {
    glUniformMatrix4fv(glGetUniformLocation(ProgramID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}
void ktnShaderProgram::SetUniformMat4(const string &name, const mat4 &&mat) const {
    glUniformMatrix4fv(glGetUniformLocation(ProgramID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

//-----Set Uniform Vec2-----//
void ktnShaderProgram::SetUniformVec2(const string &name, const vec2 &vec) const {
    glUniform2fv(glGetUniformLocation(ProgramID, name.c_str()), 1, &vec[0]);
}
void ktnShaderProgram::SetUniformVec2(const string &name, const vec2 &&vec) const {
    glUniform2fv(glGetUniformLocation(ProgramID, name.c_str()), 1, &vec[0]);
}
void ktnShaderProgram::SetUniformVec2(const string &name, const float &x, const float &y) const {
    glUniform2f(glGetUniformLocation(ProgramID, name.c_str()), x, y);
}
void ktnShaderProgram::SetUniformVec2(const string &name, const float &&x, const float &&y) const {
    glUniform2f(glGetUniformLocation(ProgramID, name.c_str()), x, y);
}

//-----Set Uniform Vec3-----//
void ktnShaderProgram::SetUniformVec3(const string &name, const vec3 &vec) const {
    glUniform3fv(glGetUniformLocation(ProgramID, name.c_str()), 1, &vec[0]);
}
void ktnShaderProgram::SetUniformVec3(const string &name, const vec3 &&vec) const {
    glUniform3fv(glGetUniformLocation(ProgramID, name.c_str()), 1, &vec[0]);
}
void ktnShaderProgram::SetUniformVec3(const string &name, const float &x, const float &y, const float &z) const {
    glUniform3f(glGetUniformLocation(ProgramID, name.c_str()), x, y, z);
}
void ktnShaderProgram::SetUniformVec3(const string &name, const float &&x, const float &&y, const float &&z) const {
    glUniform3f(glGetUniformLocation(ProgramID, name.c_str()), x, y, z);
}

//-----Set Uniform Vec4-----//
void ktnShaderProgram::SetUniformVec4(const string &name, const vec4 &vec) const {
    glUniform4fv(glGetUniformLocation(ProgramID, name.c_str()), 1, &vec[0]);
}
void ktnShaderProgram::SetUniformVec4(const string &name, const vec4 &&vec) const {
    glUniform4fv(glGetUniformLocation(ProgramID, name.c_str()), 1, &vec[0]);
}
void ktnShaderProgram::SetUniformVec4(const string &name, const float &x, const float &y, const float &z, const float &w) const {
    glUniform4f(glGetUniformLocation(ProgramID, name.c_str()), x, y, z, w);
}
void ktnShaderProgram::SetUniformVec4(const string &name, const float &&x, const float &&y, const float &&z, const float &&w) const {
    glUniform4f(glGetUniformLocation(ProgramID, name.c_str()), x, y, z, w);
}
} // namespace ktn::Graphics
