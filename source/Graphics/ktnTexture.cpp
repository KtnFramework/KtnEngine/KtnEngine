#include "ktnEngine/Graphics/ktnTexture.hpp"

#include <epoxy/gl.h>

#include <iostream>

using namespace std;

namespace ktn::Graphics {
ktnTexture::ktnTexture(const vk::Extent2D &eExtent2D) {
    m_Path = "";
    m_Extent2D = eExtent2D;
}

ktnTexture::ktnTexture(const filesystem::path &path) {
    m_Path = path;
}

ktnTexture::ktnTexture(ktnTexture &&other) {
    m_Path = other.m_Path;
    other.m_Path = "";

    // probably don't have to reset the texture type of other
    m_Type = other.m_Type;

    ID = other.ID;
    other.ID = 0;

    m_Extent2D = other.m_Extent2D;
    other.m_Extent2D = vk::Extent2D();

    m_nChannels = other.m_nChannels;
    other.m_nChannels = 0;
}

ktnTexture::~ktnTexture() {
    glDeleteTextures(1, &ID);
}

auto ktnTexture::operator==(const ktnTexture &eTexture) -> bool {
    return m_Path == eTexture.m_Path //
           && m_Type == eTexture.m_Type //
           && ID == eTexture.ID //
           && m_Extent2D == eTexture.m_Extent2D //
           && m_nChannels == eTexture.m_nChannels;
}

auto ktnTexture::operator!=(const ktnTexture &eTexture) -> bool {
    return !operator==(eTexture);
}

void ktnTexture::CreateOnGPU(const TextureType &eType) {
    m_Type = eType;
    glGenTextures(1, &ID);
    glBindTexture(GL_TEXTURE_2D, ID);

    switch (m_Type) {
    case TextureType::DIFFUSE:
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, m_Extent2D.width, m_Extent2D.height, 0, GL_RGB, GL_FLOAT, nullptr);
        SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        break;
    case TextureType::DEPTH: {
        glTexImage2D( //
            GL_TEXTURE_2D,
            0,
            GL_DEPTH_COMPONENT,
            static_cast<GLsizei>(m_Extent2D.width),
            static_cast<GLsizei>(m_Extent2D.height),
            0,
            GL_DEPTH_COMPONENT,
            GL_FLOAT,
            nullptr);
        // set texture wrapping parameters
        SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

        /* For shadow mapping, we want the value outside of the border to be white
         * so that when shadow is mapped on to the final image,
         * the pixels that map to outside the light frustum will not have a black shadow
         * This does not account for the pseudoshadow caused by pixels that are out of the far plane
         * which we have to address inside the fragment shader of the combined pass
         */
        std::vector<float> bordercolor = {1.0, 1.0, 1.0, 1.0};
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, bordercolor.data());
    } break;
    case TextureType::G_BUFFER_DIFFUSE:
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, m_Extent2D.width, m_Extent2D.height, 0, GL_RGB, GL_FLOAT, nullptr);
        break;
    case TextureType::G_BUFFER_NORMAL:
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, m_Extent2D.width, m_Extent2D.height, 0, GL_RGB, GL_FLOAT, nullptr);
        break;
    case TextureType::G_BUFFER_POSITION:
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, m_Extent2D.width, m_Extent2D.height, 0, GL_RGB, GL_FLOAT, nullptr);
        break;
    case TextureType::G_BUFFER_TEXCOORDS:
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RG16F, m_Extent2D.width, m_Extent2D.height, 0, GL_RG, GL_FLOAT, nullptr);
        break;
    default:
        throw;
    }

    // set texture filtering parameters
    SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glBindTexture(GL_TEXTURE_2D, 0);
}

void ktnTexture::SetTextureParameter(unsigned int target, unsigned int paramName, int param) {
    glTexParameteri(target, paramName, param);
}
} // namespace ktn::Graphics
