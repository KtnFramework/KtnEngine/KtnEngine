#include <ktnEngine/Math/ktnVector2D.hpp>
namespace ktn::Math {
void ktnVector2D::SetX(const float &x) {
    m_Data[0] = x;
}

void ktnVector2D::SetY(const float &y) {
    m_Data[1] = y;
}
} // namespace ktn::Math
