#include <ktnEngine/Math/ktnEulerAngles.hpp>

#include <ktnEngine/Core/ktnCoreUtils.hpp>

using ktn::Core::Utils::CompareFloat;
using ktn::Core::Utils::CompareFloatResult;

namespace ktn::Math {
ktnEulerAngles::ktnEulerAngles(const AngleUnit &eUnit, const float &ePitch, const float &eRoll, const float &eYaw) {
    switch (eUnit) {
    case AngleUnit::degree:
        Pitch = ePitch;
        Roll = eRoll;
        Yaw = eYaw;
        break;
    case AngleUnit::radian:
        Pitch = RadianToDegree(ePitch);
        Roll = RadianToDegree(eRoll);
        Yaw = RadianToDegree(eYaw);
        break;
    }
}

ktnEulerAngles::ktnEulerAngles(const ktnEulerAngles &eAngle) {
    Pitch = eAngle.Pitch;
    Roll = eAngle.Roll;
    Yaw = eAngle.Yaw;
}

auto ktnEulerAngles::operator==(const ktnEulerAngles &eAngles) const -> bool {
    return !(
        CompareFloat(Pitch, eAngles.Pitch) != CompareFloatResult::Equal //
        || CompareFloat(Roll, eAngles.Roll) != CompareFloatResult::Equal //
        || CompareFloat(Yaw, eAngles.Yaw) != CompareFloatResult::Equal);
}

auto ktnEulerAngles::operator!=(const ktnEulerAngles &eAngles) const -> bool {
    return CompareFloat(Pitch, eAngles.Pitch) != CompareFloatResult::Equal //
           || CompareFloat(Roll, eAngles.Roll) != CompareFloatResult::Equal //
           || CompareFloat(Yaw, eAngles.Yaw) != CompareFloatResult::Equal;
}

auto ktnEulerAngles::operator=(const ktnEulerAngles &eAngles) -> ktnEulerAngles & = default;
} // namespace ktn::Math
