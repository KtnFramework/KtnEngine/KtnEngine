#include <ktnEngine/Math/ktnVector3D.hpp>

namespace ktn::Math {
void ktnVector3D::SetX(const float &x) {
    Set(0, x);
}

void ktnVector3D::SetY(const float &y) {
    Set(1, y);
}

void ktnVector3D::SetZ(const float &z) {
    Set(2, z);
}

auto ktnVector3D::CrossProduct(const ktnVector3D &eVector) const -> ktnVector3D {
    ktnVector3D result;
    const size_t x = 0;
    const size_t y = 1;
    const size_t z = 2;
    result.SetX((*this)[y] * eVector.Z() - (*this)[z] * eVector.Y());
    result.SetY((*this)[z] * eVector.X() - (*this)[x] * eVector.Z());
    result.SetZ((*this)[x] * eVector.Y() - (*this)[y] * eVector.X());
    return result;
}

} // namespace ktn::Math
