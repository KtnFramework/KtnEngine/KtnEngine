#include <ktnEngine/Math/ktnQuaternion.hpp>

#include <iostream>

namespace ktn::Math {
///////////////////////////
// ktnQuaternion::Component
ktnQuaternion::Component::Component(const ComponentBasis &basis, const float &scale) : Basis(basis) {
    Scale = scale;
}

///////////////////////////////
// ktnQuaternion::ComponentReal
ktnQuaternion::ComponentReal::ComponentReal(const float &scale) noexcept : Component(ComponentBasis::Real, scale) {}

ktnQuaternion::ComponentReal::ComponentReal(const ComponentReal &c) : Component(ComponentBasis::Real, c.Scale) {}

ktnQuaternion::ComponentReal::~ComponentReal() = default;

auto ktnQuaternion::ComponentReal::operator=(const ComponentReal &c) -> ktnQuaternion::ComponentReal & {
    Scale = c.Scale;
    return *this;
}

auto ktnQuaternion::ComponentReal::operator+(const ComponentReal &c) const -> ktnQuaternion::ComponentReal {
    return ComponentReal(Scale + c.Scale);
}

auto ktnQuaternion::ComponentReal::operator+(const float &addend) const -> ktnQuaternion::ComponentReal {
    return ComponentReal(Scale + addend);
}

auto ktnQuaternion::ComponentReal::operator*(const float &scale) const -> ktnQuaternion::ComponentReal {
    return ComponentReal(Scale * scale);
}

////////////////////////////
// ktnQuaternion::ComponentI
ktnQuaternion::ComponentI::ComponentI(const float &scale) noexcept : Component(ComponentBasis::I, scale) {}

ktnQuaternion::ComponentI::ComponentI(const ComponentI &c) : Component(ComponentBasis::I, c.Scale) {}

ktnQuaternion::ComponentI::~ComponentI() = default;

auto ktnQuaternion::ComponentI::operator=(const ComponentI &c) -> ktnQuaternion::ComponentI & {
    Scale = c.Scale;
    return *this;
}

auto ktnQuaternion::ComponentI::operator+(const ComponentI &c) const -> ktnQuaternion::ComponentI {
    return ComponentI(Scale + c.Scale);
}

auto ktnQuaternion::ComponentI::operator*(const float &scale) const -> ktnQuaternion::ComponentI {
    return ComponentI(Scale * scale);
}

////////////////////////////
// ktnQuaternion::ComponentJ
ktnQuaternion::ComponentJ::ComponentJ(const float &scale) noexcept : Component(ComponentBasis::J, scale) {}

ktnQuaternion::ComponentJ::ComponentJ(const ComponentJ &c) : Component(ComponentBasis::J, c.Scale) {}

ktnQuaternion::ComponentJ::~ComponentJ() = default;

auto ktnQuaternion::ComponentJ::operator=(const ComponentJ &c) -> ktnQuaternion::ComponentJ & {
    Scale = c.Scale;
    return *this;
}

auto ktnQuaternion::ComponentJ::operator+(const ComponentJ &c) const -> ktnQuaternion::ComponentJ {
    return ComponentJ(Scale + c.Scale);
}

auto ktnQuaternion::ComponentJ::operator*(const float &scale) const -> ktnQuaternion::ComponentJ {
    return ComponentJ(Scale * scale);
}

////////////////////////////
// ktnQuaternion::ComponentK
ktnQuaternion::ComponentK::ComponentK(const float &scale) noexcept : Component(ComponentBasis::K, scale) {}

ktnQuaternion::ComponentK::ComponentK(const ComponentK &c) : Component(ComponentBasis::K, c.Scale) {}

ktnQuaternion::ComponentK::~ComponentK() = default;

auto ktnQuaternion::ComponentK::operator=(const ComponentK &c) -> ktnQuaternion::ComponentK & {
    Scale = c.Scale;
    return *this;
}

auto ktnQuaternion::ComponentK::operator+(const ComponentK &c) const -> ktnQuaternion::ComponentK {
    return ComponentK(Scale + c.Scale);
}

auto ktnQuaternion::ComponentK::operator*(const float &scale) const -> ktnQuaternion::ComponentK {
    return ComponentK(Scale * scale);
}

////////////////
// ktnQuaternion
ktnQuaternion::ktnQuaternion() = default;

ktnQuaternion::ktnQuaternion(const ktnQuaternion &q) {
    W = q.W;
    X = q.X;
    Y = q.Y;
    Z = q.Z;
}

ktnQuaternion::ktnQuaternion(const ComponentReal &real, const ComponentI &i, const ComponentJ &j, const ComponentK &k) {
    Set(real, i, j, k);
}

ktnQuaternion::ktnQuaternion(float real, float i, float j, float k) {
    Set(real, i, j, k);
}

ktnQuaternion::~ktnQuaternion() = default;

auto ktnQuaternion::operator=(const ktnQuaternion &eQuaternion) -> ktnQuaternion & {
    Set(eQuaternion.W, eQuaternion.X, eQuaternion.Y, eQuaternion.Z);
    return *this;
}

auto ktnQuaternion::operator+(const ktnQuaternion &eQuaternion) const -> ktnQuaternion {
    return ktnQuaternion(W + eQuaternion.W, X + eQuaternion.X, Y + eQuaternion.Y, Z + eQuaternion.Z);
}

auto ktnQuaternion::operator*(const ComponentI &c) const -> ktnQuaternion {
    // (w + xi + yj + zk) * i = wi + xii + yji + zki = -x + wi + zj - yk
    return ktnQuaternion(-X.Scale, W.Scale, Z.Scale, -Y.Scale) * c.Scale;
}

auto ktnQuaternion::operator*(const ComponentJ &c) const -> ktnQuaternion {
    // (w + xi + yj + zk) * j = wj + xij + yjj + zkj = -y - zi + wj + xk
    return ktnQuaternion(-Y.Scale, -Z.Scale, W.Scale, X.Scale) * c.Scale;
}

auto ktnQuaternion::operator*(const ComponentK &c) const -> ktnQuaternion {
    // (w + xi + yj + zk) * k = wk + xik + yjk + zkk = -z + yi - xj + wk
    return ktnQuaternion(-Z.Scale, Y.Scale, -X.Scale, W.Scale) * c.Scale;
}

auto ktnQuaternion::operator*(const ComponentReal &c) const -> ktnQuaternion {
    return operator*(c.Scale);
}

auto ktnQuaternion::operator*(const float &scale) const -> ktnQuaternion {
    return ktnQuaternion(W * scale, X * scale, Y * scale, Z * scale);
}

void ktnQuaternion::Set(const ComponentReal &real, const ComponentI &i, const ComponentJ &j, const ComponentK &k) {
    W = real;
    X = i;
    Y = j;
    Z = k;
}

void ktnQuaternion::Set(const float &real, const float &i, const float &j, const float &k) {
    W = ComponentReal(real);
    X = ComponentI(i);
    Y = ComponentJ(j);
    Z = ComponentK(k);
}

auto ktnQuaternion::Equals(const ktnQuaternion &q, const float &tolerance) -> bool {
    if (std::abs(W.Scale - q.W.Scale) > tolerance) { return false; }
    if (std::abs(X.Scale - q.X.Scale) > tolerance) { return false; }
    if (std::abs(Y.Scale - q.Y.Scale) > tolerance) { return false; }
    if (std::abs(Z.Scale - q.Z.Scale) > tolerance) { return false; }
    return true;
}

auto ktnQuaternion::Conjugated() const -> ktnQuaternion {
    return ktnQuaternion(W.Scale, -X.Scale, -Y.Scale, -Z.Scale);
}
auto ktnQuaternion::Norm() const -> float {
    float real = W.Scale;
    float i = X.Scale;
    float j = Y.Scale;
    float k = Z.Scale;
    return sqrtf((real * real) + (i * i) + (j * j) + (k * k));
}

////////////
// utilities
auto operator*(float scale, const ktnQuaternion &q) -> ktnQuaternion {
    return ktnQuaternion(q.W * scale, q.X * scale, q.Y * scale, q.Z * scale);
}

auto operator*(const ktnQuaternion::ComponentReal &basis, const ktnQuaternion &q) -> ktnQuaternion {
    return q * basis;
}

auto operator*(const ktnQuaternion::ComponentI &basis, const ktnQuaternion &q) -> ktnQuaternion {
    auto temp = q * basis;
    temp.Y = temp.Y * -1;
    temp.Z = temp.Z * -1;
    return temp;
}

auto operator*(const ktnQuaternion::ComponentJ &basis, const ktnQuaternion &q) -> ktnQuaternion {
    auto temp = q * basis;
    temp.X = temp.X * -1;
    temp.Z = temp.Z * -1;
    return temp;
}

auto operator*(const ktnQuaternion::ComponentK &basis, const ktnQuaternion &q) -> ktnQuaternion {
    auto temp = q * basis;
    temp.X = temp.X * -1;
    temp.Y = temp.Y * -1;
    return temp;
}

auto Normalize(const ktnQuaternion &q) -> QuaternionNormalizationResult {
    float norm = q.Norm();
    if (norm == 0.0F) {
        std::cerr << "ktnQuaternion.cpp: Normalize: The given quaternion has size 0 and therefore cannot be normalized. " << std::endl;
        return {ktn::ExecutionStatus::FAILURE, ktnQuaternion(0, 0, 0, 0)};
    }
    return {ktn::ExecutionStatus::OK, ktnQuaternion(q.W, q.X, q.Y, q.Z) * (1 / norm)};
}
} // namespace ktn::Math
