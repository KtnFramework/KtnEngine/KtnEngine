#include <ktnEngine/Physics/ktnPhysicsScene.hpp>

#include <ktnEngine/Physics/ktnPhysicsUtils.hpp>

#include <chrono/ChConfig.h>

#include <chrono/collision/ChCollisionSystemBullet.h>
#include <chrono/physics/ChSystemNSC.h>

#include <iostream>
#include <memory>

using namespace std;

const inline static float GRAVITY_EARTH(9.81);
const inline static float MSECS_IN_A_SECOND(1000.0F);

namespace ktn::Physics {

class ktnPhysicsScene::Impl {
public:
    Impl() {
        m_System = make_unique<::chrono::ChSystemNSC>();
    }

    ::chrono::ChSystem &System() {
        return *m_System;
    }

    void AddPhysicsItem(shared_ptr<ktnRigidBody> body) {
        m_System->Add(body->Body);
    }

    void Step(double milliseconds) {
        m_System->DoStepDynamics(milliseconds / 1000.0);
    }

private:
    std::unique_ptr<::chrono::ChSystemNSC> m_System;
};

ktnPhysicsScene::ktnPhysicsScene() {
    m_Impl = new Impl();
}

ktnPhysicsScene::~ktnPhysicsScene() {
    delete m_Impl;
}

void ktnPhysicsScene::AddObject(std::shared_ptr<ktnPhysicalObject> eObject) {
    m_Objects.push_back(eObject);
    m_Impl->AddPhysicsItem(eObject->RigidBody);
}

void ktnPhysicsScene::Start() {
    m_LastTime = Core::Utils::CurrentMillisecondsSinceEpoch();
}

void ktnPhysicsScene::Stop() {
    m_LastTime = nullopt;
}

void ktnPhysicsScene::Update() {
    auto currenttime = Core::Utils::CurrentMillisecondsSinceEpoch();
    m_Impl->Step(currenttime - m_LastTime.value());
    m_LastTime = currenttime;

    for (auto &obj : m_Objects) {
        if (!obj->RigidBody->Body->GetSleeping()) {
            obj->RigidBody->PositionUpdated.Emit(Utils::Vec3Convert_ch2glm(obj->RigidBody->Body->GetPos()));
            obj->RigidBody->RotationUpdated.Emit(Utils::QuatConvert_ch2glm(obj->RigidBody->Body->GetRot()));
        }
    }
}
} // namespace ktn::Physics
