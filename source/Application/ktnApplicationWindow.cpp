#include <ktnEngine/Application/ktnApplicationWindow.hpp>

#include <GLFW/glfw3.h>

namespace ktn::Application {
using namespace ktn::Graphics;
using namespace std;

ktnApplicationWindow::ktnApplicationWindow(const vk::Extent2D &eWindowSize) {
    WindowSize = eWindowSize;
}

ktnApplicationWindow::~ktnApplicationWindow() {}

void ktnApplicationWindow::CleanUp() {
    m_Application.reset();
    glfwDestroyWindow(Window);
    glfwTerminate();
}
void ktnApplicationWindow::Initialize(shared_ptr<ktnApplication> eApplication) {
    glfwInit();
    m_Application = eApplication;
    switch (m_Application->Renderer->Backend()) {
    case ktnGraphicsBackend::OPENGL:
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        break;
    case ktnGraphicsBackend::VULKAN:
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        break;
    }

    m_Application->Renderer->FrameRendered.Connect(this, &ktnApplicationWindow::SwapBuffers);

    Window = glfwCreateWindow(WindowSize.width, WindowSize.height, WindowName.c_str(), nullptr, nullptr);
    if (Window == nullptr) { throw runtime_error("ktnGame: could not create GLFW window"); }

    glfwMakeContextCurrent(Window);
    glfwSwapInterval(1); // vsync option, wait till the number inside is rendered

    // cast the private member methods to static functions so that they can be assigned as callback for glfw
    glfwSetWindowUserPointer(Window, this);

    auto callback_MouseMove_static = [](GLFWwindow *window, double xPos, double yPos) {
        static_cast<ktnApplicationWindow *>(glfwGetWindowUserPointer(window))->callback_MouseMove(window, xPos, yPos);
    };
    glfwSetCursorPosCallback(Window, callback_MouseMove_static);

    auto callback_MouseClick_static = [](GLFWwindow *window, int button, int action, int mods) {
        static_cast<ktnApplicationWindow *>(glfwGetWindowUserPointer(window))->callback_MouseClick(window, button, action, mods);
    };
    glfwSetMouseButtonCallback(Window, callback_MouseClick_static);

    auto callback_FrameBufferSizeChange_static = [](GLFWwindow *window, int width, int height) {
        static_cast<ktnApplicationWindow *>(glfwGetWindowUserPointer(window))->callback_FrameBufferSizeChange(window, width, height);
    };
    glfwSetFramebufferSizeCallback(Window, callback_FrameBufferSizeChange_static);

    auto callback_Drop_static = [](GLFWwindow *window, int count, const char **paths) {
        static_cast<ktnApplicationWindow *>(glfwGetWindowUserPointer(window))->callback_Drop(window, count, paths);
    };
    glfwSetDropCallback(Window, callback_Drop_static);

    m_Application->Renderer->SetContext(Window);
    m_Application->Initialize(WindowSize);
    WindowSignal_MouseMoved.Connect(&m_Application->Signal_MouseMoved);
    WindowSignal_FrameBufferSizeChanged.Connect(&m_Application->Signal_FrameBufferSizeChanged);
}

void ktnApplicationWindow::callback_Drop(GLFWwindow *window, int count, const char **paths) {
    WindowSignal_ItemsDroppedIntoWindow.Emit(count, paths);
}

void ktnApplicationWindow::callback_FrameBufferSizeChange(GLFWwindow *window, int width, int height) {
    WindowSignal_FrameBufferSizeChanged.Emit(width, height);
}

void ktnApplicationWindow::callback_MouseClick(GLFWwindow *window, int button, int action, int mods) {
    WindowSignal_MouseClicked.Emit(button, action, mods);
}

void ktnApplicationWindow::callback_MouseMove(GLFWwindow *window, double xpos, double ypos) {
    WindowSignal_MouseMoved.Emit(xpos, ypos);
}

void ktnApplicationWindow::HideCursor() const {
    if (nullptr == Window) { throw invalid_argument(string(__FUNCTION__) + ": Game window is not initialized."); }
    glfwSetInputMode(Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); // hides and disallows the cursor from leaving the window
}

void ktnApplicationWindow::Run() {
    while (glfwWindowShouldClose(Window) == 0) {
        m_Application->ActiveScene()->PhysicsScene()->Update();
        m_Application->ActiveScene()->ProcessInput();
        m_Application->Renderer->RenderFrame(m_Application->ActiveScene()->GraphicsScene());
        glfwPollEvents();
        m_Application->Renderer->WaitIdle();
    }
}
void ktnApplicationWindow::ShowCursor() const {
    if (nullptr == Window) { throw invalid_argument(string(__FUNCTION__) + ": Game window is not initialized."); }
    glfwSetInputMode(Window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

void ktnApplicationWindow::SwapBuffers() const {
    glfwSwapBuffers(Window);
}

} // namespace ktn::Application
