#include "ktnEngine/Application/ktnApplication.hpp"

#include "ktnEngine/Graphics/ktnRenderer_OpenGL.hpp"
#include "ktnEngine/Graphics/ktnRenderer_Vulkan.hpp"

#include <chrono>
#include <iostream>
#include <mutex>

using namespace ktn::Graphics;

namespace ktn::Application {
ktnApplication::ktnApplication(std::shared_ptr<Graphics::IktnRenderer> eRenderer) {
    constexpr unsigned int defaultShadowMapSize(1024);
    ShadowMapSize = defaultShadowMapSize;
    Renderer = eRenderer;
}

ktnApplication::~ktnApplication() {
    if (Renderer != nullptr) { Renderer->CleanUp(); }
}

void ktnApplication::SetActiveScene(const std::shared_ptr<ktnScene> &scene) {
    if (m_ActiveScene != nullptr) {
        m_ActiveScene->PhysicsScene()->Stop();
        Renderer->UnloadFromGPU(m_ActiveScene->GraphicsScene());
    }
    Signal_FrameBufferSizeChanged.DisconnectReceiver(m_ActiveScene.get());
    Signal_ItemsDroppedIntoWindow.DisconnectReceiver(m_ActiveScene.get());
    Signal_MouseClicked.DisconnectReceiver(m_ActiveScene.get());
    Signal_MouseMoved.DisconnectReceiver(m_ActiveScene.get());

    m_ActiveScene = scene;
    Renderer->LoadToGPU(m_ActiveScene->GraphicsScene());
    Signal_FrameBufferSizeChanged.Connect(m_ActiveScene.get(), &ktnScene::HandleEvent_FrameBufferSizeChanged);
    Signal_ItemsDroppedIntoWindow.Connect(m_ActiveScene.get(), &ktnScene::HandleEvent_ItemsDroppedIntoWindow);
    Signal_MouseClicked.Connect(m_ActiveScene.get(), &ktnScene::HandleEvent_MouseClicked);
    Signal_MouseMoved.Connect(m_ActiveScene.get(), &ktnScene::HandleEvent_MouseMoved);

    m_ActiveScene->PhysicsScene()->Start();
}

void ktnApplication::Initialize(const vk::Extent2D &eWindowSize) {
    Renderer->Initialize(eWindowSize, ShadowMapSize);
    Signal_FrameBufferSizeChanged.Connect(Renderer.get(), &IktnRenderer::HandleEvent_FramebufferSizeChanged);
}

auto ktnApplication::ActiveScene() -> std::shared_ptr<ktnScene> {
    return m_ActiveScene;
}

} // namespace ktn::Application
