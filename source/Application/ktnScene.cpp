#include "ktnEngine/Application/ktnScene.hpp"

#include <fstream>
#include <streambuf>

namespace ktn::Application {
using namespace std;
using namespace ktn::Core;
using namespace ktn::Graphics;
using namespace ktn::Physics;

ktnScene::ktnScene(ktnGraphicsScenePtr eGraphicsScene, ktnPhysicsScenePtr ePhysicsScene, string eName) : ktnObject(eName) {
    m_GraphicsScene = eGraphicsScene;
    m_PhysicsScene = ePhysicsScene;
}

ktnScene::~ktnScene() {}

ktnGraphicsScenePtr ktnScene::GraphicsScene() {
    return m_GraphicsScene;
}

ktnPhysicsScenePtr ktnScene::PhysicsScene() {
    return m_PhysicsScene;
}

void ktnScene::HandleEvent_ItemsDroppedIntoWindow(int count, const char **paths) {
    cout << typeid(this).name() << "::" << __FUNCTION__ << ": Items dropped into scene " //
         << this->Name.ToStdString() << "with no effect: " << endl;
    for (int i = 0; i < count; ++i) { cout << paths[i] << endl; }
}

void ktnScene::HandleEvent_FrameBufferSizeChanged(int width, int height) {
    cout << typeid(this).name() << "::" << __FUNCTION__ << ": GLFW window size changed to [" << width << "," << height << "]" << endl;
}
} // namespace ktn::Application
