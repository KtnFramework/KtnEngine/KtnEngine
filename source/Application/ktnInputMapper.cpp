#include <ktnEngine/Application/ktnInputMapper.hpp>

#include "GLFW/glfw3.h"

#include <vector>

namespace ktn ::Application {
/**
 * AllRecognizableKeys is used for mapping every recognizable keys at once.
 * This is inconvenient, but since GLFW uses defines to mark its keys, which are not sequential,
 * this is the fastest and least error-prone way of doing it.
 * Ideally, this list should be written only once in the header file, but that would require detecting the input without GLFW
 * Getting rid of GLFW is a goal, albeit not high priority.
 */
static std::vector<ktnKey> AllRecognizableKeys = {
    ktnKey::KTNKEY_UNKNOWN,
    ktnKey::KTNKEY_SPACE,
    ktnKey::KTNKEY_APOSTROPHE,
    ktnKey::KTNKEY_COMMA,
    ktnKey::KTNKEY_MINUS,
    ktnKey::KTNKEY_PERIOD,
    ktnKey::KTNKEY_SLASH,

    ktnKey::KTNKEY_0,
    ktnKey::KTNKEY_1,
    ktnKey::KTNKEY_2,
    ktnKey::KTNKEY_3,
    ktnKey::KTNKEY_4,
    ktnKey::KTNKEY_5,
    ktnKey::KTNKEY_6,
    ktnKey::KTNKEY_7,
    ktnKey::KTNKEY_8,
    ktnKey::KTNKEY_9,

    ktnKey::KTNKEY_SEMICOLON,
    ktnKey::KTNKEY_EQUAL,

    ktnKey::KTNKEY_A,
    ktnKey::KTNKEY_B,
    ktnKey::KTNKEY_C,
    ktnKey::KTNKEY_D,
    ktnKey::KTNKEY_E,
    ktnKey::KTNKEY_F,
    ktnKey::KTNKEY_G,
    ktnKey::KTNKEY_H,
    ktnKey::KTNKEY_I,
    ktnKey::KTNKEY_J,
    ktnKey::KTNKEY_K,
    ktnKey::KTNKEY_L,
    ktnKey::KTNKEY_M,
    ktnKey::KTNKEY_N,
    ktnKey::KTNKEY_O,
    ktnKey::KTNKEY_P,
    ktnKey::KTNKEY_Q,
    ktnKey::KTNKEY_R,
    ktnKey::KTNKEY_S,
    ktnKey::KTNKEY_T,
    ktnKey::KTNKEY_U,
    ktnKey::KTNKEY_V,
    ktnKey::KTNKEY_W,
    ktnKey::KTNKEY_X,
    ktnKey::KTNKEY_Y,
    ktnKey::KTNKEY_Z,

    ktnKey::KTNKEY_LEFT_BRACKET,
    ktnKey::KTNKEY_BACKLASH,
    ktnKey::KTNKEY_RIGHT_BRACKET,
    ktnKey::KTNKEY_GRAVE_ACCENT,
    ktnKey::KTNKEY_WORLD_1,
    ktnKey::KTNKEY_WORLD_2,

    ktnKey::KTNKEY_ESCAPE,
    ktnKey::KTNKEY_ENTER,
    ktnKey::KTNKEY_TAB,
    ktnKey::KTNKEY_BACKSPACE,
    ktnKey::KTNKEY_INSERT,
    ktnKey::KTNKEY_DELETE,
    ktnKey::KTNKEY_RIGHT,
    ktnKey::KTNKEY_LEFT,
    ktnKey::KTNKEY_DOWN,
    ktnKey::KTNKEY_UP,
    ktnKey::KTNKEY_PAGE_UP,
    ktnKey::KTNKEY_PAGE_DOWN,
    ktnKey::KTNKEY_HOME,
    ktnKey::KTNKEY_END,
    ktnKey::KTNKEY_CAPS_LOCK,
    ktnKey::KTNKEY_SCROLL_LOCK,
    ktnKey::KTNKEY_NUM_LOCK,
    ktnKey::KTNKEY_PRINT_SCREEN,
    ktnKey::KTNKEY_PAUSE,

    ktnKey::KTNKEY_F1,
    ktnKey::KTNKEY_F2,
    ktnKey::KTNKEY_F3,
    ktnKey::KTNKEY_F4,
    ktnKey::KTNKEY_F5,
    ktnKey::KTNKEY_F6,
    ktnKey::KTNKEY_F7,
    ktnKey::KTNKEY_F8,
    ktnKey::KTNKEY_F9,
    ktnKey::KTNKEY_F10,
    ktnKey::KTNKEY_F11,
    ktnKey::KTNKEY_F12,
    ktnKey::KTNKEY_F13,
    ktnKey::KTNKEY_F14,
    ktnKey::KTNKEY_F15,
    ktnKey::KTNKEY_F16,
    ktnKey::KTNKEY_F17,
    ktnKey::KTNKEY_F18,
    ktnKey::KTNKEY_F19,
    ktnKey::KTNKEY_F20,
    ktnKey::KTNKEY_F21,
    ktnKey::KTNKEY_F22,
    ktnKey::KTNKEY_F23,
    ktnKey::KTNKEY_F24,
    ktnKey::KTNKEY_F25,

    ktnKey::KTNKEY_KP_0,
    ktnKey::KTNKEY_KP_1,
    ktnKey::KTNKEY_KP_2,
    ktnKey::KTNKEY_KP_3,
    ktnKey::KTNKEY_KP_4,
    ktnKey::KTNKEY_KP_5,
    ktnKey::KTNKEY_KP_6,
    ktnKey::KTNKEY_KP_7,
    ktnKey::KTNKEY_KP_8,
    ktnKey::KTNKEY_KP_9,
    ktnKey::KTNKEY_KP_DECIMAL,
    ktnKey::KTNKEY_KP_DIVIDE,
    ktnKey::KTNKEY_KP_MULTIPLY,
    ktnKey::KTNKEY_KP_SUBTRACT,
    ktnKey::KTNKEY_KP_ADD,
    ktnKey::KTNKEY_KP_ENTER,
    ktnKey::KTNKEY_KP_EQUAL,

    ktnKey::KTNKEY_LEFT_SHIFT,
    ktnKey::KTNKEY_LEFT_CONTROL,
    ktnKey::KTNKEY_LEFT_ALT,
    ktnKey::KTNKEY_LEFT_SUPER,
    ktnKey::KTNKEY_RIGHT_SHIFT,
    ktnKey::KTNKEY_RIGHT_CONTROL,
    ktnKey::KTNKEY_RIGHT_ALT,
    ktnKey::KTNKEY_RIGHT_SUPER,
    ktnKey::KTNKEY_MENU //
};

ktnInputMapper::~ktnInputMapper() = default;

void ktnInputMapper::Initialize() {
    // default keymap should map all keys detectable with glfw
    for (ktnKey &key : AllRecognizableKeys) { MapKeyboardInput(key, key); }
}

void ktnInputMapper::MapKeyboardInput(const ktnKey &eKey, const ktnKey &ePressedKey) {
    Map[eKey] = ePressedKey;
}
} // namespace ktn::Application
